﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ArObject : MonoBehaviour
{
    public string mainType;
    public string subtype;

    private GameObject arObjectParent;
    private GameObject myCamera;
    private Outline outline;
    private bool isSelected;
    private MeshRenderer mesh;
    private int id;

    protected SteamVR_Action_Boolean input;

    // Trajectories
    private IList<Vector3> trajectoryToFollow;
    private List<Quaternion> rotationToFollow;
    private Vector3 directionToFollow;
    private bool followDirection;
    private EventIconController eventToTrigger;
    private int trajectoryIdx;
    
    // Geometry
    private float radius;
    private float localRadius;

    // Attributes
    private List<AttributeController> attributeIcons;
    private bool attributesAreDisplayed;
    private bool isVisibleAtStart;
    private Color colorAtStart;

    // Events
    private EventIconController[] eventsToReattach;
    private CircleDispatcher dispatcher;


    // Start is called before the first frame update
    void Start()
    {
        bool colorisSet = false;
        if (id > 0)
        {
            colorisSet = true;
        }
        if (id <3)
        {
            id = ApplicationModel.idCpt;
            ApplicationModel.idCpt = id + 1;
        }
        GeometryAnalyzer geoAnalyzer = new GeometryAnalyzer();
        radius = geoAnalyzer.getHorizontalRadius(gameObject);
        localRadius = geoAnalyzer.getLocalRadius(gameObject);
        input = SteamVR_Input.GetBooleanAction("GrabGrip");
        arObjectParent = GameObject.Find("ARCreations");
        myCamera = GameObject.Find("VRCamera");

        outline = GetComponent<Outline>();
        if (outline == null)
        {
            try
            {
                outline = gameObject.AddComponent<Outline>();
            }
            catch
            {
                Debug.Log("Cannot add outline to " + gameObject.name);
            }
            
        }
        gameObject.AddComponent<MoveRecorder>();
        gameObject.AddComponent<CollisionRecorder>();

        if (outline != null)
        {
            outline.OutlineMode = Outline.Mode.OutlineAll;
            outline.OutlineColor = Color.black;
            outline.OutlineWidth = 5f;
            enableOutline(false);
        }

        // Attributes
        attributeIcons = new List<AttributeController>();

        // Visibility attribute
        string name = "Attributes\\EyeAttribute";
        UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
        if (assetToGet == null)
        {
            Debug.LogError("Can't find eye attribute");
        }
        VisibleAttributeController eyeController = GetComponentInChildren<VisibleAttributeController>();
        if (eyeController == null)
        {
            GameObject eye = Instantiate((GameObject)assetToGet, transform.position, Quaternion.identity);
            eye.transform.SetParent(transform);
            AttributeController attrController = eye.GetComponent<AttributeController>();
            attrController.setArParent(this);
            attrController.setToDefaultValue();
            attributeIcons.Add(attrController);
            eye.SetActive(false);
        }
        else
        {
            attributeIcons.Add(eyeController);
        }
        

        // Color Attribute
        if (gameObject.GetComponent<MeshRenderer>() != null)
        {
            ColorAttribute colorAttrCtrl = GetComponentInChildren<ColorAttribute>();
            if (colorAttrCtrl == null)
            {
                string colorName = "Attributes\\ColorAttribute";
                mesh = gameObject.GetComponent<MeshRenderer>();
                UnityEngine.Object colorAsset = Resources.Load<UnityEngine.Object>(colorName);
                GameObject colorAttr = Instantiate((GameObject)colorAsset, transform.position, Quaternion.identity);
                colorAttr.transform.SetParent(transform);
                colorAttrCtrl = colorAttr.GetComponent<ColorAttribute>();
                colorAttrCtrl.setArParent(this);
                attributeIcons.Add(colorAttrCtrl);
                colorAttr.SetActive(false);
            }
            else
            {
                attributeIcons.Add(colorAttrCtrl);
            }

            if (!colorisSet)
            {
                colorAtStart = mesh.material.color;
            }
            else
            {
                colorAttrCtrl.setToDefaultValue();
            }

            
        }

        attributesAreDisplayed = false;

    }

    public void hideShowAttributes()
    {
        if (attributesAreDisplayed)
        {
            // Hide attributes
            foreach(AttributeController attr in attributeIcons)
            {
                attr.gameObject.SetActive(false);
            }
        }
        else
        {
            //Show attributes
            foreach (AttributeController attr in attributeIcons)
            {
                attr.gameObject.SetActive(true);
            }
        }
        attributesAreDisplayed = !attributesAreDisplayed;
    }

    public void hideAttributes()
    {
        if (attributesAreDisplayed)
        {
            // Hide attributes
            foreach (AttributeController attr in attributeIcons)
            {
                attr.gameObject.SetActive(false);
            }
        }
        attributesAreDisplayed = false;
    }

    public void setAttributesToDefault()
    {
        foreach(AttributeController attr in attributeIcons)
        {
            attr.setToDefaultValue();
        }
    }

    public void setIsVisibleAtStart(bool visible)
    {
        isVisibleAtStart = visible;
        setTransparency(visible);
    }

    public bool getIsVisibleAtStart()
    {
        return isVisibleAtStart;
    }

    public bool getIsSelected()
    {
        return isSelected;
    }

    public void enableOutline(bool onOff)
    {
        
        if (outline != null)
        {
            outline.OutlineColor = new Color(0, 0, 255);
            outline.enabled = onOff;
            if (onOff)
            {
                isSelected = true;
            }
            else
            {
                isSelected = false;
            }
        }
        else
        {
            Debug.Log("Outline is null");
        }
        
    }

    public void colorOutline(Color c)
    {
        // Switch on outline with a given color
        outline.OutlineColor = c;
        outline.enabled = true;
    }

    public void defaultOutline()
    {
        if (outline == null)
        {
            outline = GetComponent<Outline>();
        }
        outline.OutlineColor = Color.black;
        outline.enabled = false;
        Debug.Log("Initialized outline");
    }

    public void outlineOff()
    {
        // Swith off the object's outline unless it is selected than switch back to blue color
        if (!isSelected)
        {
            outline.enabled = false;
        }
        else
        {
            outline.OutlineColor = new Color(0, 0, 255);
        }
    }

    public void startFollowingDirection(Vector3 direction)
    {
        directionToFollow = direction;
        followDirection = true;
        InvokeRepeating("nextMove", 0f, 0.2f);
    }

    public void followTrajectory(IList<Vector3> trajectory, List<Quaternion> rotations, EventIconController eventIconToTrigger)
    {
        followDirection = false;
        trajectoryToFollow = trajectory;
        rotationToFollow = rotations;
        eventToTrigger = eventIconToTrigger;
        trajectoryIdx = 0;
        InvokeRepeating("nextMove", 0f, 0.2f);
    }

    public void nextMove()
    {
        if(ApplicationModel.mode == "Test")
        {
            if (followDirection)
            {
                Vector3 nextPt = new Vector3(transform.position.x + directionToFollow.x, transform.position.y + directionToFollow.y, transform.position.z + directionToFollow.z);
                // source : https://gamedevbeginner.com/the-right-way-to-lerp-in-unity-with-examples/
                float time = 0;
                Vector3 startPosition = transform.position;
                float duration = 0.2f;
                while (time < duration)
                {
                    transform.position = Vector3.Lerp(startPosition, nextPt, time / duration);
                    time += Time.deltaTime;
                }
                //transform.position = Vector3.MoveTowards(transform.position, nextPt, 0.5f);
            }
            else
            {
                if ((trajectoryToFollow!=null && trajectoryIdx < trajectoryToFollow.Count) || (rotationToFollow!=null && trajectoryIdx < rotationToFollow.Count))
                {
                    if(trajectoryToFollow!=null && trajectoryIdx < trajectoryToFollow.Count)
                    {
                        // source : https://gamedevbeginner.com/the-right-way-to-lerp-in-unity-with-examples/
                        float time = 0;
                        Vector3 startPosition = transform.position;
                        float duration = 0.2f;
                        while (time < duration)
                        {
                            transform.position = Vector3.Lerp(startPosition, trajectoryToFollow[trajectoryIdx], time / duration);
                            time += Time.deltaTime;
                        }
                        //transform.position = Vector3.MoveTowards(transform.position, trajectoryToFollow[trajectoryIdx], 0.2f);
                    }
                    if (rotationToFollow!=null && trajectoryIdx < rotationToFollow.Count)
                    {
                        transform.rotation = rotationToFollow[trajectoryIdx];
                    }
                    trajectoryIdx++;
                }
                else
                {
                    CancelInvoke();
                    eventToTrigger.trigerDone();
                    trajectoryIdx = 0;
                    trajectoryToFollow = null;
                }
            }
        }
        else
        {
            CancelInvoke();
        }
        
    }

    public float getRadius()
    {
        return radius;
    }


    public float getLocalRadius()
    {
        return localRadius;
    }

    public Vector3 getMiddlePoint()
    {
        GeometryAnalyzer geoAnalyzer = ScriptableObject.CreateInstance<GeometryAnalyzer>();
        return geoAnalyzer.getCenterPoint(gameObject);
    }

    public void setColorAtStart(Color coloration)
    {
        colorAtStart = coloration;
    }

    public Color getColorAtStart()
    {
        return colorAtStart;
    }

    public void defaultColor()
    {
        mesh.material.color = colorAtStart;
        setTransparency(isVisibleAtStart);
    }

    public void initializeAttributes(int objectID, bool visible, Color color)
    {
        id = objectID;
        setIsVisibleAtStart(visible);
        setColorAtStart(color);
    }

    public ObjectDescription getObjectDescription()
    {
        float[] pos = new float[] { transform.position.x, transform.position.y, transform.position.z };
        float[] rota = new float[] { transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w};
        float[] coloration = new float[] { colorAtStart.r, colorAtStart.g, colorAtStart.b, colorAtStart.a };
        float[] size = new float[] { transform.localScale.x, transform.localScale.y, transform.localScale.z };
        ObjectDescription description = new ObjectDescription(mainType, id, subtype, pos, rota, isVisibleAtStart, coloration, size);
        return description;
    }

    public void setID(int idx)
    {
        id = idx;
    }

    public int getID()
    {
        return id;
    }

    public void attachAttributes(bool getAttached)
    {
        foreach(AttributeController atr in attributeIcons)
        {
            if (getAttached)
            {
                atr.gameObject.transform.SetParent(gameObject.transform);
            }
            else
            {
                atr.gameObject.transform.SetParent(arObjectParent.transform);
            }
        }
    }

    public void attachEvents(bool getAttached)
    {
        if (!getAttached)
        {
            eventsToReattach = GetComponentsInChildren<EventIconController>();
            foreach(EventIconController e in eventsToReattach)
            {
                Debug.Log("Hide event " + e.getRecordedEvent().getActionName());
                e.gameObject.transform.SetParent(arObjectParent.transform);
                e.gameObject.SetActive(false);
            }
        }
        else
        {
            if (dispatcher == null)
            {
                dispatcher = GetComponent<CircleDispatcher>();
            }
            foreach(EventIconController e in eventsToReattach)
            {
                Debug.Log("Show event " + e.getRecordedEvent().getActionName());
                e.gameObject.SetActive(true);
                e.gameObject.transform.SetParent(transform);
            }
            dispatcher.haveToUpdate();
        }
    }

    public void setTransparency(bool isOpaque)
    {
        MeshRenderer meshRender = gameObject.GetComponent<MeshRenderer>();

        if (meshRender != null)
        {
            Material objectMaterial = meshRender.material;
            Color c = objectMaterial.color;
            if (isOpaque)
            {
                MaterialExtensions.ToOpaqueMode(objectMaterial);
                objectMaterial.color = new Color(c.r, c.g, c.b, 1);
            }
            else
            {
                MaterialExtensions.ToFadeMode(objectMaterial);
                meshRender.material.color = new Color(c.r, c.g, c.b, 0.4f);
                //objectMaterial.color = new Color(c.r, c.g, c.b, 200);
            }
            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//source: https://answers.unity.com/questions/339532/how-can-i-detect-which-side-of-a-box-i-collided-wi.html
public class SurfaceDetectorController : MonoBehaviour
{
    private Quaternion preCollisionRotation;
    private Quaternion forcedRotation;
    private GameObject oldParent;
    private Vector3 oldScale;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    /*void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Image collided");
        Debug.Log(ReturnDirection(collision.gameObject, this.gameObject));
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall" && ApplicationModel.mode!="Test")
        {
            oldParent = gameObject.transform.parent.gameObject;
            oldScale = gameObject.transform.localScale;
            Vector3 rotation = Vector3.zero;
            HitDirection face = ReturnDirection(other.gameObject, this.gameObject);
            if (face == HitDirection.Back)
            {
                rotation = new Vector3(0f, 0f, 0f);
            }
            else if (face == HitDirection.Forward)
            {
                rotation = new Vector3(0f, 180f, 0f);
            }
            else if (face == HitDirection.Right)
            {
                rotation = new Vector3(0f, -90f, 0f);
            }
            else if (face == HitDirection.Left)
            {
                rotation = new Vector3(0f, 90f, 0f);
            }
            else if (face == HitDirection.Top)
            {
                rotation = new Vector3(90f, 0f, 0f);
            }
            else if (face == HitDirection.Bottom)
            {
                rotation = new Vector3(-90f, 0f, 0f);
            }
            gameObject.transform.SetParent(other.gameObject.transform);
            gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            gameObject.transform.Rotate(rotation);
            gameObject.transform.SetParent(oldParent.transform);
            gameObject.transform.localScale = oldScale;
            forcedRotation = gameObject.transform.rotation;
        }
        

    }

    private enum HitDirection { None, Top, Bottom, Forward, Back, Left, Right }
    private HitDirection ReturnDirection(GameObject Object, GameObject ObjectHit)
    {

        HitDirection hitDirection = HitDirection.None;
        RaycastHit MyRayHit;
        Vector3 direction = (Object.transform.position - ObjectHit.transform.position).normalized;
        Ray MyRay = new Ray(ObjectHit.transform.position, direction);

        if (Physics.Raycast(MyRay, out MyRayHit))
        {

            if (MyRayHit.collider != null)
            {

                Vector3 MyNormal = MyRayHit.normal;
                MyNormal = MyRayHit.transform.TransformDirection(MyNormal);

                if (MyNormal == MyRayHit.transform.up) { hitDirection = HitDirection.Top; }
                if (MyNormal == -MyRayHit.transform.up) { hitDirection = HitDirection.Bottom; }
                if (MyNormal == MyRayHit.transform.forward) { hitDirection = HitDirection.Forward; }
                if (MyNormal == -MyRayHit.transform.forward) { hitDirection = HitDirection.Back; }
                if (MyNormal == MyRayHit.transform.right) { hitDirection = HitDirection.Right; }
                if (MyNormal == -MyRayHit.transform.right) { hitDirection = HitDirection.Left; }
            }
        }
        return hitDirection;
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall")
        {
            preCollisionRotation = gameObject.transform.rotation;
            Vector3[] vertices = other.gameObject.GetComponent<MeshFilter>().mesh.vertices;
            if (vertices.Length > 0)
            {
                Vector3 v1 = transform.TransformPoint(vertices[0]);
                float xmin = v1.x;
                float xmax = xmin;
                float ymin = v1.y;
                float ymax = ymin;
                float zmin = v1.z;
                float zmax = zmin;

                foreach(Vector3 v in vertices)
                {
                    Vector3 v2 = transform.TransformPoint(v);
                    if (v2.x < xmin)
                    {
                        xmin = v2.x;
                    }
                    if(v2.x > xmax)
                    {
                        xmax = v2.x;
                    }
                    if(v2.y < ymin)
                    {
                        ymin = v2.y;
                    }
                    if (v2.y > ymax)
                    {
                        ymax = v2.y;
                    }
                    if (v2.z < zmin)
                    {
                        zmin = v2.z;
                    }
                    if(v2.z > zmax)
                    {
                        zmax = v2.z;
                    }
                }
                float x = gameObject.transform.position.x;
                float y = gameObject.transform.position.y;
                float z = gameObject.transform.position.z;
                oldParent = gameObject.transform.parent.gameObject;
                oldScale = gameObject.transform.localScale;

                float distXmin = Mathf.Abs(x - xmin);
                float distXmax = Mathf.Abs(x - xmax);
                float distYmin = Mathf.Abs(y - ymin);
                float distYmax = Mathf.Abs(y - ymax);
                float distZmin = Mathf.Abs(z - zmin);
                float distZmax = Mathf.Abs(z - zmax);

                float minDist = Mathf.Min(distXmin, distXmax, distYmin, distYmax, distZmin, distZmax);
                Vector3 rotation = Vector3.zero;
                if (minDist == distZmin)
                {
                    rotation = new Vector3(0f, 180f, 0f);
                    Debug.Log("Case1");
                }
                else if (minDist == distZmax)
                {
                    Debug.Log("Case2");
                    rotation = new Vector3(0f, 0f, 0f);
                }
                else if(minDist == distXmin)
                {
                    Debug.Log("Case3");
                    rotation = new Vector3(0f, 90f, 0f);
                }
                else if (minDist == distXmax)
                {
                    Debug.Log("Case4");
                    rotation = new Vector3(0f, -90f, 0f);
                }
                else if(minDist == distYmin)
                {
                    Debug.Log("Case5");
                    rotation = new Vector3(90f, 0f, 0f);
                }
                else if (minDist == distYmax)
                {
                    Debug.Log("Case6");
                    rotation = new Vector3(-90f, 0f, 0f);
                }

                gameObject.transform.SetParent(other.gameObject.transform);
                gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                gameObject.transform.Rotate(rotation);
                gameObject.transform.SetParent(oldParent.transform);


                /*if (xmin<=x && x<=xmax && ymin<=y && y <= ymax)
                {
                    if (z <=zmin || z < other.gameObject.transform.position.z )
                    {
                        Debug.Log("Case11");
                        gameObject.transform.SetParent(other.gameObject.transform);
                        gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                        gameObject.transform.SetParent(oldParent.transform);
                        //gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, zmin);
                    }
                    else if (z >= zmax || z>= other.gameObject.transform.position.z)
                    {
                        Debug.Log("Case12");
                        gameObject.transform.SetParent(other.gameObject.transform);
                        gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                        gameObject.transform.Rotate(new Vector3(0f, 180f, 0f));
                        gameObject.transform.SetParent(oldParent.transform);
                        //gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, zmax);
                    }
                    else
                    {
                        Debug.Log("Case13");
                    }
                }
                else if(ymin<y && y<=ymax && zmin<=z && z <= zmax)
                {
                    if (x <= xmin || x<other.gameObject.transform.position.x)
                    {
                        Debug.Log("Case21");
                        gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                        //gameObject.transform.Rotate(new Vector3(0f, 90f, 0f));
                        //gameObject.transform.position = new Vector3(xmin, gameObject.transform.position.y, gameObject.transform.position.z); 
                    }
                    else if (x >= xmax || x<other.gameObject.transform.position.x)
                    {
                        Debug.Log("Case22");
                        gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                        //gameObject.transform.Rotate(new Vector3(0f, -90f, 0f));
                        //gameObject.transform.position = new Vector3(xmax, gameObject.transform.position.y, gameObject.transform.position.z);
                    }
                    else
                    {
                        Debug.Log("Case23");
                    }
                }
                else if(zmin<z && z<=zmax && xmin<=x && x <= xmax)
                {
                    if (y <= ymin || y < other.gameObject.transform.position.y)
                    {
                        Debug.Log("Case31");
                        gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                        //gameObject.transform.Rotate(new Vector3(90f, 0f, 0f));
                        //gameObject.transform.position = new Vector3(gameObject.transform.position.x, ymin, gameObject.transform.position.y);
                    }
                    else if (y >= ymax || y > other.gameObject.transform.position.y)
                    {
                        Debug.Log("Case32");
                        gameObject.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                        //gameObject.transform.Rotate(new Vector3(-90f, 0f, 0f));
                        //gameObject.transform.position = new Vector3(gameObject.transform.position.x, ymax, gameObject.transform.position.z);
                    }
                    else
                    {
                        Debug.Log("Case33");
                    }
                }
                else
                {
                    Debug.Log("None of the case above");
                }
                forcedRotation = gameObject.transform.rotation;
                gameObject.transform.localScale = oldScale;
            }
        }
        

    }*/


    /*private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Wall")
        {
            gameObject.transform.rotation = forcedRotation;
        }
        
    }*/

}

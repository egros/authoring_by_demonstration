﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSceneObjectsController : ButtonController
{
    public int category;
    public string createdObject;
    public GameObject prefab;
    private GameObject myCamera;

    private void Start()
    {
        myCamera = GameObject.Find("VRCamera");
        if (myCamera == null)
        {
            myCamera = GameObject.Find("FallbackObjects");
        }
    }

    public  override void OnClick()
    {
        base.OnClick();
        if (category == 11) //3D Objects
        {
            ApplicationModel.form = createdObject;
            ApplicationModel.mode = "creation";
        }
        else if (category == 2)
        {
            if (createdObject == "Link")
            {
                ApplicationModel.mode = "SetLink";
                GameObject instantiatedPrefab = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);
            }
        }
        else if (category == 21)
        {
            if (createdObject == "Fall")
            {
                ApplicationModel.mode = "InstallFall";
                GameObject instantiatedPrefab = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);

            }
        }
        else if(category == 22)
        {
            if (createdObject == "OnEnterArea")
            {
                ApplicationModel.mode = "onEnterCreation";

                GameObject instantiatedPrefab = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);

                float yRotation = myCamera.transform.rotation.eulerAngles.y - instantiatedPrefab.transform.rotation.eulerAngles.y;
                double angle = Math.PI * (myCamera.transform.rotation.eulerAngles.y / 180f);
                float cosY = (float)Math.Cos(angle);
                float sinY = (float)Math.Sin(angle);
                instantiatedPrefab.transform.position = new Vector3(myCamera.transform.position.x + sinY * 1.5f, 0f, myCamera.transform.position.z + cosY * 1.5f);
                

                
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class EditController : MonoBehaviour
{
    public SteamVR_Action_Boolean input;
    //public SteamVR_Action_Boolean switchInput;
    private bool lastState;
    private GameObject leftHand;
    private GameObject rightHand;
    private int leftHandIndex;
    private int rightHandIndex;
    private GameObject arObjects;

    // Start is called before the first frame update
    void Start()
    {
        lastState = input.state;
        leftHand = GameObject.Find("LeftHand");
        rightHand = GameObject.Find("RightHand");
        arObjects = GameObject.Find("ARCreations");

        Debug.Log("left hand tracked: "+GameObject.Find("LeftHand").GetComponent<TrackedReference>());

    }

    // Update is called once per frame
    void Update()
    {
        if (input.lastStateDown)
        {
            //Debug.Log( "Active device: "+input.activeDevice);
            //Debug.Log("Tracking device n°"+ input.trackedDeviceIndex);
            if (input.activeDevice.ToString()=="RightHand")  // The number depends on the device => need to calibrate at the begining of the session
            {
                if (ApplicationModel.mode=="creation")
                {
                    Vector3 handPosition = new Vector3(rightHand.transform.position.x, rightHand.transform.position.y, rightHand.transform.position.z);
                    ApplicationModel.handPosition = handPosition;
                    //arObjects.AddComponent<ArObject>();
                    //Debug.Log("create new Sphere");
                }
                
            }
            else
            {
                if (ApplicationModel.mode == "creation")
                {
                    ApplicationModel.mode = "simple";
                }
                else
                {
                    ApplicationModel.mode = "creation";
                }
            }
            

        }
    }
}

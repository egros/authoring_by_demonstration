﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Valve.VR;

public class PaletteSectionController : MonoBehaviour
{
    public string path;
    public List<string> acceptedFormats;
    public GameObject buttonPrefab;
    public GameObject prefabToInstanciate;
    public GameObject viewPort;
    public RectTransform content;

    private int xrf;
    private int yrf;
    private int cpt;
    private float yMax;

    // Start is called before the first frame update
    void Start()
    {
        yrf = -25;
        xrf = -69;
        int nbElements = 0;
        foreach (string format in acceptedFormats)
        {
            foreach (string file in Directory.GetFiles(path, format))
            {
                nbElements++;
            }
        }
        int nbLines = nbElements / 4;
        if(nbElements%4 > 0)
        {
            nbLines++;
        }
        float necessaryHeight = nbLines * 45;
        yMax = necessaryHeight / 2;
        content.sizeDelta = new Vector2(content.sizeDelta.x, necessaryHeight);

        foreach (string format in acceptedFormats)
        {
            foreach (string file in Directory.GetFiles(path, format, SearchOption.AllDirectories))
            {
                instantiatePrefab(file);
            }
        }
    }


    private void instantiatePrefab(string file)
    {
        /*if (xrf > 400)
        {
            yrf += 1;
            xrf = 0; //xrf % 400;
        }
        int x = xrf - 200;
        int y = 150 - yrf * 85; //* 80;
        xrf = xrf + 80;*/
        
        GameObject instantiatedPrefab = Instantiate(buttonPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
        instantiatedPrefab.transform.SetParent(transform);
        instantiatedPrefab.transform.localScale = new Vector3(1f, 1f, 1f);
        instantiatedPrefab.transform.rotation = new Quaternion(0f, 0f, 0f, 0f); ;
        //instantiatedPrefab.transform.localPosition = new Vector3(x, y-200, 0f);
        instantiatedPrefab.transform.localPosition = new Vector3(xrf, yrf, 0f);
        RectTransform buttonRect = instantiatedPrefab.GetComponent<RectTransform>();
        
        xrf = xrf + 46;
        cpt++;
        if(cpt > 3)
        {
            xrf = -69;
            yrf = yrf - 46;
            cpt = 0;
        }
        

        instantiatedPrefab.GetComponent<PaletteCreateFromLibraryButton>().newMode = "Creation";

        string[] splittedFile = file.Split('/');
        string name = splittedFile[splittedFile.Length - 1];
        name = name.Split('.')[0];
        PaletteCreateFromLibraryButton librButton = instantiatedPrefab.GetComponent<PaletteCreateFromLibraryButton>();
        librButton.setButton( name.Split('\\')[0] , name.Split('\\')[1]);
        UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
        if (assetToGet != null)
        {
            // Solution found:
            int counter = 0;
            Texture2D texture = null;
            while (texture == null && counter < 75)
            {
                texture = UnityEditor.AssetPreview.GetAssetPreview(assetToGet);
                counter++;
                System.Threading.Thread.Sleep(15);
            }
            //UnityEditor.AssetPreview.GetMiniThumbnail(assetToGet);
            Material thumbnail = new Material(Shader.Find("UI/Default"));
            thumbnail.mainTexture = texture;
            thumbnail.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;

            instantiatedPrefab.GetComponent<PaletteButtonController>().material = thumbnail;
            librButton.personalAsset = assetToGet;
            librButton.prefab = prefabToInstanciate;
            librButton.viewPort = viewPort;
        }
        else
        {
            Debug.Log("Couldn't load the asset");
        }
        
        
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteCreateButton : PaletteButtonController
{
    public GameObject prefab;
    public string newMode;
    private GameObject myCamera;
    private GameObject arObjectsParent;
    private InteractionArObjectsRecorder arRecorder;

    protected override void Start()
    {
        base.Start();
        arObjectsParent = GameObject.Find("ARCreations");
        myCamera = GameObject.Find("VRCamera");
        arRecorder = GameObject.FindObjectOfType<InteractionArObjectsRecorder>();

    }

    public override void OnClick()
    {
        if (arObjectsParent == null)
        {
            arObjectsParent = GameObject.Find("ARCreations");
        }
        base.OnClick();
        if (ApplicationModel.mode != "Test" || availableInTestMode)
        {
            GeometryAnalyzer geoAnalyzer = ScriptableObject.CreateInstance<GeometryAnalyzer>();
            GameObject instantiatedPrefab = Instantiate(prefab, new Vector3(2f, 1f, -1.25f), Quaternion.identity);
            instantiatedPrefab.transform.SetParent(myCamera.transform);
            minNmax extremities = geoAnalyzer.getLocalMinNmax(instantiatedPrefab);
            Vector3 worldMin = instantiatedPrefab.transform.TransformPoint(extremities.min);
            Vector3 worldMax = instantiatedPrefab.transform.TransformPoint(extremities.max);
            float dist = Mathf.Abs(worldMax.z - worldMin.z) / 2 + 0.6f;
            instantiatedPrefab.transform.localPosition = new Vector3(0f, 0f, dist);
            instantiatedPrefab.transform.SetParent(arObjectsParent.transform);
            instantiatedPrefab.layer = 9;

            ArObject arComp = instantiatedPrefab.GetComponent<ArObject>();
            if (arComp != null)
            {
                if (ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "ArObjects")
                {
                    arRecorder.addShow(instantiatedPrefab);
                    arComp.setIsVisibleAtStart(false);
                }
                else
                {
                    arComp.setIsVisibleAtStart(true);
                }
            }
            

        }
            

    }
}

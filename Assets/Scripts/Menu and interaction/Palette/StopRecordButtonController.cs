﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopRecordButtonController : PaletteButtonController
{

    protected override void Start()
    {
        base.Start();
    }


    public override void OnClick()
    {
        base.OnClick();
        ApplicationModel.recordingStatus = "Analyze";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteWimButton : PaletteButtonController
{
    private WimController wim;

    public override void OnClick()
    {
        base.OnClick();
        if (wim == null)
        {
            wim = GameObject.FindObjectOfType<WimController>();
        }
        wim.openClose();
    }
}

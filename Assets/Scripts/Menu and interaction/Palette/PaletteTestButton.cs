﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

struct Association
{
    public GameObject arObj;
    public EventIconController[] icons;

    public Association(GameObject ar, EventIconController[] eventIcons)
    {
        arObj = ar;
        icons = eventIcons;
    }
}

public class PaletteTestButton : PaletteButtonController
{
    public Material pauseMaterial;
    public bool debugMode;
    private List<Association> associateBack;
    private Camera vrCamera;
    private SceneHandler sceneHandler;
    private ResizingLaserController resizeLaser;

    protected override void Start()
    {
        base.Start();
        vrCamera = GameObject.Find("VRCamera").GetComponent<Camera>();
        sceneHandler = GameObject.FindObjectOfType<SceneHandler>();
        resizeLaser = GameObject.FindObjectOfType<ResizingLaserController>();
    }

    public override void OnClick()
    {
        base.OnClick();
        if (ApplicationModel.mode != "Test")
        {
            ApplicationModel.mode = "Test";
            resizeLaser.switchLaser(false);
            image.material = pauseMaterial;
            SimpleAttach[] simpleAttachs = GameObject.FindObjectsOfType<SimpleAttach>();
            foreach (SimpleAttach s in simpleAttachs)
            {
                s.savePreTest();
            }

            associateBack = new List<Association>();
            ArObject[] arObjects = GameObject.FindObjectsOfType<ArObject>();
            foreach(ArObject ar in arObjects)
            {
                if (!ar.getIsVisibleAtStart())
                {
                    EventIconController[] eventIcons = ar.gameObject.GetComponentsInChildren<EventIconController>();
                    foreach(EventIconController e in eventIcons)
                    {
                        e.gameObject.transform.SetParent(ar.gameObject.transform.parent);
                    }
                    associateBack.Add(new Association(ar.gameObject, eventIcons));
                    ar.gameObject.SetActive(false);
                    ar.setTransparency(true);
                }
                ar.hideAttributes();
            }
            if (!debugMode)
            {
                vrCamera.cullingMask = ~(1 << LayerMask.NameToLayer("EventIcon"));
                Interactable[] interactables = GameObject.FindObjectsOfType<Interactable>();
                foreach(Interactable i in interactables)
                {
                    i.highlightOnHover = false;
                }
            }

        }
        else
        {
            ApplicationModel.mode = "simple";
            image.material = material;
            vrCamera.cullingMask = -1;
            //cancel All animations
            EventIconController[] eventIcons = GameObject.FindObjectsOfType<EventIconController>();
            foreach(EventIconController e in eventIcons)
            {
                e.resetAnimation();
            }
            SimpleAttach[] simpleAttachs = GameObject.FindObjectsOfType<SimpleAttach>();
            foreach (SimpleAttach s in simpleAttachs)
            {
                if (s.gameObject.GetComponent<ArObject>() != null)
                {
                    s.resetPreTest();
                }
            }
            foreach(Association asso in associateBack)
            {
                asso.arObj.SetActive(true);
                asso.arObj.GetComponent<ArObject>().setTransparency(false);
                foreach(EventIconController e in asso.icons)
                {
                    e.gameObject.transform.SetParent(asso.arObj.transform);
                }
            }
            if (!debugMode)
            {
                Interactable[] interactables = GameObject.FindObjectsOfType<Interactable>();
                foreach (Interactable i in interactables)
                {
                    i.highlightOnHover = true;
                }
            }
            CircleDispatcher[] dispatchers = GameObject.FindObjectsOfType<CircleDispatcher>();
            foreach(CircleDispatcher dispatch in dispatchers)
            {
                dispatch.haveToUpdate();
            }
        }
    }
}

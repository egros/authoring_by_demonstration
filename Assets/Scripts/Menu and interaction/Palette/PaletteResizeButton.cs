﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteResizeButton : PaletteButtonController
{
    private ResizingLaserController resizeCntr;

    protected override void Start()
    {
        base.Start();
        resizeCntr = GameObject.FindObjectOfType<ResizingLaserController>();
        resizeCntr.switchLaser(false);
    }

    public override void OnClick()
    {
        base.OnClick();
        resizeCntr.inverseLaser();
    }

}

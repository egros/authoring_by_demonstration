﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteARPhoneButton : PaletteButtonController
{

    private GameObject arPhone;
    private bool isVisible;
    private Camera vrCamera;
    private Camera phoneCamera;
    private PaletteController paletteController;
    private GameObject nonDominantHand;

    private List<Association> associateBack;

    protected override void Start()
    {
        base.Start();
        nonDominantHand = GameObject.Find("LeftHand");
        arPhone = GameObject.Find("ARSmartPhone");
        arPhone.SetActive(false);
        isVisible = false;
        vrCamera = GameObject.Find("VRCamera").GetComponent<Camera>();
        phoneCamera = arPhone.GetComponentInChildren<Camera>();
        phoneCamera.cullingMask = ~(1 << LayerMask.NameToLayer("EventIcon"));
        paletteController = GameObject.FindObjectOfType<PaletteController>();//GameObject.Find("Palette").GetComponent<PaletteController>();
    }

    private void OnEnable()
    {
        if (arPhone == null)
        {
            arPhone = GameObject.Find("ARSmartPhone");
        }
        if (phoneCamera == null)
        {
            phoneCamera = arPhone.GetComponentInChildren<Camera>();
            phoneCamera.cullingMask = ~(1 << LayerMask.NameToLayer("EventIcon"));
        }
        
    }

    public override void OnClick()
    {
        base.OnClick();
        if(arPhone == null)
        {
            arPhone = GameObject.Find("ARSmartPhone");
        }
        if (vrCamera == null)
        {
            vrCamera = GameObject.Find("VRCamera").GetComponent<Camera>();
        }
        if (isVisible)
        {
            arPhone.transform.SetParent(null);
            arPhone.SetActive(false);
            isVisible = false;
            vrCamera.cullingMask = -1;

            foreach (Association asso in associateBack)
            {
                asso.arObj.SetActive(true);
                foreach (EventIconController e in asso.icons)
                {
                    e.gameObject.transform.SetParent(asso.arObj.transform);
                }
            }
            //ApplicationModel.mode = "simple";
        }
        else
        {
            arPhone.SetActive(true);
            arPhone.transform.SetParent(nonDominantHand.transform);
            arPhone.transform.localPosition = new Vector3(-0.139f, -0.0109f, -0.0973f);
            arPhone.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            arPhone.transform.Rotate(new Vector3(0f, 90f, 30f));
            isVisible = true;
            //vrCamera.cullingMask = ~(1 << LayerMask.NameToLayer("ARObjectLayer") << LayerMask.NameToLayer("EventIcon"));
            int layermask = 1 << LayerMask.NameToLayer("ARObjectLayer");
            layermask = layermask | (1 << LayerMask.NameToLayer("EventIcon"));
            vrCamera.cullingMask = ~layermask;
            paletteController.openClosePalette();

            associateBack = new List<Association>();
            ArObject[] arObjects = GameObject.FindObjectsOfType<ArObject>();
            foreach (ArObject ar in arObjects)
            {
                if (!ar.getIsVisibleAtStart())
                {
                    EventIconController[] eventIcons = ar.gameObject.GetComponentsInChildren<EventIconController>();
                    foreach (EventIconController e in eventIcons)
                    {
                        e.gameObject.transform.SetParent(ar.gameObject.transform);
                    }
                    associateBack.Add(new Association(ar.gameObject, eventIcons));
                    ar.gameObject.SetActive(false);
                }
                ar.hideAttributes();
            }
            //ApplicationModel.mode = "Test";
        }
    }
}

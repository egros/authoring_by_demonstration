﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalettePointerButton : PaletteButtonController
{

    public int mode;

    private SceneHandler sceneHandler;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        sceneHandler =  GameObject.Find(ApplicationModel.dominantHand).GetComponent<SceneHandler>();
    }

    public override void OnClick()
    {
        base.OnClick();
        if (ApplicationModel.mode != "Test" || availableInTestMode)
        {
            sceneHandler.switchMode(mode);
        }
    }


}

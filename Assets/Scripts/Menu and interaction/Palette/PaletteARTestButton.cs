﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PaletteARTestButton : PaletteButtonController

{
    public string arSceneName;
    public string vrScene;
    public ResizingLaserController resizeLaser;

    protected override void Start()
    {
        base.Start();
        ApplicationModel.vrSceneName = vrScene;
        resizeLaser = GameObject.FindObjectOfType<ResizingLaserController>();
    }

    public override void OnClick()
    {
        base.OnClick();
        // Switch off resizing laser
        resizeLaser.switchLaser(false);
        // Save
        string path = Application.streamingAssetsPath + "/Sauvegardes/" + ApplicationModel.sceneToTest;
        SaveScene saving = ScriptableObject.CreateInstance<SaveScene>();
        saving.writeOnFile(path);
        // Jump to scene
        SceneManager.LoadScene(arSceneName);
    }
}

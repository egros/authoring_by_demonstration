﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaletteButtonController : MonoBehaviour
{
    public Material material;
    public bool availableInTestMode;
    protected Image image;
    protected Button button;
    public string helpFunction;
    protected Text textDisplay;
    protected GameObject panel;
    protected string unavailableMessage;
    private Camera vrCamera;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        vrCamera = GameObject.Find("VRCamera").GetComponent<Camera>();
        unavailableMessage = "Not available in mode TEST. \n Stop test mode before using it.";
        button = GetComponent<Button>();
        image = GetComponent<Image>();

        if (material != null)
        {
            image.material = material;
        }
        textDisplay = gameObject.GetComponentInChildren<Text>();
        textDisplay.text = helpFunction;
        panel = gameObject.transform.GetChild(0).gameObject;
        panel.SetActive(false);
    }

    public virtual  void OnHover(bool onHover)
    {
        if (button != null)
        {
            ColorBlock colors = button.colors;
            if (!onHover)
            {
                colors.normalColor = new Color32(255, 255, 255, 255);
                panel.SetActive(false);
            }
            else
            {
                colors.normalColor = new Color32(86, 31, 174, 255);
                if (ApplicationModel.mode == "Test" && !availableInTestMode)
                {
                    textDisplay.text = unavailableMessage;
                    panel.SetActive(true);
                }
                else
                {
                    textDisplay.text = helpFunction;
                    if (helpFunction != "")
                    {
                        panel.SetActive(true);
                    }
                }
                

            }
            button.colors = colors;
        }
        
        //GetComponent<Image>().color = new Color(236, 146, 146, 255);
    }


    public virtual void OnClick()
    {
        //Debug.Log("Clicked "+helpFunction);
        
    }

    public void OnTriggerEnter(Collider other)
    {
        Vector3 screenPoint = vrCamera.WorldToViewportPoint(transform.position);
        bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
        if (other.gameObject.name == "finger_index_2_r" && onScreen)
        {
            OnClick();
        }
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public struct ObjectDescription
{
    public string mainType;
    public int id;
    public string subtype;
    public float[] position;
    public float[] size;
    public float[] rotation;
    public bool visible;
    public float[] color;

    public ObjectDescription(string typeObj, int identifiant, string subtypeobj, float[] pos, float[]rot, bool isVisible, float[] coloration, float[] objSize= null)
    {
        mainType = typeObj;
        id = identifiant;
        subtype = subtypeobj;
        position = pos;
        rotation = rot;
        visible = isVisible;
        color = coloration;
        if (objSize == null)
        {
            size = new float[] { 0f, 0f, 0f };
        }
        else
        {
            size = objSize;
        }
    }
}

[Serializable]
public struct EventDescription
{
    public string mainType;
    public int id;
    public int target;
    public int[] animations;
    public int previousIcon;
    public int nextIcon;
    public float[] color;
    public int holdingHand;
    public string trajectory;
    public string rotations;
    public int collided;
    public bool followDirection;

    public EventDescription(string mType, int identifiant, int targ, int[] anim, int previous = -1, int nextI = -1, float[] coloration = null, int holdingH = -1, string traj="", string rot="", int collidedObj=-1, bool follow=false)
    {
        mainType = mType;
        id = identifiant;
        target = targ;
        animations = anim;
        previousIcon = previous;
        nextIcon = nextI;
        color = coloration;
        holdingHand = holdingH;
        trajectory = traj;
        rotations = rot;
        collided = collidedObj;
        followDirection = follow;
    }
}


public class PaletteSaveButton : PaletteButtonController
{
    private string path;
    private Material validSavedMat;

    protected override void Start()
    {
        base.Start();
        path = Application.streamingAssetsPath + "/Sauvegardes/"+helpFunction;
        validSavedMat = Resources.Load<Material>("Materials\\SavedMaterial");

    }

    public override void OnClick()
    {
        base.OnClick();
        writeOnFile();
    }

    public void writeOnFile()
    {
        // source: https://videlais.com/2021/02/25/using-jsonutility-in-unity-to-save-and-load-game-data/
        SaveScene saving = ScriptableObject.CreateInstance<SaveScene>();
        saving.writeOnFile(path);
        StartCoroutine(MeshCoroutine());
    }

    IEnumerator MeshCoroutine()
    {
        image.material = validSavedMat;
        yield return new WaitForSeconds(1);
        image.material = material;
    }
}

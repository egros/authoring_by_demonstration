﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteController : MonoBehaviour
{
    private GameObject nonDominantHand;
    private GameObject palette;
    private GameObject opener;
    private MeshRenderer openerRenderer;
    private Quaternion openerRotation;
    private Vector3 openerPosition;

    private float selectorY;
    public GameObject selector1;
    public GameObject selector2;

    public List<GameObject> panels;
    public bool paletteIsOpened;
    public Material openMat;
    public Material closeMat;

    // Start is called before the first frame update
    /*void Start()
    {
        
    }*/

    public void OnEnable()
    {
        paletteIsOpened = true;

        palette = GameObject.Find("PaletteCanvas");
        opener = GameObject.Find("Open_Close_Cube");
        openerRotation = opener.transform.localRotation;
        openerPosition = opener.transform.localPosition;

        nonDominantHand = GameObject.Find(ApplicationModel.nonDominantHand);

        // Get menu and hide it

        palette.transform.SetParent(nonDominantHand.transform);
        palette.transform.localPosition = new Vector3(0f, 0.1f, -0.12f);
        palette.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        palette.transform.Rotate(new Vector3(60f, 0f, 0f));

        selectorY = selector1.transform.localPosition.y;

        openPanel(panels[0]);
    }



    public void openPanel(GameObject panelToOpen)
    {
        foreach(GameObject panel in panels)
        {
            if (panel.name == panelToOpen.name)
            {
                panel.SetActive(true);
            }
            else
            {
                panel.SetActive(false);
            }
        }
    }

    public void openClosePalette()
    {
        if (openerRenderer == null)
        {
            openerRenderer = opener.GetComponent<MeshRenderer>();
        }
        if (paletteIsOpened)
        {
            opener.transform.parent = nonDominantHand.transform;
            opener.transform.localPosition = new Vector3(-0.03f, 0.08f, -0.14f);
            opener.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            opener.transform.Rotate(new Vector3(90f, 0f, 0f));
            palette.SetActive(false);
            openerRenderer.material = openMat;
        }
        else
        {
            palette.SetActive(true);
            opener.transform.parent = palette.transform;
            opener.transform.localPosition = openerPosition;
            opener.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            openerRenderer.material = closeMat;
        }
        paletteIsOpened = !paletteIsOpened;
    }
}

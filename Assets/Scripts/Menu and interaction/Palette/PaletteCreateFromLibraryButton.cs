﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PaletteCreateFromLibraryButton : PaletteButtonController
{
    public GameObject prefab;
    public UnityEngine.Object personalAsset;
    public string newMode;
    public GameObject viewPort;
    private SteamVR_Action_Boolean input;
    private BoxCollider collider;

    private string elementType;

    private GameObject myCamera;
    private GameObject arObjectsParent;
    private InteractionArObjectsRecorder arRecorder;

    private GameObject demo;

    protected override void Start()
    {
        base.Start();
        arObjectsParent = GameObject.Find("ARCreations");
        myCamera = GameObject.Find("VRCamera");
        availableInTestMode = false;
        arRecorder = GameObject.FindObjectOfType<InteractionArObjectsRecorder>();
        collider = GetComponent<BoxCollider>();

        if (elementType == "Images")
        {
            demo = Instantiate(prefab, new Vector3(2f, 1f, -1.25f), Quaternion.identity);
            Material imageMat = new Material(Shader.Find("Standard"));
            // smoothness
            imageMat.SetFloat("_Glossiness", 0f);
            //transparency
            imageMat.SetFloat("_Mode", 2);
            imageMat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            imageMat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            imageMat.SetInt("_ZWrite", 0);
            imageMat.DisableKeyword("_ALPHATEST_ON");
            imageMat.EnableKeyword("_ALPHABLEND_ON");
            imageMat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            imageMat.renderQueue = 3000;
            Texture2D imageTexture = personalAsset as Texture2D;
            imageMat.mainTexture = imageTexture;
            demo.GetComponentInChildren<Image>().material = imageMat;
        }
        else if (elementType == "3DProps")
        {
            GameObject pa = (GameObject)personalAsset;
            demo = Instantiate(pa, new Vector3(2f, 1f, -1.25f), Quaternion.identity);
        }
        GeometryAnalyzer geoAnalyzer = new GeometryAnalyzer();
        minNmax extremities = geoAnalyzer.getLocalMinNmax(demo);
        Vector3 worldMin = demo.transform.TransformPoint(extremities.min);
        Vector3 worldMax = demo.transform.TransformPoint(extremities.max);
        float dist = Mathf.Abs(worldMax.z - worldMin.z) / 2 + 0.6f;
        demo.transform.SetParent(myCamera.transform);
        demo.transform.localPosition = new Vector3(0f, 0f, Mathf.Max(dist, 1f));
        demo.transform.LookAt(myCamera.transform);
        demo.transform.Rotate(new Vector3(0f, 180f, 0f));
        demo.SetActive(false);
    }

    private void Update()
    {
        Vector2 pos = viewPort.transform.InverseTransformPoint(transform.position);
        if(pos.y > 0 || pos.y < -200)
        {
            collider.enabled = false;
        }
        else
        {
            collider.enabled = true;
        }
    }

    private void OnEnable()
    {
        if (arObjectsParent == null)
        {
            arObjectsParent = GameObject.Find("ARCreations");
        }
        if (myCamera == null)
        {
            myCamera = GameObject.Find("VRCamera");
        }
    }

    public void showDemo(bool show)
    {
        demo.SetActive(show);
    }

    public override void OnClick()
    {
        base.OnClick();
        showDemo(false);
        if (ApplicationModel.mode != "Test" || availableInTestMode)
        {
            GameObject instantiatedPrefab;// = new GameObject();
            if (elementType == "Images")
            {
                instantiatedPrefab = Instantiate(prefab, new Vector3(2f, 1f, -1.25f), Quaternion.identity);
                Material imageMat = new Material(Shader.Find("Standard"));//"Unlit/Texture"));

                // smoothness
                imageMat.SetFloat("_Glossiness", 0f);
                //transparency
                imageMat.SetFloat("_Mode", 2);
                imageMat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                imageMat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                imageMat.SetInt("_ZWrite", 0);
                imageMat.DisableKeyword("_ALPHATEST_ON");
                imageMat.EnableKeyword("_ALPHABLEND_ON");
                imageMat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                imageMat.renderQueue = 3000;


                Texture2D imageTexture = personalAsset as Texture2D;
                imageMat.mainTexture = imageTexture;
                instantiatedPrefab.GetComponentInChildren<Image>().material = imageMat;
                ArObject arComponent = instantiatedPrefab.GetComponent<ArObject>();
                arComponent.mainType = elementType;
                arComponent.subtype = helpFunction;

            }
            else if (elementType == "3DProps")
            {
                GameObject pa = (GameObject)personalAsset;
                instantiatedPrefab = Instantiate(pa, new Vector3(2f, 1f, -1.25f), Quaternion.identity);
                //instantiatedPrefab.AddComponent<CreateCollider>();
                if (instantiatedPrefab.GetComponent<Collider>() == null)
                {
                    ColliderMaker colMak = instantiatedPrefab.AddComponent<ColliderMaker>();
                    colMak.makeCollider();
                }
                ArObject arComponent = instantiatedPrefab.AddComponent<ArObject>();
                arComponent.mainType = elementType;
                arComponent.subtype = helpFunction;
                instantiatedPrefab.tag = "ArObjects";
                Interactable interact = instantiatedPrefab.AddComponent<Interactable>();
                interact.hideControllerOnAttach = false;
                instantiatedPrefab.AddComponent<SimpleAttach>();
                Rigidbody rigidBody = instantiatedPrefab.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
                rigidBody.useGravity = false;
                instantiatedPrefab.AddComponent<CircleDispatcher>();
            }
            else
            {
                instantiatedPrefab = new GameObject();
            }
            GeometryAnalyzer geoAnalyzer = new GeometryAnalyzer();
            minNmax extremities = geoAnalyzer.getLocalMinNmax(instantiatedPrefab);
            Vector3 worldMin = instantiatedPrefab.transform.TransformPoint(extremities.min);
            Vector3 worldMax = instantiatedPrefab.transform.TransformPoint(extremities.max);
            float dist = Mathf.Abs(worldMax.z - worldMin.z) / 2 + 0.6f;

            instantiatedPrefab.transform.SetParent(myCamera.transform);
            instantiatedPrefab.transform.localPosition = new Vector3(0f, 0f, dist);
            instantiatedPrefab.transform.LookAt(myCamera.transform);
            instantiatedPrefab.transform.Rotate(new Vector3(0f, 180f, 0f));
            instantiatedPrefab.transform.SetParent(arObjectsParent.transform);
            setLayer(instantiatedPrefab);

            if (ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "ArObjects")
            {
                arRecorder.addShow(instantiatedPrefab);
                instantiatedPrefab.GetComponent<ArObject>().setIsVisibleAtStart(false);
            }
            else
            {
                instantiatedPrefab.GetComponent<ArObject>().setIsVisibleAtStart(true);
            }
        }
            
    }

    private void setLayer(GameObject obj)
    {
        obj.layer = 9;
        foreach(Transform child in obj.transform)
        {
            setLayer(child.gameObject);
        }
    }

    public void setButton(string typeEl, string fileName)
    {
        //Debug.Log("Type: " + typeEl + "  name: " + fileName);
        elementType = typeEl;
        helpFunction = fileName;
        if (textDisplay == null)
        {
            textDisplay = gameObject.GetComponentInChildren<Text>();
        }
        textDisplay.text = helpFunction;
    }

    public override void OnHover(bool onHover)
    {
        base.OnHover(onHover);
        //showDemo(onHover);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteRestoreButton : PaletteButtonController
{
    private ExtractFromFile extractor;
    private bool needToSetLinks;

    protected override void Start()
    {
        base.Start();
        extractor = ScriptableObject.CreateInstance<ExtractFromFile>();
        needToSetLinks = false;
        //string testP = "[[7.96377,1.301855,-6.063476], [7.93174,1.299113,-6.056077], [7.883405,1.316283,-6.032414], [7.797271,1.34148,-5.979432], [7.69108,1.353771,-5.90449], [7.585466,1.344401,-5.818911], [7.496241,1.33477,-5.701578], [7.449083,1.323096,-5.612659], [7.427327,1.319245,-5.529539], [7.432408,1.329658,-5.50266], [7.440232,1.337081,-5.501029]]";
        //string testR = "[[0.9941128,-0.01148174,0.1074506,-0.007896373], [0.9905475,-0.01481131,0.1361355,-0.007949203], [0.9847566,0.001035962,0.173904,-0.003274977], [0.9690369,0.02759851,0.2452868,0.006331176], [0.9401337,0.04562462,0.3374409,0.01416342], [0.9036731,0.05606439,0.4229376,0.03681619], [0.8467174,0.06240696,0.5266182,0.04299307], [0.7984903,0.06031961,0.5972306,0.0457202], [0.742063,0.044939,0.6682563,0.02750462], [0.7210139,0.04635054,0.6912408,0.01329115], [0.7185503,0.05074294,0.6935768,0.007858069], [0.7185503,0.05074294,0.6935768,0.007858069], [0.7185503,0.05074294,0.6935768,0.007858069]]";
        //extractor.turnStringToTransformList(testP, testR);
    }

    public override void OnClick()
    {
        base.OnClick();
        extractor.readFile(helpFunction);
        needToSetLinks = true;
    }

    public void Update()
    {
        if (needToSetLinks)
        {
            needToSetLinks = false;
            extractor.setLinksOnUpdate();
        }
    }
}

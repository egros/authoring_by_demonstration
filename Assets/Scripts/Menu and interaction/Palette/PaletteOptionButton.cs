﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteOptionButton : PaletteButtonController
{
    public GameObject panelToShow;
    //public GameObject subSelector;
    private GameObject selector;

    private PaletteController palette;

    protected override void Start()
    {
        base.Start();
        palette = GameObject.Find("Palette").GetComponent<PaletteController>();
        selector = GameObject.Find("Selector1");
    }

    public override void OnClick()
    {
        base.OnClick();
        if (ApplicationModel.mode != "Test" || availableInTestMode)
        {
            palette.openPanel(panelToShow);
            //subSelector.transform.localPosition = new Vector3(subSelector.transform.localPosition.x, 180f, 0f);
            selector.transform.localPosition = new Vector3(selector.transform.localPosition.x, transform.localPosition.y, 0f);
        }
            
    }
}

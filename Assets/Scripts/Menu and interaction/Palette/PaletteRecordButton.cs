﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteRecordButton : PaletteButtonController
{
    public string recordTarget;

    protected override void Start()
    {
        base.Start();
    }

    public override void OnClick()
    {
        base.OnClick();
        if (ApplicationModel.mode != "Test" || availableInTestMode)
        {
            ApplicationModel.recordingStatus = "On";
            ApplicationModel.recordedTarget = recordTarget;
        }
    }
}

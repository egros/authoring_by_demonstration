﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteSectionButton : PaletteButtonController
{
    public GameObject sectionToShow;
    //public GameObject parentPanel;
    private GameObject selector;

    private PalettePanelController panelController;

    protected override void Start()
    {
        base.Start();
        panelController = GetComponentInParent < PalettePanelController>();
        selector = GameObject.Find("Selector2");
        //panelController = parentPanel.GetComponent<PalettePanelController>();
    }

    public override void OnClick()
    {
        base.OnClick();
        if(ApplicationModel.mode!="Test" || availableInTestMode)
        {
            panelController.openSection(sectionToShow);
            selector.transform.localPosition = new Vector3(selector.transform.localPosition.x, transform.localPosition.y, 0f);
        }
        
    }

    public void OnEnable()
    {
        if (selector == null)
        {
            selector = GameObject.Find("Selector2");
        }
    }

    public void Update()
    {
        if (sectionToShow.activeSelf)
        {
            selector.transform.localPosition = new Vector3(selector.transform.localPosition.x, transform.localPosition.y, 0f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalettePanelController : MonoBehaviour
{

    public List<GameObject> sections;

    // Start is called before the first frame update
    void Start()
    {
        openSection(sections[0]);
    }

    public void openSection(GameObject sectionToOpen)
    {
        foreach(GameObject section in sections)
        {
            if(section.name == sectionToOpen.name)
            {
                section.SetActive(true);
            }
            else
            {
                section.SetActive(false);
            }
        }
    }

}

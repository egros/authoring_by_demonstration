﻿using DitzelGames.FastIK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;
using Valve.VR.InteractionSystem;

public class SimpleAttach : MonoBehaviour
{
    
    protected Interactable interactable;
    protected InteractionControllerRecorder dominantHandRecorder;
    protected InteractionControllerRecorder nonDominantHandRecorder;
    protected GameObject initialParent;
    private GameObject spine;
    protected SteamVR_Action_Boolean input;
    protected bool isAttached;
    protected string holdingHand;

    private FastIKLook lookControl;

    private GameObject selectorReferent;
    private bool selectionIsAttached;
    private GameObject arParent;
    private List<EventIconController> grabEventsToDetect;
    private List<EventIconController> dropEventsToDetect;
    private List<EventIconController> linkedDragEvents;
    //private List<Vector3> dragPositions;

    // Return to pre-test organization
    protected Vector3 formerPosition;
    protected Quaternion formerRotation;
    protected bool formerIsVisible;
    public bool shouldHighlightOnHover;

    // Start is called before the first frame update
    public virtual void Start()
    {
        input = SteamVR_Input.GetBooleanAction("GrabGrip");
        interactable = GetComponent<Interactable>();
        interactable.highlightOnHover = shouldHighlightOnHover;
        dominantHandRecorder = GameObject.Find(ApplicationModel.dominantHand).GetComponent<InteractionControllerRecorder>();
        nonDominantHandRecorder = GameObject.Find(ApplicationModel.nonDominantHand).GetComponent<InteractionControllerRecorder>();
        if (nonDominantHandRecorder == null)
        {
            Debug.Log("NonDominantHand recorder is null");
        }
        spine = GameObject.Find("spine.006");
        if (spine != null)
        {
            lookControl = spine.GetComponent<FastIKLook>();
        }
        selectorReferent = GameObject.Find("SelectorReference");
        selectionIsAttached = false;
        arParent = GameObject.Find("ARCreations");

        if (grabEventsToDetect == null)
        {
            grabEventsToDetect = new List<EventIconController>();
        }
        if (dropEventsToDetect == null)
        {
            dropEventsToDetect = new List<EventIconController>();
        }
        if (linkedDragEvents == null)
        {
            linkedDragEvents = new List<EventIconController>();
        }
        //dragPositions = new List<Vector3>();

    }

    public virtual void Update()
    {
        if (spine == null)
        {
            spine = GameObject.Find("spine.006");
            if (spine != null)
            {
                lookControl = spine.GetComponent<FastIKLook>();
            }
        }
        if (input.lastStateUp && isAttached && input.activeDevice.ToString() == holdingHand)
        {
            if(ApplicationModel.recordingStatus == "On")
            {
                //if (transform.parent.gameObject.name == ApplicationModel.dominantHand && ApplicationModel.recordedTarget == ApplicationModel.dominantHand)
                if (ApplicationModel.recordedTarget == transform.parent.gameObject.name || ApplicationModel.recordedTarget == "BothHands")
                {
                    if(transform.parent.gameObject.name == ApplicationModel.dominantHand)
                    {
                        dominantHandRecorder.dropping(gameObject);
                    }
                    else if(transform.parent.gameObject.name == ApplicationModel.nonDominantHand)
                    {
                        nonDominantHandRecorder.dropping(gameObject);
                    }
                    
                }
                if (ApplicationModel.recordedTarget == "Puppet" && gameObject.name == "WoodBody")
                {
                    GameObject.Find("SphereRightHand").GetComponent<MoveRecorder>().restartMonitoring();
                    GameObject.Find("SphereLeftHand").GetComponent<MoveRecorder>().restartMonitoring();
                }
            }

            if (ApplicationModel.mode == "Test")
            {
                // Test drop events
                foreach (EventIconController e in dropEventsToDetect)
                {
                    e.trigerDone();
                }

            }

            if (initialParent == null)
            {
                transform.SetParent(null);
            }
            else
            {
                if (selectionIsAttached)
                {
                    selectorReferent.transform.SetParent(arParent.transform);
                    selectionIsAttached = false;
                }
                else
                {
                    transform.SetParent(initialParent.transform);
                }
                
            }
            if (gameObject.name == "WoodBody" && lookControl != null)
            {
                lookControl.enabled = true;
                lookControl.resetVariables();
            }

            if (gameObject.GetComponent<JackConnectorController>() != null)
            {
                gameObject.GetComponent<JackConnectorController>().onRelease();
            }
            holdingHand = null;
            isAttached = false;
        }
    }

    public void OnHandHoverEnd(Hand hand)
    {
        hand.HideGrabHint();
    }

    public bool canBeGrabbedInTest()
    {
        bool testGrab = false;
        foreach (EventIconController e in grabEventsToDetect)
        {
            if (e != null)
            {
                testGrab = true;
                break;
            }
        }
        if (!testGrab)
        {
            foreach (EventIconController e in dropEventsToDetect)
            {
                if (e != null)
                {
                    testGrab = true;
                    break;
                }
            }
        }
        if (!testGrab)
        {
            foreach (EventIconController e in linkedDragEvents)
            {
                if (e != null)
                {
                    testGrab = true;
                    break;
                }
            }
        }
        return testGrab;
    }

    public virtual void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();

        //GRAB THE OBJECT
        if (ApplicationModel.mode=="simple" || (ApplicationModel.mode=="Test"))
        {
            if(grabType != GrabTypes.None && !isAttached)//grab
            {
                if(ApplicationModel.mode!="Test" || canBeGrabbedInTest())
                {
                    if (ApplicationModel.recordingStatus == "On")
                    {
                        //if (hand.handType.ToString() == ApplicationModel.dominantHand && ApplicationModel.recordedTarget == hand.gameObject.name)
                        if (ApplicationModel.recordedTarget == hand.gameObject.name || ApplicationModel.recordedTarget == "BothHands")
                        {
                            if (hand.gameObject.name == ApplicationModel.dominantHand)
                            {
                                dominantHandRecorder.grabbing(gameObject);
                            }
                            else if (hand.gameObject.name == ApplicationModel.nonDominantHand)
                            {
                                nonDominantHandRecorder.grabbing(gameObject);
                            }
                        }
                        if (ApplicationModel.recordedTarget == "Puppet" && gameObject.name == "WoodBody")
                        {
                            GameObject.Find("SphereRightHand").GetComponent<MoveRecorder>().stopMonitoring();
                            GameObject.Find("SphereLeftHand").GetComponent<MoveRecorder>().stopMonitoring();
                        }
                    }

                    // Trigger grab events on test mode
                    if (ApplicationModel.mode == "Test")
                    {
                        foreach (EventIconController e in grabEventsToDetect)
                        {
                            e.trigerDone();
                        }
                    }

                    if (transform.parent != null)
                    {
                        initialParent = transform.parent.gameObject;
                    }
                    else
                    {
                        initialParent = null;
                    }
                    if (gameObject.name == "WoodBody" && lookControl != null)
                    {
                        lookControl.enabled = false;
                    }
                    holdingHand = hand.gameObject.name;
                    if (GetComponent<ArObject>() != null)
                    {
                        ArObject arOb = GetComponent<ArObject>();
                        if (arOb.getIsSelected())
                        {
                            selectorReferent.transform.SetParent(hand.transform);
                            selectionIsAttached = true;
                        }
                        else
                        {
                            transform.SetParent(hand.transform);
                            selectionIsAttached = false;
                        }
                    }
                    else
                    {
                        transform.SetParent(hand.transform);
                        selectionIsAttached = false;
                    }
                    isAttached = true;
                }
            }

        }
    }

    public void addGrabEvent(EventIconController grabEvent)
    {
        if(grabEventsToDetect == null)
        {
            grabEventsToDetect = new List<EventIconController>();
        }
        grabEventsToDetect.Add(grabEvent);
    }

    public void addDropEvent(EventIconController dropEvent)
    {
        if (dropEventsToDetect == null)
        {
            dropEventsToDetect = new List<EventIconController>();
        }
        dropEventsToDetect.Add(dropEvent);
    }

    public void addDragEvent(EventIconController e)
    {
        if (linkedDragEvents == null)
        {
            linkedDragEvents = new List<EventIconController>();
        }
        if (e == null)
        {
            Debug.Log("Added event == null");
        }
        linkedDragEvents.Add(e);
    }

    public void savePreTest()
    {
        formerPosition = transform.position;
        formerRotation = transform.rotation;
    }


    public void resetPreTest()
    {
        transform.position = formerPosition;
        transform.rotation = formerRotation;
    }
}

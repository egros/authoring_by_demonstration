﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyIconController : MonoBehaviour
{
    private GameObject selection;
    private GameObject arCreationFolder;
    private GameObject vrCamera;
    private Dictionary<int, ArObject> oldTonewConnexion;
    private Dictionary<int, ArObject> arObjectsInScene;
    private List<EventDescription> eventsToInstantiate;
    private Dictionary<int, int> linksToInstantiate;
    private Dictionary<int, EventIconController> eventsInscene;
    private Dictionary<int, EventIconController> oldTonewEvents;

    // Start is called before the first frame update
    void Start()
    {
        selection = GameObject.Find("SelectorReference");
        arCreationFolder = GameObject.Find("ARCreations");
        vrCamera = GameObject.Find("VRCamera");
    }

    public void OnClick()
    {
        oldTonewConnexion = new Dictionary<int, ArObject>();
        arObjectsInScene = new Dictionary<int, ArObject>();
        ArObject[] arObjects = GameObject.FindObjectsOfType<ArObject>();
        foreach (ArObject a in arObjects)
        {
            arObjectsInScene.Add(a.getID(), a);
        }
        eventsToInstantiate = new List<EventDescription>();
        eventsInscene = new Dictionary<int, EventIconController>();
        oldTonewEvents = new Dictionary<int, EventIconController>();
        linksToInstantiate = new Dictionary<int, int>();
        EventIconController[] events = GameObject.FindObjectsOfType<EventIconController>();
        foreach (EventIconController e in events)
        {
            eventsInscene.Add(e.getID(), e);
        }


        List<Transform> selectionChildren = new List<Transform>();
        foreach (Transform t in selection.transform)
        {
            selectionChildren.Add(t);
        }
        foreach (Transform t in selectionChildren)
        {
            Debug.Log("Copying " + t.gameObject.name);
            ArObject arParent = t.gameObject.GetComponent<ArObject>();
            arParent.enableOutline(false);
            Vector3 tLocal = vrCamera.transform.InverseTransformPoint(t.position);
            Vector3 pos = new Vector3(tLocal.x - 0.5f, tLocal.y, tLocal.z);
            GameObject assetCopy = Instantiate(t.gameObject, pos, t.rotation);
            assetCopy.transform.SetParent(arCreationFolder.transform);
            assetCopy.transform.position = vrCamera.transform.TransformPoint(pos);

            // Delete unwanted children
            foreach(Transform child in assetCopy.transform)
            {
                Destroy(child.gameObject);
            }

            ArObject arObj = assetCopy.GetComponent<ArObject>();
            arObj.initializeAttributes(ApplicationModel.idCpt, arParent.getIsVisibleAtStart(), arParent.getColorAtStart());
            ApplicationModel.idCpt++;
            oldTonewConnexion.Add(arParent.getID(), arObj);

            // Attached Events
            EventIconController[] attachedEvents = t.GetComponentsInChildren<EventIconController>();
            foreach (EventIconController e in attachedEvents)
            {
                eventsToInstantiate.Add(e.getDescription());
            }

            StartCoroutine(forEndOfFrame(arObj));
            arParent.enableOutline(true);
        }

        foreach(EventDescription d in eventsToInstantiate)
        {
            createEventFromDescription(d);
        }

        
    }

    IEnumerator forEndOfFrame(ArObject arObj)
    {
        yield return new WaitForSeconds(0f);

        arObj.defaultOutline();
        Debug.Log("Create links");
        foreach (KeyValuePair<int, int> link in linksToInstantiate)
        {
            Debug.Log("Link " + link.Key + " and " + oldTonewEvents[link.Value].getID());
            if (oldTonewEvents.ContainsKey(link.Value))
            {
                eventsInscene[link.Key].linkToAnimation(oldTonewEvents[link.Value]);
            }
        }
    }

    public void displayNextTo(GameObject parent)
    {
        GeometryAnalyzer geoAnalyze = ScriptableObject.CreateInstance<GeometryAnalyzer>();
        minNmax extrem = geoAnalyze.getMinNmax(parent);
        Vector3 leftPoint = new Vector3(extrem.min.x, parent.transform.position.y, parent.transform.position.z);
        Transform vrCam = GameObject.Find("VRCamera").transform;
        Vector3 localPos = vrCam.InverseTransformPoint(leftPoint);
        Vector3 leftpos = new Vector3(localPos.x - 0.2f, localPos.y, localPos.z);
        transform.position = vrCam.TransformPoint(leftpos);
        transform.LookAt(vrCamera.transform);
        transform.Rotate(new Vector3(0, 180, 0));
    }

    public void createEventFromDescription(EventDescription descr)
    {
        /*
        mainType = mType;
        id = identifiant;
        target = targ;
        animations = anim;
        previousIcon = previous;
        nextIcon = nextI;
        color = coloration;
        holdingHand = holdingH;
        trajectory = traj;
        rotations = rot;
        collided = collidedObj;
        followDirection = follow;
         */
        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        GameObject rightHand = GameObject.Find("RightHand");
        GameObject leftHand = GameObject.Find("LeftHand");
        InteractionControllerRecorder rightController = rightHand.GetComponent<InteractionControllerRecorder>();
        InteractionControllerRecorder leftController = leftHand.GetComponent<InteractionControllerRecorder>();
        ArObject[] arObjectsInScene = GameObject.FindObjectsOfType<ArObject>();
        Dictionary<int, ArObject> dictArObj = new Dictionary<int, ArObject>();
        foreach (ArObject a in arObjectsInScene)
        {
            dictArObj.Add(a.getID(), a);
        }

        string mainType = descr.mainType;
        int id = ApplicationModel.iconIdx;
        ApplicationModel.iconIdx++;
        int target = descr.target;
        int[] animations = descr.animations;
        Color color = Color.black;
        if (descr.color != null)
        {
            color = new Color(descr.color[0], descr.color[1], descr.color[2], descr.color[3]);
        }
        
        int holdingHand = descr.holdingHand;
        string trajectory = descr.trajectory;
        string rotations = descr.rotations;
        int collided = descr.collided;
        bool followDirection = descr.followDirection;

        string name = "Events\\Icons\\";
        GameObject targetObj;
        bool isArObject = false;
        if (target == 0)
        {
            targetObj = rightHand;
        }
        else if (target == 1)
        {
            targetObj = leftHand;
        }
        else if (target == 2)
        {
            targetObj = GameObject.Find("Player");
        }
        else if (target > 2)
        {
            if (oldTonewConnexion.ContainsKey(target))
            {
                targetObj = oldTonewConnexion[target].gameObject;
            }
            else
            {
                Debug.Log("Parent should always have a copy");
                targetObj = arObjectsInScene[target].gameObject;
            }
            isArObject = true;
        }
        else //targetObj==-1
        {
            targetObj = null;
        }


        RecordedEvent recordedEvent = null;
        if (mainType == "Move")
        {
            name = name + "MoveIcon";
            List<TransformReccord> traj = translater.turnStringToTransformList(trajectory, rotations);
            RecordedMovement record = ScriptableObject.CreateInstance<RecordedMovement>();
            record.setTrajectory(traj, targetObj, isArObject);
            record.setFollowDirection(followDirection);
            recordedEvent = record;
        }
        else if (mainType == "Drag")
        {
            name = name + "DragIcon";
            List<TransformReccord> traj = translater.turnStringToTransformList(trajectory, rotations);
            RecordedDrag record = ScriptableObject.CreateInstance<RecordedDrag>();
            string hnd = "";
            if (holdingHand == 0)
            {
                hnd = "RightHand";
            }
            else
            {
                hnd = "LeftHand";
            }
            record.setElements(targetObj, traj, hnd, traj[0].getPosition());
            record.setFollowDirection(followDirection);
            recordedEvent = record;
        }
        else if (mainType == "Grab")
        {
            name = name + "GrabIcon";
            RecordedGrab grab = ScriptableObject.CreateInstance<RecordedGrab>();
            Vector3 v;
            if (targetObj == null)
            {
                List<Vector3> pos = translater.turnStringToVectorList(trajectory);
                v = pos[0];
            }
            else
            {
                v = targetObj.transform.position;
            }
            if (holdingHand == 0)
            {
                grab.setGrabbedObject(targetObj, v, rightHand);
                rightController.addRecordedGrab(grab);
            }
            else
            {
                grab.setGrabbedObject(targetObj, v, leftHand);
                leftController.addRecordedGrab(grab);
            }
            recordedEvent = grab;

        }
        else if (mainType == "Drop")
        {
            name = name + "DropIcon";
            RecordedDrop drop = ScriptableObject.CreateInstance<RecordedDrop>();
            Vector3 v;
            if (targetObj == null)
            {
                List<Vector3> pos = translater.turnStringToVectorList(trajectory);
                v = pos[0];
            }
            else
            {
                v = targetObj.transform.position;
            }
            if (holdingHand == 0)
            {
                drop.setDroppedObject(targetObj, v, rightHand);
                rightController.addRecordedDrop(drop);
            }
            else
            {
                drop.setDroppedObject(targetObj, v, leftHand);
                leftController.addRecordedDrop(drop);
            }
            recordedEvent = drop;
        }
        else if (mainType == "Color")
        {
            name = name + "ColorIcon";
            RecordedColor colorEvent = ScriptableObject.CreateInstance<RecordedColor>();
            colorEvent.setColorEvent(targetObj, color);
            recordedEvent = colorEvent;
        }
        else if (mainType == "Collision")
        {
            name = name + "CollisionIcon";
            RecordedCollision collision = ScriptableObject.CreateInstance<RecordedCollision>();
            GameObject collidedObj = null;
            if (collided == 0)
            {
                collidedObj = rightHand;
            }
            else if (collided == 1)
            {
                collidedObj = leftHand;
            }
            else if (collided == 2)
            {
                collidedObj = GameObject.Find("Player");
            }
            else
            {
                if (oldTonewConnexion.ContainsKey(collided))
                {
                    collidedObj = oldTonewConnexion[collided].gameObject;
                }
                else
                {
                    collidedObj = arObjectsInScene[collided].gameObject;
                }
            }
            collision.setCollision(collidedObj, targetObj);
            recordedEvent = collision;
        }
        else if (mainType == "Show")
        {
            name = name + "ShowIcon";
            RecordedShow showEvent = ScriptableObject.CreateInstance<RecordedShow>();
            showEvent.setShow(targetObj);
            recordedEvent = showEvent;
        }
        else if (mainType == "Hide")
        {
            name = name + "HideIcon";
            RecordedHide hideEvent = ScriptableObject.CreateInstance<RecordedHide>();
            hideEvent.setHideEvent(targetObj);
            recordedEvent = hideEvent;
        }
        else if (mainType == "Look")//Look
        {
            name = name + "LookIcon";
            RecordedLook lookEvent = ScriptableObject.CreateInstance<RecordedLook>();
            lookEvent.setLook(targetObj);
            recordedEvent = lookEvent;
        }

        UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
        GameObject instantiatedObject = Instantiate((GameObject)assetToGet, Vector3.zero, Quaternion.identity);
        if (recordedEvent.getAttachParent() != null)
        {
            instantiatedObject.transform.position = recordedEvent.getAttachParent().transform.position;
            instantiatedObject.transform.SetParent(recordedEvent.getAttachParent().transform);
            CircleDispatcher dispatch = recordedEvent.getAttachParent().GetComponent<CircleDispatcher>();
            if (dispatch != null)
            {
                dispatch.haveToUpdate();
            }
            else
            {
                dispatch = recordedEvent.getAttachParent().AddComponent<CircleDispatcher>();
                dispatch.haveToUpdate();
            }
        }
        else
        {
            instantiatedObject.transform.position = recordedEvent.getAttachPosition();
            instantiatedObject.transform.SetParent(arCreationFolder.transform);
        }
        EventIconController eventIcon = instantiatedObject.GetComponent<EventIconController>();
        eventIcon.setRecordedEvent(recordedEvent, null, id);
        eventsInscene.Add(id, eventIcon);
        oldTonewEvents.Add(descr.id, eventIcon);

        // Links
        foreach (int i in animations)
        {
            linksToInstantiate.Add(id, i);
        }

    }

    
}

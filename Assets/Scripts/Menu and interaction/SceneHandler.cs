﻿/* SceneHandler.cs*/
/*Partly from : https://setzeus.medium.com/tutorial-steamvr-2-0-laser-pointer-bbc816ebeec5 */

using DitzelGames.FastIK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.Extras;
using Valve.VR.InteractionSystem;

public class SceneHandler : MonoBehaviour
{

    public SteamVR_LaserPointer laserPointer;

    private Hand dominantHand;
    private GameObject attachedObject;
    private Transform previousParent;
    private GameObject spine;
    private FastIKLook lookControl;


    Component[] laserRenderers;
    Renderer laserRenderer;


    private SteamVR_Action_Boolean grabInput;
    private SteamVR_Action_Boolean rightGrab;
    private SteamVR_Action_Boolean input;
    private SteamVR_Action_Boolean forwardInput;
    private SteamVR_Action_Boolean backInput;

    private bool laserVisible;
    private bool enabledLaser;

    private GameObject selectorReferent;
    private bool selectionIsAttached;
    private GameObject arParent;
    private GameObject copyIcon;

    private GameObject collidedObject;

    private EventAnimationLinkController linkMonitoring;
    private ScrollHandleController scroll;

    // Navigation
    private bool navAnchored;
    private Vector2 navAnchorPoint;
    private GameObject player;
    private int towards;

    // Modes
    private int currentMode;

    // Recording interactions
    private InteractionArObjectsRecorder arObjectsRecorder;

    public void Start()
    {
        if (!ApplicationModel.isIntro)
        {
            selectorReferent = GameObject.Find("SelectorReference");
            selectionIsAttached = false;
            arParent = GameObject.Find("ARCreations");
            copyIcon = GameObject.Find("CopyIcon");
            copyIcon.SetActive(false);
        }
        

        dominantHand = GameObject.Find(ApplicationModel.dominantHand).GetComponent<Hand>();

        // Get inputs
        grabInput = SteamVR_Input.GetBooleanAction("GrabGrip");
        rightGrab = SteamVR_Input.GetBooleanAction("RightGrab");
        input = SteamVR_Input.GetBooleanAction("Laser");
        forwardInput = SteamVR_Input.GetBooleanAction("Forward");
        backInput = SteamVR_Input.GetBooleanAction("Back");

        // Get laser renderer
        laserRenderers = laserPointer.GetComponentsInChildren(typeof(Renderer));

        //Navigation
        enabledLaser = true;
        navAnchored = false;
        player = GameObject.Find("Player");

        //Modes
        currentMode = 1;

        // Interactions recording
        arObjectsRecorder = GameObject.FindObjectOfType<InteractionArObjectsRecorder>();

        laserVisible = true;
    }


    void Awake()
    {
        laserPointer.PointerIn += PointerInside;
        laserPointer.PointerOut += PointerOutside;
        laserPointer.PointerClick += PointerClick;
    }

    public void PointerClick(object sender, PointerEventArgs e)
    {
        // Click on a button
        Button button = e.target.GetComponent<Button>();
        if (button != null)
        {
            ButtonController menuButton = button.GetComponent<ButtonController>();
            if (menuButton != null)
            {
                menuButton.OnClick();
            }
            else
            {
                button.GetComponent<PaletteButtonController>().OnClick();
            }
        }
        else
        {
            if (e.target.name == "Open_Close_Cube")
            {
                GameObject.Find("Palette").GetComponent<PaletteController>().openClosePalette();
            }
        }

        // If the target is ColorPicker
        ColorPickerTriangle colorPicker = e.target.GetComponent<ColorPickerTriangle>();
        if (colorPicker != null)
        {
            colorPicker.hitColorPicker(laserPointer.hitPoint);
        }
        else
        {
            // If the target is an attribute icon
            BooleanAttributeController attributeIcon = e.target.GetComponent<BooleanAttributeController>();
            if (attributeIcon != null && linkMonitoring == null)
            {
                attributeIcon.switchOnOff();
                dominantHand.HideGrabHint();
            }
        }

        

        // If the target is an event icon
        EventIconController eIcon = e.target.GetComponent<EventIconController>();
        if (eIcon != null && ApplicationModel.mode != "Test")
        {
            if (linkMonitoring != null)
            {
                if (linkMonitoring.triggerIsNull())
                {
                    linkMonitoring.addTrigger(eIcon);
                    dominantHand.HideGrabHint();
                }
                else if(linkMonitoring.animationIsNull() && eIcon.isAnimation())
                {
                    linkMonitoring.addAnimation(eIcon);
                    dominantHand.HideGrabHint();
                }
            }
        }

        // Click on virtual AR object in deletion/selection/inspection mode
        if(currentMode!=1 && e.target.gameObject.tag == "ArObjects")
        {
            if(currentMode == 2)
            {
                selectObject(e.target.gameObject);
            }
            else if (currentMode == 3)
            {
                deleteClick(e.target.gameObject);
            }
            else if(currentMode == 4)
            {
                inspectObject(e.target.gameObject);
            }
        }

        // If the target is a CopyIcon
        if(e.target.gameObject.GetComponent<CopyIconController>() != null)
        {
            e.target.gameObject.GetComponent<CopyIconController>().OnClick();
        }
    }
    

    public void PointerInside(object sender, PointerEventArgs e)
    {
        // If the target is a button
        Button button = e.target.GetComponent<Button>();
        if (button != null)
        {
            ButtonController menuButton = button.GetComponent<ButtonController>();
            if (menuButton != null)
            {
                menuButton.OnHover(true);
            }
            else
            {
                button.GetComponent<PaletteButtonController>().OnHover(true);
            }
        }

        // If the target is an event icon
        EventIconController eIcon = e.target.GetComponent<EventIconController>();
        if ( eIcon!= null && ApplicationModel.mode!="Test")
        {
            eIcon.launchSimulation();
            eIcon.displayJack();
            if (linkMonitoring != null)
            {
                if(linkMonitoring.triggerIsNull() || (linkMonitoring.animationIsNull() && eIcon.isAnimation()))
                {
                    dominantHand.ShowGrabHint();
                }
            }
        }

        // If the target is an attribute icon
        AttributeController attributeIcon = e.target.GetComponent<AttributeController>();
        if(attributeIcon != null && linkMonitoring==null)
        {
            attributeIcon.displayInfoPanel();
            dominantHand.ShowGrabHint();
        }

        // If the target is an AR object
        ArObject arTarget = e.target.GetComponent<ArObject>();
        if (e.target.gameObject.tag== "ArObjects")
        {
            // Outline the object with a specific color depending on the laser mode
            Color c = new Color(0, 0, 0);
            if (currentMode == 2)
            {
                c = new Color(0, 0, 255);
            }
            else if (currentMode == 3)
            {
                c = new Color(255, 0, 0);
            }
            else if (currentMode == 4)
            {
                c = new Color(0, 255, 0);
            }

            if(arTarget!=null && arTarget.getIsSelected())
            {
                foreach(Transform t in selectorReferent.transform)
                {
                    t.GetComponent<ArObject>().colorOutline(c);
                }
            }
            else if(arTarget!=null)
            {
                arTarget.colorOutline(c);
            }
        }
        


        collidedObject = e.target.gameObject;

        
    }

    public void PointerOutside(object sender, PointerEventArgs e)
    {
        // if the collided object is an ArObject
        ArObject arTarget = collidedObject.GetComponent<ArObject>();
        if (arTarget != null)
        {
            if (arTarget.getIsSelected())
            {
                foreach(Transform t in selectorReferent.transform)
                {
                    t.GetComponent<ArObject>().colorOutline(new Color(0, 0, 255));
                }
            }
            else
            {
                arTarget.outlineOff();
            }
        }

        collidedObject = null;
        // If the target is a button
        Button button = e.target.GetComponent<Button>();
        if (button != null)
        {
            PaletteButtonController menuButton = button.GetComponent<PaletteButtonController>();
            if (menuButton != null)
            {
                menuButton.OnHover(false);
            }
        }

        // If the target is an attribute icon
        AttributeController attributeIcon = e.target.GetComponent<AttributeController>();
        if (attributeIcon != null)
        {
            attributeIcon.hideInfoPanel();
            dominantHand.HideGrabHint();
        }

        // If the target is an event icon
        EventIconController eIcon = e.target.GetComponent<EventIconController>();
        if (eIcon != null && ApplicationModel.mode != "Test")
        {
            eIcon.noLongerNeedToDisplayJack(3);
            if (linkMonitoring != null)
            {
                if (linkMonitoring.triggerIsNull() || (linkMonitoring.animationIsNull() && eIcon.isAnimation()))
                {
                    dominantHand.HideGrabHint();
                }
            }
        }


    }

    public void attachObject()
    {
        if(collidedObject!=null && attachedObject == null)
        {
            SimpleAttach simpleAttachScript = collidedObject.GetComponent<SimpleAttach>();
            if (collidedObject.tag == "ArObjects" &&  simpleAttachScript!= null && simpleAttachScript.enabled==true && ApplicationModel.mode!="Test")
            {
                if (collidedObject.gameObject.GetComponent<ArObject>() != null)
                {
                    ArObject arOb = collidedObject.gameObject.GetComponent<ArObject>();
                    if (arOb.getIsSelected())
                    {
                        selectorReferent.transform.SetParent(laserPointer.transform);

                        selectionIsAttached = true;
                        attachedObject = selectorReferent;
                    }
                    else
                    {
                        previousParent = collidedObject.transform.parent;
                        //oldscale = collidedObject.transform.localScale;
                        collidedObject.transform.SetParent(laserPointer.transform);
                        attachedObject = collidedObject.gameObject;
                        selectionIsAttached = false;
                    }
                }
                else
                {
                    previousParent = collidedObject.transform.parent;
                    //oldscale = collidedObject.transform.localScale;
                    collidedObject.transform.SetParent(laserPointer.transform);
                    attachedObject = collidedObject.gameObject;
                    selectionIsAttached = false;
                }


                if (attachedObject.name == "WoodBody" && lookControl != null)
                {
                    lookControl.enabled = false;
                }
                if (ApplicationModel.recordingStatus == "On")
                {
                    if (ApplicationModel.recordedTarget == "Puppet" && attachedObject.name == "WoodBody")
                    {
                        GameObject.Find("SphereRightHand").GetComponent<MoveRecorder>().stopMonitoring();
                        GameObject.Find("SphereLeftHand").GetComponent<MoveRecorder>().stopMonitoring();
                    }
                }
                
            }
        }
    }

    public void detachObject()
    {
        if (attachedObject != null)
        {
            if (attachedObject.name == "WoodBody" && lookControl != null)
            {
                lookControl.enabled = true;
                lookControl.resetVariables();
            }
            

            if (selectionIsAttached)
            {
                selectorReferent.transform.SetParent(arParent.transform);
                selectionIsAttached = false;
            }
            else
            {
                attachedObject.transform.SetParent(previousParent);
                //attachedObject.transform.localScale = oldscale;
                if (attachedObject.name == "SphereRightHand" || attachedObject.name == "SphereLeftHand" || attachedObject.name == "SphereLookHead")
                {
                    attachedObject.transform.rotation = Quaternion.identity;
                }
                if (attachedObject.GetComponent<JackConnectorController>() != null)
                {
                    attachedObject.GetComponent<JackConnectorController>().onRelease();
                }
            }
            if (ApplicationModel.recordingStatus == "On")
            {
                if (ApplicationModel.recordedTarget == "Puppet" && attachedObject.name == "WoodBody")
                {
                    GameObject.Find("SphereRightHand").GetComponent<MoveRecorder>().restartMonitoring();
                    GameObject.Find("SphereLeftHand").GetComponent<MoveRecorder>().restartMonitoring();
                }
            }
            
            attachedObject = null;
        }
    }


    public void Update()
    {
        
        if (attachedObject != null)
        {
            float moveDist = 0.005f;

            if (forwardInput.lastState)
            {
                attachedObject.transform.localPosition = new Vector3(attachedObject.transform.localPosition.x, attachedObject.transform.localPosition.y, attachedObject.transform.localPosition.z + moveDist);
            }
            if (backInput.lastState)
            {
                attachedObject.transform.localPosition = new Vector3(attachedObject.transform.localPosition.x, attachedObject.transform.localPosition.y, Mathf.Max(0.01f, attachedObject.transform.localPosition.z - moveDist));
            }
        }

        
        // AttachObject
        if (grabInput.lastStateDown && collidedObject != null && currentMode==1)
        {
            if(attachedObject == null)
            {
                ScrollHandleController scrollHandle = collidedObject.GetComponent<ScrollHandleController>();
                if (scrollHandle != null)
                {
                    scrollHandle.select(true);
                    scroll = scrollHandle;
                }
                else
                {
                    attachObject();
                }
            }
        }
        if (grabInput.lastStateUp && attachedObject != null && currentMode==1)
        {
            detachObject();
        }

        if(grabInput.lastStateUp && scroll != null)
        {
            scroll.select(false);
            scroll = null;
        }


        if (spine == null)
        {
            spine = GameObject.Find("spine.006");
            if (spine != null)
            {
                lookControl = spine.GetComponent<FastIKLook>();
            }
        }

        if (laserRenderer == null)
        {
            laserRenderers = laserPointer.GetComponentsInChildren(typeof(Renderer));
            foreach (Renderer renderer in laserRenderers)
            {
                if (renderer.name == "Cube")
                {
                    laserRenderer = renderer;
                }
            }
        }
        if (input.lastStateDown) 
        {
            //Handle button pressed
            if (currentMode!=1) // Is not in default mode (selection/deletion/inspection)
            {
                // Switch back to default mode
                currentMode = 1;
                laserPointer.color = new Color(0, 0, 0);
                laserPointer.clickColor = new Color(0, 0, 0);
            }
            else 
            {
                // Switch on or off laser
                switchOnOffLaser();
            }
        }

        // Navigation
        if (rightGrab.state && collidedObject!=null && collidedObject.name == "WorldFloor" && dominantHand.GetComponentInChildren<SimpleAttach>()==null) // User is grabbing the floor with the laser
        {
            if (!navAnchored)
            {
                // Defining the anchor as a reference point for movement
                navAnchored = true;
                navAnchorPoint = new Vector2(getLaserHitPoint().x, getLaserHitPoint().z);
                towards = 0;
            }
            else
            {
                // Moving in the opposite way of the laser
                Vector2 floorPos = new Vector2(getLaserHitPoint().x, getLaserHitPoint().z);
                float distance = Vector2.Distance(navAnchorPoint, floorPos);
                float dx = navAnchorPoint.x - floorPos.x;
                float dz = navAnchorPoint.y - floorPos.y;
                Vector3 oppositePoint = new Vector3(player.transform.position.x - dx, player.transform.position.y, player.transform.position.z - dz);
                player.transform.position = Vector3.MoveTowards(player.transform.position, oppositePoint, -Mathf.Min(0.03f, distance));
            }
        }
        if(rightGrab.stateUp && navAnchored) // The user stopped grabbing the floor
        {
            navAnchored = false;
        }

    }

    public void switchOnOffLaser()
    {
        /*if (enabledLaser)
        {
            if (laserVisible)
            {
                laserRenderer.enabled = false;
                laserPointer.enabled = false;
            }
            else
            {
                laserRenderer.enabled = true;
                laserPointer.enabled = true;
            }
            laserVisible = !laserVisible;
        }*/
        if (enabledLaser)
        {
            enableLaser(false);
        }
        else
        {
            enableLaser(true);
        }
    }

    public void switchOnLaser()
    {
        if (enabledLaser)
        {
            laserRenderer.enabled = true;
            laserPointer.enabled = true;
            laserVisible = true;
        }
        
    }

    public void enableLaser(bool laserIsEnable)
    {
        if (laserIsEnable)
        {
            enabledLaser = true;
            switchOnLaser();
        }
        else
        {
            switchOffLaser();
            enabledLaser = false;
        }
    }

    public void switchOffLaser()
    {
        laserRenderer.enabled = false;
        laserPointer.enabled = false;
        laserVisible = false;
    }

    public bool getLaserIsEnabled()
    {
        return enabledLaser;
    }

    public void switchMode(int newMode)
    {
        if (!enabledLaser)
        {
            enableLaser(true);
        }
        if (newMode == 2)
        {
            if (currentMode == 2)
            {
                //Switch off selection mode
                attachedObject = null;
                currentMode = 1;
                laserPointer.color = new Color(0, 0, 0);
                laserPointer.clickColor = new Color(0, 0, 0);
            }
            else
            {
                // Switch on selection mode
                currentMode = 2;
                laserPointer.color = new Color(0, 0, 255);
                laserPointer.clickColor = new Color(0, 0, 176);
                selectionIsAttached = false;
            }
        }
        else if (newMode == 3)
        {
            if (currentMode == 3) // switch off deletion mode
            {
                attachedObject = null;
                currentMode = 1;
                laserPointer.color = new Color(0, 0, 0);
                laserPointer.clickColor = new Color(0, 0, 0);
            }
            else
            {
                // switch on deletion mode
                laserPointer.color = new Color(255, 0, 0);
                laserPointer.clickColor = new Color(176, 0, 0);
                currentMode = 3;
                selectionIsAttached = false;
                if (linkMonitoring != null)
                {
                    Destroy(linkMonitoring.gameObject);
                    linkMonitoring = null;
                }
            }
        }
        else if (newMode == 4)
        {
            if (currentMode == 4)
            {
                // Stop inspection mode
                attachedObject = null;
                currentMode = 1;
                laserPointer.color = new Color(0, 0, 0);
                laserPointer.clickColor = new Color(0, 0, 0);
            }
            else
            {
                // Start inspection mode
                currentMode = 4;
                laserPointer.color = new Color(0, 255, 0);
                laserPointer.clickColor = new Color(0, 176, 0);
                selectionIsAttached = false;
            }
        }
    }

    public int getCollidedObjectID()
    {
        if (collidedObject != null)
        {
            ArObject arComponent = collidedObject.GetComponent<ArObject>();
            if (arComponent != null)
            {
                return arComponent.getID();
            }
        }
        return -1;
    }

    public int getAttachedObjectID()
    {
        if (attachedObject != null)
        {
            ArObject arComponent = attachedObject.GetComponent<ArObject>();
            if (arComponent != null)
            {
                return arComponent.getID();
            }
        }
        return -1;
    }

    public GameObject getAttachedObject()
    {
        return attachedObject;
    }

    public Vector3 getLaserHitPoint()
    {
        // Get the Vector3 position of the point the laser is hitting
        return laserPointer.hitPoint;
    }

    public GameObject getCollidedObject()
    {
        return collidedObject;
    }

    public bool isLinkMonitoring()
    {
        if (linkMonitoring == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void setLink(EventAnimationLinkController link)
    {
        linkMonitoring = link;
    }

    public void stopMonitoring()
    {
        linkMonitoring = null;
    }

    public bool isHittingScrollHandle()
    {
        if (collidedObject != null)
        {
            ScrollHandleController scrollerHandle = collidedObject.GetComponent<ScrollHandleController>();
            if (scrollerHandle != null)
            {
                return true;
            }
        }
        return false;
    }


    private void deleteClick(GameObject target)
    {
        // Click on the object to delete it
        //bool cond1 = (target.gameObject.tag == "ArObjects");
        bool cond21 = (target.gameObject.transform.parent == arParent.transform || target.gameObject.transform.parent == selectorReferent.transform);
        bool cond22 = (target.gameObject.GetComponent<TwinController>() != null);
        bool cond23 = (target.gameObject.GetComponent<EventIconController>() != null);
        bool cond24 = (target.gameObject.GetComponent<MovePositionController>() != null);
        bool cond25 = (target.gameObject.GetComponent<LastMovePosition>() != null);
        bool cond3 = target.gameObject.GetComponentInParent<Zone3DColliderController>() != null;

        //if (cond1 && (cond21 || cond22 || cond23 || cond24 || cond25))
        if (cond21 || cond22 || cond23 || cond24 || cond25 || cond3)
        {
            if (ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "ArObjects" && target.gameObject.GetComponent<ArObject>() != null)
            {
                // On recording mode: hide the object
                arObjectsRecorder.addHide(target.gameObject);
                target.gameObject.SetActive(false);
            }
            else
            {
                CircleDispatcher dispatch = null;
                if (cond23)
                {
                    // If the object is an eventIcon
                    EventIconController eventIconController = target.gameObject.GetComponent<EventIconController>();
                    if (target.gameObject.transform.parent != null)
                    {
                        dispatch = target.gameObject.transform.parent.gameObject.GetComponent<CircleDispatcher>();
                    }
                    if (eventIconController.getPrevious() == null && eventIconController.getNext() != null)
                    {
                        Destroy(eventIconController.getNext().GetComponent<EventIconController>().getBezier());
                    }
                    else if (eventIconController.getPrevious() != null && eventIconController.getNext() != null)
                    {
                        eventIconController.getPrevious().GetComponent<EventIconController>().setNextEventIcon(eventIconController.getNext(), true);
                    }
                }
                if (!cond24 && !cond25)
                {
                    // Delete ArObject
                    ArObject arObj = target.GetComponent<ArObject>();
                    if (arObj!=null && arObj.getIsSelected())
                    {
                        // Delete all selection
                        foreach(Transform t in selectorReferent.transform)
                        {
                            Destroy(t.gameObject);
                        }
                    }
                    else if (cond3 && !cond23)
                    {
                        Destroy(target.gameObject.GetComponentInParent<Zone3DColliderController>().gameObject);
                    }
                    else
                    {
                        Destroy(target.gameObject);
                    }
                }
                else if (cond24)
                {
                    // Delete position Dot
                    target.gameObject.GetComponent<MovePositionController>().deletePos();
                }
                else if (cond25)
                {
                    // Make trajectory a straight line
                    target.gameObject.GetComponent<LastMovePosition>().makeContinuous();
                }

                if (dispatch != null)
                {
                    // Make event icons update
                    dispatch.haveToUpdate();
                }
            }
        }
    }

    private void inspectObject(GameObject target)
    {
        // Inspect objects to make their properties appear

        // If the object is an ArObject => show attributes
        ArObject arComponent = target.gameObject.GetComponent<ArObject>();
        if (arComponent != null)
        {
            arComponent.hideShowAttributes();
        }

        // If the object is an event icon move or drag, show its properties
        EventIconController eventIcon = target.gameObject.GetComponent<EventIconController>();
        if (eventIcon != null && (eventIcon.getRecordedEvent().getActionName() == "Move" || eventIcon.getRecordedEvent().getActionName() == "Drag"))
        {
            eventIcon.inspect();
        }
    }

    private void selectObject(GameObject target)
    {
        // Add or remove object to selection
        if (target.transform.parent == arParent.transform || target.transform.parent == selectorReferent.transform)
        {
            ArObject arObj = target.GetComponent<ArObject>();
            if (arObj != null)
            {
                if (arObj.getIsSelected())
                {
                    // Remove from selected objects
                    arObj.enableOutline(false);
                    target.transform.SetParent(arParent.transform);
                    if (selectorReferent.transform.childCount == 0)
                    {
                        displayCopyIcon(false);
                    }
                }
                else
                {
                    // Add to selection
                    arObj.enableOutline(true);
                    bool display = false;
                    if (selectorReferent.transform.childCount == 0)
                    {
                        display = true;
                    }
                    target.transform.SetParent(selectorReferent.transform);
                    if (display)
                    {
                        displayCopyIcon(true);
                    }
                }
            }
        }
    }

    public void displayCopyIcon(bool display)
    {
        copyIcon.SetActive(display);
        if (display)
        {
            copyIcon.GetComponent<CopyIconController>().displayNextTo(selectorReferent.transform.GetChild(0).gameObject);
        }
    }

    public void attachToLaser(GameObject objectToAttach, float distance)
    {
        Debug.Log("Attach marble to laser");
        objectToAttach.transform.SetParent(laserPointer.transform);
        objectToAttach.transform.localPosition = new Vector3(0f, 0f, distance);
        attachedObject = objectToAttach;
    }

    public void detachToLaser(GameObject objectToDetach, Transform parent)
    {
        if (objectToDetach.transform.IsChildOf(laserPointer.transform) && attachedObject!=null)
        {
            objectToDetach.transform.SetParent(parent);
            attachedObject = null;
        }
    }

}
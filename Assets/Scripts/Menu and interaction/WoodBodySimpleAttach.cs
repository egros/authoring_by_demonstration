﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class WoodBodySimpleAttach : SimpleAttach
{
    public GameObject lookSphere;

    public override void Update()
    {
        if (input.lastStateUp && isAttached && input.activeDevice.ToString() == holdingHand)
        {
            if (transform.parent.gameObject.name == ApplicationModel.dominantHand && ApplicationModel.recordingStatus == "On")
            {
                dominantHandRecorder.dropping(gameObject);
            }

            if (initialParent == null)
            {
                transform.SetParent(null);
            }
            else
            {
                transform.SetParent(initialParent.transform);
            }
            holdingHand = null;
            if (lookSphere.GetComponent<LookController>() != null)
            {
                lookSphere.GetComponent<LookController>().enabled = true;
                Debug.Log("Enable LookController");
            }
            isAttached = false;
        }
    }

    public override void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();
        //bool isGrabEnding = hand.IsGrabEnding(gameObject);

        //GRAB THE OBJECT
        if (ApplicationModel.mode == "simple")
        {
            if (grabType != GrabTypes.None && !isAttached)//grab
            {
                if (hand.handType.ToString() == ApplicationModel.dominantHand && ApplicationModel.recordingStatus == "On")
                {
                    dominantHandRecorder.grabbing(gameObject);
                }
                if (transform.parent != null)
                {
                    initialParent = transform.parent.gameObject;
                }
                else
                {
                    initialParent = null;
                }
                if (lookSphere.GetComponent<LookController>() != null)
                {
                    lookSphere.GetComponent<LookController>().enabled = false;
                    Debug.Log("Disable LookController");
                }
                holdingHand = hand.gameObject.name;
                transform.SetParent(hand.transform);
                isAttached = true;
            }

        }
    }
}

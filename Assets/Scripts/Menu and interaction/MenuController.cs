﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.UI;
using System;
using Valve.VR.Extras;

public class MenuController : MonoBehaviour
{
    private GameObject rightHand;
    private bool opened=false;
    private GameObject menu;
    private SceneHandler laserController;


    // Panels
    public List<GameObject> panels;


    // Public variables
    public GameObject myCamera;
    public SteamVR_Action_Boolean inputMenu;

    // Start is called before the first frame update
    void Start()
    {
        rightHand = GameObject.Find("RightHand");
        laserController = rightHand.GetComponent<SceneHandler>();

        // Hide sub-panels
        foreach (GameObject panel in panels)
        {
            panel.SetActive(false);
        }

        // Get menu and hide it
        menu = GameObject.Find("MenuCanvas");
        menu.SetActive(false);

        menu.transform.parent = Camera.main.transform;
        menu.transform.localPosition = new Vector3(0, -0.15f, 1);
        menu.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
    }

    private void switchForm(string form)
    {
        ApplicationModel.form = form;
    }

    private void openMenu()
    {
        if (ApplicationModel.mode == "Test")
        {
            menu.SetActive(true);
            panels[6].SetActive(true);
        }
        else
        {
            ApplicationModel.mode = "simple";
            menu.SetActive(true);
            panels[0].SetActive(true);
        }
        
        

        laserController.switchOnLaser();
        
    }

    public void closeMenu()
    {
        foreach (GameObject panel in panels)
        {
            panel.SetActive(false);
        }
        menu.SetActive(false);

        laserController.switchOffLaser();

    }

    public void openPanel(GameObject panelToHide, GameObject panelToShow)
    {
        panelToHide.SetActive(false);
        panelToShow.SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("m") || inputMenu.lastStateDown)
        {
            if (opened)
            {
                closeMenu();
            }
            else
            {
                openMenu();
            }
            opened = !opened;
        }

    }

    
}

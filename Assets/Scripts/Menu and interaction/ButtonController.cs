﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public GameObject panelToHide;
    public GameObject panelToShow;

    private GameObject menu;

    private void Start()
    {
        menu = GameObject.Find("Menu");
    }

    public void OnHover(bool onHover)
    {
        Button button = GetComponent<Button>();
        ColorBlock colors = button.colors;
        if (onHover)
        {
            colors.normalColor = new Color32(236, 146, 146, 255);
        }
        else
        {
            colors.normalColor = new Color32(255, 255, 255, 255);
        }
        button.colors = colors;
        //GetComponent<Image>().color = new Color(236, 146, 146, 255);
    }

    public virtual void OnClick()
    {
        if (menu == null)
        {
            menu = GameObject.Find("Menu");
        }
        if (panelToShow != null)
        {
            menu.GetComponent<MenuController>().openPanel(panelToHide, panelToShow);
        }
        else
        {
            menu.GetComponent<MenuController>().closeMenu();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Debug.Log("Colliding " + other.gameObject.name);
    }
}

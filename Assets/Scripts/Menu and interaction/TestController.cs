﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestController : ButtonController
{
    private Text buttonText;
    // Start is called before the first frame update
    void Start()
    {
        buttonText = GetComponentInChildren<Text>();
    }



    public override void OnClick()
    {
        base.OnClick();
        if(ApplicationModel.mode == "Test")
        {
            ApplicationModel.mode = "simple";
            buttonText.text = "Launch Test";
        }
        else
        {
            ApplicationModel.mode = "Test";
            buttonText.text = "Stop Test";
        }
    }
}

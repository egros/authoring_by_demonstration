﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    private GameObject miniature;
    private bool opened=false;
    private GameObject myCamera;
    private GameObject avatar;
    private GameObject player;


    public SteamVR_Action_Boolean inputNav;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        avatar = GameObject.Find("Avatar");
        miniature = GameObject.Find("MiniaturePlane");
        miniature.SetActive(false);

        myCamera = GameObject.Find("VRCamera");
        if (myCamera == null)
        {
            myCamera = GameObject.Find("FallbackObjects");
        }
        //miniature.transform.parent = Camera.main.transform;
        //miniature.transform.localPosition = new Vector3(0, 0, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("n"))
        {
            if (opened)
            {
                miniature.SetActive(false);
            }
            else
            {
                miniature.SetActive(true);
                float yRotation = myCamera.transform.rotation.eulerAngles.y - miniature.transform.rotation.eulerAngles.y;
                double angle = Math.PI * (myCamera.transform.rotation.eulerAngles.y / 180f);
                float cosY = (float)Math.Cos(angle);
                float sinY = (float)Math.Sin(angle);
                miniature.transform.position = new Vector3(myCamera.transform.position.x + sinY/1.2f, myCamera.transform.position.y-0.5f, myCamera.transform.position.z + cosY/1.2f);
                miniature.transform.Rotate(0f, yRotation, 0);

                //Debug.Log("Camera position: " + myCamera.transform.position);
                //Debug.Log("Camera local position: " + GameObject.Find("MainFloor").transform.InverseTransformPoint(myCamera.transform.position));
                //Debug.Log("Player local position: " + player.transform.localPosition);
                Vector3 localPos = GameObject.Find("Floor").transform.InverseTransformPoint(myCamera.transform.position);
                avatar.transform.localPosition= new Vector3(localPos.x, 0.0f, localPos.z);
            }
            opened = !opened;
        }
    }
}

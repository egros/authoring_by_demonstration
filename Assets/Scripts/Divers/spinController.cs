﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spinController : MonoBehaviour
{
    public float xSpin;
    public float ySpin;
    public float zSpin;
    // Start is called before the first frame update
    void LateUpdate()
    {
        //transform.localRotation = new Quaternion();
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x + xSpin * Time.deltaTime, transform.localEulerAngles.y + ySpin * Time.deltaTime, transform.localEulerAngles.z + zSpin * Time.deltaTime);
        //transform.Rotate(xSpin * Time.deltaTime, ySpin * Time.deltaTime, zSpin * Time.deltaTime);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugController : MonoBehaviour
{

    private GameObject debugCanvas;
    private Text text;
    // Start is called before the first frame update
    void Start()
    {
        debugCanvas = GameObject.Find("DebugCanvas");
        debugCanvas.transform.parent = Camera.main.transform;
        debugCanvas.transform.localPosition = new Vector3(0f, -0.5f, 1);
        debugCanvas.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

        text = GameObject.Find("DebugText").GetComponent<Text>();
    }

    public void display(string message)
    {
        text.text = message;
    }
}

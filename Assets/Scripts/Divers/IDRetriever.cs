﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDRetriever : ScriptableObject
{
    public int retrieveObjectID(GameObject target)
    {
        ArObject arObj = target.GetComponent<ArObject>();
        EventIconController icon = target.GetComponent<EventIconController>();
        AreaColliderController zone = target.GetComponent<AreaColliderController>();
        string name = target.gameObject.name;

        GameObject leftHandCollider = GameObject.Find("HandColliderLeft(Clone)");
        GameObject rightHandCollider = GameObject.Find("HandColliderRight(Clone)");
        if (name == "WoodBody" || name  == "Player")
        {
            return 2;
        }
        else if (target.transform.IsChildOf(leftHandCollider.transform) || name == "SphereLeftHand" || name == "LeftHand")
        {
            return 1;
        }
        else if (target.transform.IsChildOf(rightHandCollider.transform) || name == "SphereRightHand" || name == "RightHand")
        {
            return 0;
        }
        else if (arObj != null)
        {
            return arObj.getID();
        }
        else if (icon != null)
        {
            icon.getID();
        }
        else if (zone != null)
        {
            return zone.getID();
        }
        return -1;
    }
}

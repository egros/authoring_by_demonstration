﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.Newtonsoft.Json;

public class TurnTo : ScriptableObject
{
    public string turnTabToString(float[][] tab)
    {
        string result = "[";
        for (int i = 0; i < tab.Length; i++)
        {
            float[] t = tab[i];
            string subTab = "[";
            for (int j = 0; j < t.Length - 1; j++)
            {
                string f = t[j].ToString();
                f = f.Replace(',', '.');
                subTab = subTab + f + ",";
            }
            string fl = t[t.Length - 1].ToString();
            fl = fl.Replace(',', '.');
            subTab = subTab + fl + "]";
            result = result + subTab;
            if (i < (tab.Length - 1))
            {
                result = result + ", ";
            }
        }
        result = result + "]";
        return result;
    }

    public float[][] turnVectorListToTab(List<Vector3> vecList)
    {
        float[][] tab = new float[vecList.Count][];
        for (int i = 0; i < vecList.Count; i++)
        {
            tab[i] = new float[] { vecList[i].x, vecList[i].y, vecList[i].z };
        }
        return tab;
    }

    public float[][] turnQuaternionListToTab(List<Quaternion> quatList)
    {
        float[][] tab = new float[quatList.Count][];
        for (int i = 0; i < quatList.Count; i++)
        {
            tab[i] = new float[] { quatList[i].x, quatList[i].y, quatList[i].z, quatList[i].w };
        }
        return tab;
    }

    public List<TransformReccord> turnStringToTransformList(string positions, string rotations)
    {
        float[][] pos = JsonConvert.DeserializeObject<float[][]>(positions);
        float[][] rot = JsonConvert.DeserializeObject<float[][]>(rotations);
        //var result = objects.Select(obj => JsonConvert.SerializeObject(obj)).ToArray();
        List<TransformReccord> transforms = new List<TransformReccord>();
        for (int i = 0; i < Mathf.Min(pos.Length, rot.Length); i++)
        {
            Vector3 v = new Vector3(pos[i][0], pos[i][1], pos[i][2]);
            Quaternion q = new Quaternion(rot[i][0], rot[i][1], rot[i][2], rot[i][3]);
            TransformReccord transRec = new TransformReccord(v, q);
            transforms.Add(transRec);
        }
        return transforms;
    }

    public List<Vector3> turnStringToVectorList(string positions)
    {
        float[][] pos = JsonConvert.DeserializeObject<float[][]>(positions);
        List<Vector3> result = new List<Vector3>();

        for (int i = 0; i < pos.Length; i++)
        {
            Vector3 v = new Vector3(pos[i][0], pos[i][1], pos[i][2]);
            result.Add(v);
        }
        return result;
    }
}

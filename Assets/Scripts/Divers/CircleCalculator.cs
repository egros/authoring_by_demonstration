﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleCalculator : ScriptableObject
{
    public Vector3 getIntersection(Vector3 center2, float r, float R)
    {
        // Formulas from https://members.loria.fr/DRoegel/loc/note0001.pdf

        // We assume that the first circle center is (0,0)
        float a = 2 * center2.x;
        float b = 2 * center2.z;
        float c = Mathf.Pow(center2.x, 2) + Mathf.Pow(center2.z, 2) - Mathf.Pow(R, 2) + Mathf.Pow(r, 2);

        float delta = Mathf.Pow(2 * a * c, 2) - 4 * (Mathf.Pow(a, 2) + Mathf.Pow(b, 2)) * (Mathf.Pow(c, 2) - Mathf.Pow(b, 2) * Mathf.Pow(r, 2));

        float xp = (2 * a * c - Mathf.Sqrt(delta)) / (2 * (Mathf.Pow(a, 2) + Mathf.Pow(b, 2)));
        float xq = (2 * a * c + Mathf.Sqrt(delta)) / (2 * (Mathf.Pow(a, 2) + Mathf.Pow(b, 2)));

        float zp = 0;
        float zq = 0;
        if (b != 0)
        {
            zp = (c - a * xp) / b;
            zq = (c - a * xq) / b;
        }
        else
        {
            float rac = Mathf.Pow(R, 2) - Mathf.Pow((2 * c - Mathf.Pow(a, 2)) / (2 * a), 2);
            zp = (b / 2) + Mathf.Sqrt(rac);
            zq = (b / 2) - Mathf.Sqrt(rac);
        }
        



        Vector3 intersect = Vector3.zero;
        if (center2.x == 0)
        {
            if (center2.z > 0)
            {
                if (xp > xq)
                {
                    intersect = new Vector3(xp, center2.y, zp);
                }
                else
                {
                    intersect = new Vector3(xq, center2.y, zq);
                }
            }
            else
            {
                if (xp > xq)
                {
                    intersect = new Vector3(xq, center2.y, zq);
                }
                else
                {
                    intersect = new Vector3(xp, center2.y, zp);
                }
            }
        }
        else if (center2.x > 0)
        {
            if (zp < zq)
            {
                intersect = new Vector3(xp, center2.y, zp);
            }
            else
            {
                intersect = new Vector3(xq, center2.y, zq);
            }
        }
        else
        {
            if (zp > zq)
            {
                intersect = new Vector3(xp, center2.y, zp);
            }
            else
            {
                intersect = new Vector3(xq, center2.y, zq);
            }
        }

        return intersect;
    }

    public Vector3 getlineNCircleIntersect(Vector3 point, float r)
    {
        float a = Mathf.Pow(r, 2) / (Mathf.Pow(point.x, 2) + Mathf.Pow(point.y, 2) + Mathf.Pow(point.z, 2));
        float t = Mathf.Sqrt(a);

        Vector3 v = new Vector3(t * point.x, t * point.y, t * point.z);
        return v;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct LineEquation
{
    public float a;
    public float b;

    public LineEquation(float coefA, float interceptB)
    {
        a = coefA;
        b = interceptB;
    }
}

public struct PolarCoordinate
{
    public float alpha;
    public float r;

    public PolarCoordinate(float angle, float radius)
    {
        alpha = angle;
        r = radius;
    }
}

public class CubeCalculator : ScriptableObject
{
    public Vector3[] localPointsFromAnchors(Vector3 C, Vector3 G)
    {
        Vector3[] points = new Vector3[8];
        points[0] = Vector3.zero;
        points[1] = new Vector3(0, C.y, 0);
        points[2] = C;
        points[3] = new Vector3(C.x, 0, C.z);


        float dx = G.x - C.x;
        float dz = G.z - C.z;
        points[4] = new Vector3(dx, 0, dz);
        points[5] = new Vector3(dx, C.y, dz);
        points[6] = G;
        points[7] = new Vector3(G.x, 0, G.z);

        /*if (belongToPerpendicularPlan(C, G) && C.y==G.y)
        {
            
        }
        else
        {
            Debug.LogError("G does not belong to the perpendicular axis");
        }*/
        return points;
    }

    public LineEquation perpendicularEquation(Vector3 B)
    {
        float ap = 0;
        float bp = 0;
        // A = (0,0)
        // Equation of AB : z = a.X 
        // b = 0
        if (B.x != 0 && B.z!=0)
        {
            float a = B.z / B.x;
            // BC perpendicular to AB
            // Equation of BC : z = ap.X + bp
            // We need a.ap = -1 for BC and AB to be perpendicular
            ap = -1 / a;
            //bp = B.z - (Mathf.Pow(B.x,2)/B.z);
            bp = B.z - ap * B.x;
        }
        else
        {
            ap = 0;
        }
        
        LineEquation equa = new LineEquation(ap, bp);
        return equa;
    }

    public bool belongToPerpendicularPlan(Vector3 B, Vector3 C)
    {
        float delta = 0;
        if (B.x == 0)
        {
            delta = C.z - B.z;
        }
        else
        {
            if (B.z == 0)
            {
                delta = C.x - B.x;
            }
            else
            {
                LineEquation perpEq = perpendicularEquation(B);
                float z = perpEq.a * C.x + perpEq.b;
                delta = z - C.z;
            }
        }
        
        
        if (delta < 0.001f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Vector2 getPointG(float xC, float zC, float distance)
    {
        PolarCoordinate cPoint = getPolarCoordinate(xC, zC);
        float r = Mathf.Sqrt(Mathf.Pow(distance, 2) + Mathf.Pow(cPoint.r, 2));
        float alpha = 0;
        float beta = Mathf.Acos(cPoint.r / r);
        if (distance > 0)
        {
            alpha = cPoint.alpha + beta;
        }
        else
        {
            alpha = cPoint.alpha - beta;
        }

        return getOrthogonalCoordinate(alpha, r);
    }

    public Vector3 recalculateGfromC(Vector3 pointC, float distance, bool trigoSide)
    {
        Vector3 pointG = Vector3.zero;
        if (pointC.x == 0)
        {
            // G = (Cx +/- distance, Cy, Cz)
            if (pointC.z>=0)
            {
                if (trigoSide)
                {
                    pointG = new Vector3(-distance, pointC.y, pointC.z);
                }
                else
                {
                    pointG = new Vector3(distance, pointC.y, pointC.z);
                }
            }
        }
        else if (pointC.z == 0)
        {
            // G = (Cx, Cy, Cz +/- distance)
            if (pointC.x >= 0)
            {
                if (trigoSide)
                {
                    pointG = new Vector3(pointC.x, pointC.y, distance);
                }
                else
                {
                    pointG = new Vector3(pointC.x, pointC.y, -distance);
                }
            }
        }
        else
        {
            LineEquation eq = perpendicularEquation(pointC);
            float aeq = eq.a;
            float beq = eq.b;

            float a = 1 + Mathf.Pow(aeq, 2);
            float b = -2 * pointC.x + 2 * aeq * beq - 2 * aeq * pointC.z;
            float c = -distance - Mathf.Pow(pointC.x, 2) + Mathf.Pow(beq, 2) + Mathf.Pow(pointC.z, 2) - 2 * beq * pointC.z;

            float delta = Mathf.Pow(b, 2) - 4 * a * c;

            if(delta < 0)
            {
                if (Mathf.Abs(pointC.x) == Mathf.Min(Mathf.Abs(pointC.x), Mathf.Abs(pointC.z)))
                {
                    if (trigoSide)
                    {
                        pointG = new Vector3(-distance, pointC.y, pointC.z);
                    }
                    else
                    {
                        pointG = new Vector3(distance, pointC.y, pointC.z);
                    }
                }
                else
                {
                    if (trigoSide)
                    {
                        pointG = new Vector3(pointC.x, pointC.y, distance);
                    }
                    else
                    {
                        pointG = new Vector3(pointC.x, pointC.y, -distance);
                    }
                }
            }
            else if(delta == 0)
            {
                float gX = -b / (2 * a);
                float gZ = aeq * gX + beq;
                pointG = new Vector3(gX, pointC.y, gZ);
            }
            else
            {
                float x1 = (-b - Mathf.Sqrt(delta)) / (2 * a);
                float x2 = (-b + Mathf.Sqrt(delta)) / (2 * a);

                float xMin = Mathf.Min(x1, x2);
                float xMax = Mathf.Max(x1, x2);
                float zMin = aeq * xMin + beq;
                float zMax = aeq * xMax + beq;

                // There are two possible solutions for G
                if (pointC.x > 0)
                {
                    if (pointC.z > 0)
                    {
                        if (trigoSide)
                        {
                            // Gx<Cx
                            pointG = new Vector3(xMin, pointC.y, zMin);
                        }
                        else
                        {
                            // Gx>Cx
                            pointG = new Vector3(xMax, pointC.y, zMax);
                        }
                    }
                    else
                    {
                        if (trigoSide)
                        {
                            // Gx > Cx
                            pointG = new Vector3(xMax, pointC.y, zMax);
                        }
                        else
                        {
                            // Gx < Cx
                            pointG = new Vector3(xMin, pointC.y, zMin);
                        }
                    }
                }
                else // Cx<0
                {
                    if (pointC.z > 0)
                    {
                        if (trigoSide)
                        {
                            // Gx < Cx
                            pointG = new Vector3(xMin, pointC.y, zMin);
                        }
                        else
                        {
                            // Gx > Cx
                            pointG = new Vector3(xMax, pointC.y, zMax);
                        }
                    }
                    else
                    {
                        if (trigoSide)
                        {
                            // Gx > Cx
                            pointG = new Vector3(xMax, pointC.y, zMax);
                        }
                        else
                        {
                            // Gx < Cx
                            pointG = new Vector3(xMin, pointC.y, zMin);
                        }
                    }
                }
            }
        }
        if (Mathf.Abs(pointG.z) > 9000)
        {
            Debug.Log("z =" + pointG.z + " => z = distance");
            pointG = new Vector3(pointG.x, pointG.y, distance);
        }
        return pointG;
    }

    public bool getTrigoDirection(Vector3 C, Vector3 G)
    {
        if (C.x > 0)
        {
            if (C.z == 0)
            {
                if (G.z > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (C.z > 0)
            {
                if(G.x< C.x)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else // Cz<0
            {
                if(G.x > C.x)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else if(C.x == 0)
        {
            if(C.z == 0)
            {
                return true;
            }
            else if(C.z > 0)
            {
                if (G.x < 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else // Cz < 0
            {
                if (G.x > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else // Cx<0
        {
            if (C.z > 0)
            {
                if (G.x < C.x)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (C.z == 0)
            {
                if (G.z < 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else // Cz<0
            {
                if (G.x > C.x)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    public Vector3 recalculateCFromG(Vector3 C, Vector3 G)
    {
        float xc = C.x;
        float zc = C.z;
        if (C.x == 0)
        {
            zc = G.z;
        }
        else if (C.z == 0)
        {
            xc = G.x;
        }
        else
        {
            float m = C.z / C.x;
            LineEquation perpendicular = perpendicularEquation(C);
            float mp = perpendicular.a;
            float br = G.z - mp * G.x;
            xc = br / (m - mp);
            zc = m * xc;
        }
        
        return new Vector3(xc, G.y, zc);
    }

    public Vector3 getCenter(Vector3 C, Vector3 G)
    {
        // Square ACGF with A(0,0)
        float xM = 0;
        float zM = 0;
        float yM = G.y / 2;

        if (C.x == 0)
        {
            xM = (C.x + G.x) / 2;
            zM = C.z / 2;
        }
        else if (C.z == 0)
        {
            xM = C.x / 2;
            zM = (C.z + G.z / 2);
        }
        else
        {
            float m1 = C.z / C.x;
            LineEquation eq = perpendicularEquation(C);
            float m2 = eq.a;

            Vector3 l1 = new Vector3(C.x / 2, C.y, C.z / 2);
            Vector3 l2 = new Vector3((C.x + G.x) / 2, C.y, (C.z + G.z) / 2);

            float b2 = l1.z - m2 * l1.x;
            float b1 = l2.z - m1 * l2.x;

            xM = (b1 - b2) / (m2 - m1);
            zM = m1 * xM + b1;
        }

        return new Vector3(xM, yM, zM);
    }

    public PolarCoordinate getPolarCoordinate(float x, float y)
    {
        float r = Mathf.Sqrt(Mathf.Pow(x, 2) + Mathf.Pow(y, 2));
        float alpha = Mathf.Acos(x / r);
        if (y < 0)
        {
            alpha = -alpha;
        }
        return new PolarCoordinate(alpha, r);
    }
    
    public Vector2 getOrthogonalCoordinate(float alpha, float r)
    {
        float x = r * Mathf.Cos(alpha);
        float y = r * Mathf.Sin(alpha);
        return new Vector2(x, y);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class ScrollController : MonoBehaviour
{
    private ScrollRect scrollerRect;
    private RectTransform scrollWindow;
    private SteamVR_Action_Boolean downInput;
    private SteamVR_Action_Boolean upInput;

    // Start is called before the first frame update
    void Start()
    {
        scrollerRect = GetComponent<ScrollRect>();
        scrollWindow = scrollerRect.GetComponent<RectTransform>();
        downInput = SteamVR_Input.GetBooleanAction("Back");
        upInput = SteamVR_Input.GetBooleanAction("Forward");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("u") || upInput.lastStateDown)
        {
            scrollerRect.verticalNormalizedPosition = Mathf.Min(1f, scrollerRect.verticalNormalizedPosition + 0.1f);

        }
        if (Input.GetKeyUp("d") || downInput.lastStateDown)
        {
            scrollerRect.verticalNormalizedPosition = Mathf.Max(0f, scrollerRect.verticalNormalizedPosition - 0.1f);

        }
    }

    public void setScroll(float ratio)
    {
        scrollerRect.verticalNormalizedPosition = ratio;
    }
}

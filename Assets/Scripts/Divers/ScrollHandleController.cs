﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.Extras;

public class ScrollHandleController : MonoBehaviour
{
    private ScrollController scroller;
    private RectTransform slidingArea;
    private float posUp;
    private float posDown;
    private float height;

    private SceneHandler sceneHandler;
    private bool isSelected;

    void Start()
    {
        sceneHandler = GameObject.Find("RightHand").GetComponent<SceneHandler>();
        scroller = GetComponentInParent<ScrollController>();
        slidingArea = transform.parent.GetComponent<RectTransform>();
        posUp = slidingArea.rect.yMax;
        posDown = slidingArea.rect.yMin;
        height = Mathf.Abs(posUp - posDown);
    }

    public void select(bool selecting)
    {
        isSelected = selecting;
    }

    // Update is called once per frame
    void Update()
    {
        if (isSelected && sceneHandler.isHittingScrollHandle())
        {
            Vector3 hitPoint = sceneHandler.getLaserHitPoint();
            float ratio = (slidingArea.InverseTransformPoint(hitPoint).y - posDown) / height;
            ratio = Mathf.Min(1f, ratio);
            ratio = Mathf.Max(0f, ratio);
            scroller.setScroll(ratio);
        }
    }
}

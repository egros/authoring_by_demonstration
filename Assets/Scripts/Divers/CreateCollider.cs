﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CreateCollider : MonoBehaviour
{

    List<float> x, y, z;
    
    
    // Start is called before the first frame update
    void Start()
    {
        x = new List<float>();
        y = new List<float>();
        z = new List<float>();


        if(gameObject.GetComponent<Renderer>() != null)
        {
            Renderer rM = gameObject.GetComponent<Renderer>();
            x.Add(rM.bounds.min.x);
            x.Add(rM.bounds.max.x);

            y.Add(rM.bounds.min.y);
            y.Add(rM.bounds.max.y);

            z.Add(rM.bounds.min.z);
            z.Add(rM.bounds.max.z);

        }

        foreach (Transform child in transform)
        {
            if (child.gameObject.GetComponent<Renderer>() != null)
            {
                Renderer r = child.gameObject.GetComponent<Renderer>();
                x.Add(r.bounds.min.x);
                x.Add(r.bounds.max.x);

                y.Add(r.bounds.min.y);
                y.Add(r.bounds.max.y);

                z.Add(r.bounds.min.z);
                z.Add(r.bounds.max.z);
            }
        }

        // Debug.Log(x.Min());
        // Debug.Log(x.Max());
        if (x.Count != 0)
        {
            float minX = x.Min();
            float maxX = x.Max();

            float minY = y.Min();
            float maxY = y.Max();

            float minZ = z.Min();
            float maxZ = z.Max();


            Vector3 center = new Vector3(minX + (maxX - minX) / 2, minY + (maxY - minY) / 2, minZ + (maxZ - minZ) / 2);
            Vector3 size = new Vector3(maxX - minX, maxY - minY, maxZ - minZ);

            BoxCollider BC = gameObject.AddComponent<BoxCollider>();
            BC.center = transform.InverseTransformPoint(center);
            BC.size = size;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

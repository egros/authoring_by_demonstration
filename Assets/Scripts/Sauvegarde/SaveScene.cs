﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveScene : ScriptableObject
{
    public void writeOnFile(string filename)
    {
        ArObject[] arObjectsInScene = GameObject.FindObjectsOfType<ArObject>();
        string jsonString = "{\"objects\" : [\n";
        for (int i = 0; i < arObjectsInScene.Length; i++)
        {
            ArObject ar = arObjectsInScene[i];
            ObjectDescription descr = ar.getObjectDescription();
            jsonString = jsonString + JsonUtility.ToJson(descr);
            if (i < arObjectsInScene.Length - 1)
            {
                jsonString = jsonString + ", \n";
            }
        }
        jsonString = jsonString + " \n], \n \"events\": [";
        EventIconController[] eventsInScene = GameObject.FindObjectsOfType<EventIconController>();
        AreaColliderController[] areaControllers = GameObject.FindObjectsOfType<AreaColliderController>();
        //ZoneColliderController[] zoneColliders = GameObject.FindObjectsOfType<ZoneColliderController>();
        for (int k = 0; k < areaControllers.Length; k++)
        {
            AreaColliderController znCol = areaControllers[k];
            EventDescription zoneDescr = znCol.getDescription();
            jsonString = jsonString + JsonUtility.ToJson(zoneDescr);
            if (k < areaControllers.Length - 1 || eventsInScene.Length > 0)
            {
                jsonString = jsonString + ", \n";
            }
        }
        /*for (int k=0; k<zoneColliders.Length; k++)
        {
            ZoneColliderController znCol = zoneColliders[k];
            EventDescription zoneDescr = znCol.getDescription();
            jsonString = jsonString + JsonUtility.ToJson(zoneDescr);
            if (k < zoneColliders.Length - 1 || eventsInScene.Length>0)
            {
                jsonString = jsonString + ", \n";
            }
        }*/

        for (int j = 0; j < eventsInScene.Length; j++)
        {
            EventIconController ev = eventsInScene[j];
            EventDescription evDescr = ev.getDescription();
            jsonString = jsonString + JsonUtility.ToJson(evDescr);
            if (j < eventsInScene.Length - 1)
            {
                jsonString = jsonString + ", \n";
            }
        }
        jsonString = jsonString + "]}";
        // Write JSON to file.
        File.WriteAllText(filename, jsonString);
    }

    public void writeOnNewFile()
    {
        string timing = DateTime.Now.ToString();
        timing = timing.Replace("/", "_");
        timing = timing.Replace("\\","_");
        timing = timing.Replace(" ", "_");
        timing = timing.Replace(":", "");

        string path = Application.streamingAssetsPath + "/UserEvolution/" + timing+".txt";
        FileStream fileStr = File.Create(path);
        fileStr.Close();
        writeOnFile(path);
    }
}

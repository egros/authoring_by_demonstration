﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticSave : MonoBehaviour
{
    SaveScene saver;
    // Start is called before the first frame update
    void Start()
    {
        saver = new SaveScene();
        InvokeRepeating("WaitAndSave", 30.0f, 30.0f);
    }

    private void WaitAndSave()
    {
        //Debug.Log("Wait and save");
        saver.writeOnNewFile();
    }
}

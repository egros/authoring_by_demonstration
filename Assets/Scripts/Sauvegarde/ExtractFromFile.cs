﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.XR.OpenVR.SimpleJSON;
using UnityEngine;
using UnityEngine.UI;
using Valve.Newtonsoft.Json;
using Valve.VR.InteractionSystem;

// source: https://answers.unity.com/questions/1473952/how-to-write-and-read-json-in-unity.html

public struct LinkToSet
{
    public int trigger;
    public List<int> animations;

    public LinkToSet(int trig)
    {
        trigger = trig;
        animations = new List<int>();
    }
}

public class ExtractFromFile : ScriptableObject
{
    private string path;
    private GameObject arObjectsParent;
    private Dictionary<int, ArObject> objectDict;
    private Dictionary<int, EventIconController> eventDict;
    private Dictionary<int, AreaColliderController> zoneDict;
    private GameObject rightHand;
    private GameObject leftHand;
    private GameObject player;
    private List<LinkToSet> links;
    private InteractionControllerRecorder rightController;
    private InteractionControllerRecorder leftController;

    public void readFile(string filename)
    {
        rightHand = GameObject.Find("RightHand");
        leftHand = GameObject.Find("LeftHand");
        player = GameObject.Find("Player");
        arObjectsParent = GameObject.Find("ARCreations");
        objectDict = new Dictionary<int, ArObject>();
        eventDict = new Dictionary<int, EventIconController>();
        zoneDict = new Dictionary<int, AreaColliderController>();
        links = new List<LinkToSet>();
        rightController = rightHand.GetComponent<InteractionControllerRecorder>();
        leftController = leftHand.GetComponent<InteractionControllerRecorder>();

        path = Application.streamingAssetsPath + "/Sauvegardes/"+filename;
        string jsonString = File.ReadAllText(path);
        JSONNode data = JSON.Parse(jsonString);
        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();

        Quaternion quatNull = new Quaternion(0f, 0f, 0f, 0f);

        JSONNode objects = data["objects"];
        JSONNode events = data["events"];
        foreach (JSONNode node in objects)
        {
            string objType = node["mainType"];
            int id = node["id"].AsInt;
            string subType = node["subtype"];
            Vector3 position = new Vector3(node["position"][0].AsFloat, node["position"][1].AsFloat, node["position"][2].AsFloat);
            Quaternion rotation = new Quaternion(node["rotation"][0].AsFloat, node["rotation"][1].AsFloat, node["rotation"][2].AsFloat, node["rotation"][3].AsFloat);
            bool visibleAtStart = node["visible"];
            Color color = new Color(node["color"][0].AsFloat, node["color"][1].AsFloat, node["color"][2].AsFloat, node["color"][3].AsFloat);
            Vector3 scale = Vector3.zero;
            if (node["size"] != null)
            {
                scale = new Vector3(node["size"][0].AsFloat, node["size"][1].AsFloat, node["size"][2].AsFloat);
            }

            if(objType == "Geometry")
            {
                string name = "3DObjects\\"+subType;
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                GameObject instantiatedObject = Instantiate((GameObject)assetToGet, Vector3.zero, quatNull);
                instantiatedObject.transform.SetParent(arObjectsParent.transform);
                instantiatedObject.transform.position = position;
                instantiatedObject.transform.rotation = rotation;
                if (node["size"] != null)
                {
                    instantiatedObject.transform.localScale = scale;
                }
                instantiatedObject.layer = 9;

                ArObject arComponent = instantiatedObject.GetComponent<ArObject>();

                // Initial settings
                arComponent.initializeAttributes(id, visibleAtStart, color);
                objectDict.Add(id, arComponent);
            }
            else if(objType == "Images")
            {
                string name = "3DObjects\\ImagePrefab";
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                GameObject instantiatedObject = Instantiate((GameObject)assetToGet, Vector3.zero, quatNull);
                instantiatedObject.transform.SetParent(arObjectsParent.transform);
                instantiatedObject.transform.position = position;
                instantiatedObject.transform.rotation = rotation;
                if (node["size"] != null)
                {
                    instantiatedObject.transform.localScale = scale;
                }
                instantiatedObject.layer = 9;

                ArObject arComponent = instantiatedObject.GetComponent<ArObject>();

                // Add image to imagePrefab
                string imageName = objType + "\\" + subType;
                UnityEngine.Object imageToGet = Resources.Load<UnityEngine.Object>(imageName);
                Material imageMat = new Material(Shader.Find("Standard"));//"Unlit/Texture"));

                // smoothness
                imageMat.SetFloat("_Glossiness", 0f);
                //transparency
                imageMat.SetFloat("_Mode", 2);
                imageMat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                imageMat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                imageMat.SetInt("_ZWrite", 0);
                imageMat.DisableKeyword("_ALPHATEST_ON");
                imageMat.EnableKeyword("_ALPHABLEND_ON");
                imageMat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                imageMat.renderQueue = 3000;

                Texture2D imageTexture = imageToGet as Texture2D;
                imageMat.mainTexture = imageTexture;
                instantiatedObject.GetComponentInChildren<Image>().material = imageMat;

                // Initial settings
                arComponent.mainType = objType;
                arComponent.subtype = subType;
                arComponent.initializeAttributes(id, visibleAtStart, color);
                objectDict.Add(id, arComponent);
            }
            else if(objType == "3DProps")
            {
                string name = objType + "\\" + subType;
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                GameObject instantiatedObject = Instantiate((GameObject)assetToGet, Vector3.zero, quatNull);
                instantiatedObject.transform.SetParent(arObjectsParent.transform);
                instantiatedObject.transform.position = position;
                instantiatedObject.transform.rotation = rotation;
                if (node["size"] != null)
                {
                    instantiatedObject.transform.localScale = scale;
                }
                setLayer(instantiatedObject);

                if (instantiatedObject.GetComponent<Collider>() == null)
                {
                    ColliderMaker colMak = instantiatedObject.AddComponent<ColliderMaker>();
                    colMak.makeCollider();
                }
                ArObject arComponent = instantiatedObject.AddComponent<ArObject>();
                instantiatedObject.tag = "ArObjects";
                Interactable interact = instantiatedObject.AddComponent<Interactable>();
                interact.hideControllerOnAttach = false;
                instantiatedObject.AddComponent<SimpleAttach>();
                Rigidbody rigidBody = instantiatedObject.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
                rigidBody.useGravity = false;
                instantiatedObject.AddComponent<CircleDispatcher>();

                // Initial settings
                arComponent.mainType = objType;
                arComponent.subtype = subType;
                arComponent.initializeAttributes(id, visibleAtStart, color);
                objectDict.Add(id, arComponent);
            }
            
            ApplicationModel.idCpt = Mathf.Max(ApplicationModel.idCpt, id + 1);
        }
        foreach (JSONNode node in events)
        {
            string mainType = node["mainType"];
            int id = node["id"];
            int target = node["target"];
            int[] animations = new int[node["animations"].Count];
            for (int i = 0; i < animations.Length; i++)
            {
                animations[i] = node["animations"][i].AsInt;
            }
            int previousIcon = node["previousIcon"].AsInt;
            int nextIcon = node["nextIcon"].AsInt;
            Color color = new Color(node["color"][0].AsFloat, node["color"][1].AsFloat, node["color"][2].AsFloat, node["color"][3].AsFloat);
            int holdingHand = node["holdingHand"].AsInt;
            string trajectory = node["trajectory"];
            string rotations = node["rotations"];
            int collided = node["collided"].AsInt;
            bool followDirection = false;
            if (node["followDirection"] != null)
            {
                followDirection = node["followDirection"];
            }

            string name = "Events\\Icons\\" ;
            GameObject targetObj;
            bool isArObject = false;
            if (target == 0)
            {
                targetObj = rightHand;
            }
            else if (target == 1)
            {
                targetObj = leftHand;
            }
            else if (target == 2)
            {
                targetObj = player;
            }
            else if(target>2)
            {
                targetObj = objectDict[target].gameObject;
                isArObject = true;
            }
            else //targetObj==-1
            {
                targetObj = null;
            }

            if(mainType == "ZoneCollider")
            {
                name = "Events\\Icons\\ZoneCollider";
                List<Vector3> points = translater.turnStringToVectorList(trajectory);
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                GameObject instantiatedObject = Instantiate((GameObject)assetToGet, points[0], Quaternion.identity);
                ZoneColliderController zoneCol = instantiatedObject.GetComponent<ZoneColliderController>();
                Vector3 initPos = points[0];
                points.RemoveAt(0);
                zoneCol.setAllPoints(points, initPos, id);
                zoneDict.Add(id, zoneCol);
                //EventIconController evIcon = instantiatedObject.GetComponentInChildren<EventIconController>();
                //eventDict.Add(id, evIcon);
                //evIcon.setID(id);
            }
            else if(mainType== "Zone3DCollider")
            {
                name = "Events\\Icons\\3DZoneCollider";
                List<Vector3> points = translater.turnStringToVectorList(trajectory);
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                GameObject instantiatedObject = Instantiate((GameObject)assetToGet, points[0], Quaternion.identity);
                Zone3DColliderController zoneCol = instantiatedObject.GetComponent<Zone3DColliderController>();
                zoneCol.setAllVertex(points.ToArray());
                zoneDict.Add(id, zoneCol);
            }
            else
            {
                RecordedEvent recordedEvent = null;
                if (mainType == "Move")
                {
                    name = name + "MoveIcon";
                    List<TransformReccord> traj = translater.turnStringToTransformList(trajectory, rotations);
                    RecordedMovement record = ScriptableObject.CreateInstance<RecordedMovement>();
                    record.setTrajectory(traj, targetObj, isArObject);
                    record.setFollowDirection(followDirection);
                    recordedEvent = record;
                }
                else if (mainType == "Drag")
                {
                    name = name + "DragIcon";
                    List<TransformReccord> traj = translater.turnStringToTransformList(trajectory, rotations);
                    RecordedDrag record = ScriptableObject.CreateInstance<RecordedDrag>();
                    string hnd = "";
                    if (holdingHand == 0)
                    {
                        hnd = "RightHand";
                    }
                    else
                    {
                        hnd = "LeftHand";
                    }
                    record.setElements(targetObj, traj, hnd, traj[0].getPosition());
                    record.setFollowDirection(followDirection);
                    recordedEvent = record;
                }
                else if (mainType == "Grab")
                {
                    name = name + "GrabIcon";
                    RecordedGrab grab = ScriptableObject.CreateInstance<RecordedGrab>();
                    Vector3 v;
                    if (targetObj == null)
                    {
                        List<Vector3> pos = translater.turnStringToVectorList(trajectory);
                        v = pos[0];
                    }
                    else
                    {
                        v = targetObj.transform.position;
                    }
                    if (holdingHand == 0)
                    {
                        grab.setGrabbedObject(targetObj, v, rightHand);
                        rightController.addRecordedGrab(grab);
                    }
                    else
                    {
                        grab.setGrabbedObject(targetObj, v, leftHand);
                        leftController.addRecordedGrab(grab);
                    }
                    recordedEvent = grab;

                }
                else if (mainType == "Drop")
                {
                    name = name + "DropIcon";
                    RecordedDrop drop = ScriptableObject.CreateInstance<RecordedDrop>();
                    Vector3 v;
                    if (targetObj == null)
                    {
                        List<Vector3> pos = translater.turnStringToVectorList(trajectory);
                        v = pos[0];
                    }
                    else
                    {
                        v = targetObj.transform.position;
                    }
                    if (holdingHand == 0)
                    {
                        drop.setDroppedObject(targetObj, v, rightHand);
                        rightController.addRecordedDrop(drop);
                    }
                    else
                    {
                        drop.setDroppedObject(targetObj, v, leftHand);
                        leftController.addRecordedDrop(drop);
                    }
                    recordedEvent = drop;
                }
                else if (mainType == "Color")
                {
                    name = name + "ColorIcon";
                    RecordedColor colorEvent = ScriptableObject.CreateInstance<RecordedColor>();
                    colorEvent.setColorEvent(targetObj, color);
                    recordedEvent = colorEvent;
                }
                else if (mainType == "Collision")
                {
                    name = name + "CollisionIcon";
                    RecordedCollision collision = ScriptableObject.CreateInstance<RecordedCollision>();
                    GameObject collidedObj = null;
                    if (collided == 0)
                    {
                        collidedObj = rightHand;
                    }
                    else if (collided == 1)
                    {
                        collidedObj = leftHand;
                    }
                    else if (collided == 2)
                    {
                        collidedObj = player;
                    }
                    else
                    {
                        collidedObj = objectDict[collided].gameObject;
                    }
                    collision.setCollision(collidedObj, targetObj);
                    recordedEvent = collision;
                }
                else if (mainType == "Show")
                {
                    name = name + "ShowIcon";
                    RecordedShow showEvent = ScriptableObject.CreateInstance<RecordedShow>();
                    showEvent.setShow(targetObj);
                    recordedEvent = showEvent;
                }
                else if (mainType == "Hide")
                {
                    name = name + "HideIcon";
                    RecordedHide hideEvent = ScriptableObject.CreateInstance<RecordedHide>();
                    hideEvent.setHideEvent(targetObj);
                    recordedEvent = hideEvent;
                }
                else if(mainType=="Look")//Look
                {
                    name = name + "LookIcon";
                    RecordedLook lookEvent = ScriptableObject.CreateInstance<RecordedLook>();
                    lookEvent.setLook(targetObj);
                    recordedEvent = lookEvent;
                }
                else if(mainType == "EnterZone")
                {
                    name = name + "EnterZoneIcon";
                    RecordedZoneEnter enterZoneEvent = ScriptableObject.CreateInstance<RecordedZoneEnter>();
                    enterZoneEvent.setEvent(targetObj, zoneDict[collided]);
                    recordedEvent = enterZoneEvent;
                    zoneDict[collided].addEnterEvent(enterZoneEvent);
                }
                else if(mainType== "ExitZone")
                {
                    name = name + "ExitZoneIcon";
                    RecordedZoneExit exitZEvent = ScriptableObject.CreateInstance<RecordedZoneExit>();
                    exitZEvent.setEvent(targetObj, zoneDict[collided]);
                    recordedEvent = exitZEvent;
                    zoneDict[collided].addExitEvent(exitZEvent);
                }

                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                GameObject instantiatedObject = Instantiate((GameObject)assetToGet, Vector3.zero, Quaternion.identity);
                if (recordedEvent.getAttachParent() != null)
                {
                    instantiatedObject.transform.position = recordedEvent.getAttachParent().transform.position;
                    instantiatedObject.transform.SetParent(recordedEvent.getAttachParent().transform);
                    if(mainType!="EnterZone" && mainType != "ExitZone")
                    {
                        CircleDispatcher dispatch = recordedEvent.getAttachParent().GetComponent<CircleDispatcher>();
                        if (dispatch != null)
                        {
                            dispatch.haveToUpdate();
                        }
                        else
                        {
                            dispatch = recordedEvent.getAttachParent().AddComponent<CircleDispatcher>();
                            dispatch.haveToUpdate();
                        }
                    }
                    else
                    {
                        AreaColliderController zoneCollider = zoneDict[collided];
                        zoneCollider.dispatchEventIcons();
                    }
                }
                else
                {
                    instantiatedObject.transform.position = recordedEvent.getAttachPosition();
                    instantiatedObject.transform.SetParent(arObjectsParent.transform);
                }

                EventIconController eventIcon = instantiatedObject.GetComponent<EventIconController>();
                eventIcon.setRecordedEvent(recordedEvent, null, id);
                eventDict.Add(id, eventIcon);
                if (eventDict.ContainsKey(previousIcon))
                {
                    eventDict[previousIcon].setNextEventIcon(eventIcon.gameObject, true);
                }
                if (eventDict.ContainsKey(nextIcon))
                {
                    eventIcon.setNextEventIcon(eventDict[nextIcon].gameObject, true);
                }
            }
            ApplicationModel.iconIdx = Mathf.Max(ApplicationModel.iconIdx, id + 1);
            if (animations.Length > 0)
            {
                LinkToSet myLinks = new LinkToSet(id);
                foreach(int a in animations)
                {
                    myLinks.animations.Add(a);
                }
                links.Add(myLinks);
            }
        }
        
    }

    public void setLinksOnUpdate()
    {
        UnityEngine.Object linkAsset = Resources.Load<UnityEngine.Object>("Events\\Icons\\EventAnimationLink");
        //GameObject instantiatedLink;
        foreach (LinkToSet l in links)
        {
            foreach (int anim in l.animations)
            {
                Debug.Log("Looking for icons n°" + l.trigger + " and n°" + anim);
                eventDict[l.trigger].linkToAnimation(eventDict[anim]);
                /*instantiatedLink = Instantiate((GameObject)linkAsset, Vector3.zero, Quaternion.identity);
                instantiatedLink.transform.SetParent(arObjectsParent.transform);
                instantiatedLink.layer = 10;
                EventAnimationLinkController linkController = instantiatedLink.GetComponent<EventAnimationLinkController>();
                linkController.setTriggerAnimation(eventDict[l.trigger], eventDict[anim]);*/
            }
        }
    }

    /*public List<TransformReccord> turnStringToTransformList(string positions, string rotations)
    {
        float[][] pos = JsonConvert.DeserializeObject<float[][]>(positions);
        float[][] rot = JsonConvert.DeserializeObject<float[][]>(rotations);
        //var result = objects.Select(obj => JsonConvert.SerializeObject(obj)).ToArray();
        List<TransformReccord> transforms = new List<TransformReccord>();
        for(int i=0; i<Mathf.Min(pos.Length, rot.Length); i++)
        {
            Vector3 v = new Vector3(pos[i][0], pos[i][1], pos[i][2]);
            Quaternion q = new Quaternion(rot[i][0], rot[i][1], rot[i][2], rot[i][3]);
            TransformReccord transRec = new TransformReccord(v, q);
            transforms.Add(transRec);
        }
        return transforms;
    }

    public List<Vector3> turnStringToVectorList(string positions)
    {
        float[][] pos = JsonConvert.DeserializeObject<float[][]>(positions);
        List<Vector3> result = new List<Vector3>();

        for (int i = 0; i <pos.Length; i++)
        {
            Vector3 v = new Vector3(pos[i][0], pos[i][1], pos[i][2]);
            result.Add(v);
        }
        return result;
    }*/

    private void setLayer(GameObject obj)
    {
        obj.layer = 9;
        foreach (Transform child in obj.transform)
        {
            setLayer(child.gameObject);
        }
    }
}

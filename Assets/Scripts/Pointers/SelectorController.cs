﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class SelectorController : MonoBehaviour
{
    private Hand dominantHand;
    private GameObject selectorReferent;
    private GameObject arParent;

    private Valve.VR.SteamVR_Action_Boolean input;
    private DateTime timeSinceLastTrigger;


    // Start is called before the first frame update
    void Start()
    {
        selectorReferent = GameObject.Find("SelectorReference");
        arParent = GameObject.Find("ARCreations");
        dominantHand = GameObject.Find(ApplicationModel.dominantHand).GetComponent<Hand>();
        timeSinceLastTrigger = DateTime.Now;
        input = SteamVR_Input.GetBooleanAction("GrabGrip");

    }

    private void Update()
    {
        transform.LookAt(transform.parent);
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "ArObjects" && (other.gameObject.transform.parent == arParent.transform || other.gameObject.transform.parent == selectorReferent.transform))
        {
            dominantHand.ShowGrabHint();

            if (input.lastStateDown)
            {
                System.TimeSpan diff = DateTime.Now.Subtract(timeSinceLastTrigger);
                if (diff.TotalSeconds > 0.1)
                {
                    if (other.gameObject.GetComponent<ArObject>() != null)
                    {
                        ArObject arObj = other.gameObject.GetComponent<ArObject>();
                        if (arObj.getIsSelected())
                        {
                            arObj.enableOutline(false);
                            other.gameObject.transform.SetParent(arParent.transform);
                            // Remove from selected objects
                        }
                        else
                        {
                            arObj.enableOutline(true);
                            other.gameObject.transform.SetParent(selectorReferent.transform);
                            //Add to list of selected objects
                        }
                        dominantHand.HideGrabHint();
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "ArObjects")
        {

            dominantHand.HideGrabHint();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;

public class ResizingLaserController : MonoBehaviour
{
    public SteamVR_LaserPointer laserPointer;
    public SceneHandler sceneHandler;
    private Component[] laserRenderers;
    private Renderer laserRenderer;
    private GameObject objectToResize;
    private int objectID;
    private SteamVR_Action_Boolean input;
    private float initialDistance;
    private GameObject collidedObject;
    private Vector3 initialScale;
    private bool laserIsVisible;

    // Start is called before the first frame update
    void Start()
    {
        objectID = -1;
        input = SteamVR_Input.GetBooleanAction("LeftGrab");
    }

    private void Awake()
    {
        laserPointer.PointerIn += PointerInside;
        laserPointer.PointerOut += PointerOutside;
        //laserPointer.PointerClick += PointerClick;
    }

    // Update is called once per frame
    void Update()
    {
        if (laserRenderer == null)
        {
            laserRenderers = laserPointer.GetComponentsInChildren(typeof(Renderer));
            foreach (Renderer renderer in laserRenderers)
            {
                if (renderer.name == "Cube")
                {
                    laserRenderer = renderer;
                }
            }
        }
        if (objectToResize != null)
        {
            if (input.lastStateUp || (objectID != sceneHandler.getAttachedObjectID()))
            {
                ArObject arComponent = objectToResize.GetComponent<ArObject>();
                arComponent.attachAttributes(true);
                arComponent.attachEvents(true);
                objectToResize = null;
                objectID = -1;
            }
            else
            {
                if(getCollidedObjectID() == objectID && sceneHandler.getCollidedObjectID() == objectID)
                {
                    float newDistance = Vector3.Distance(laserPointer.hitPoint, sceneHandler.getLaserHitPoint());
                    float rescaleFactor = newDistance / initialDistance;
                    if (!float.IsNaN(rescaleFactor))
                    {
                        objectToResize.transform.localScale = initialScale * rescaleFactor;
                    }
                }

            }
        }
        else if(input.lastStateDown && collidedObject!=null)
        {
            ArObject arComponent = collidedObject.gameObject.GetComponent<ArObject>();
            if (arComponent != null)
            {
                if (arComponent.getID() == sceneHandler.getAttachedObjectID())
                {
                    objectToResize = collidedObject.gameObject;
                    objectID = arComponent.getID();
                    arComponent.attachAttributes(false);
                    arComponent.hideAttributes();
                    arComponent.attachEvents(false);
                    initialScale = objectToResize.transform.localScale;
                    initialDistance = Vector3.Distance(laserPointer.hitPoint, sceneHandler.getLaserHitPoint());
                }
            }
        }
        

    }

    public int getCollidedObjectID()
    {
        if (collidedObject != null)
        {
            ArObject arComponent = collidedObject.GetComponent<ArObject>();
            if (arComponent != null)
            {
                return arComponent.getID();
            }
        }
        return -1;
    }

    public void PointerInside(object sender, PointerEventArgs e)
    {
        collidedObject = e.target.gameObject;
    }

    public void PointerOutside(object sender, PointerEventArgs e)
    {
        collidedObject = null;
    }

    public void switchLaser(bool onOff)
    {
        laserIsVisible = onOff;
        laserRenderer.enabled = onOff;
        laserPointer.enabled = onOff;
    }

    public void inverseLaser()
    {
        laserIsVisible = !laserIsVisible;
        laserRenderer.enabled = laserIsVisible;
        laserPointer.enabled = laserIsVisible;
    }
}

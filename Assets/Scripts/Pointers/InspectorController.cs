﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class InspectorController : MonoBehaviour
{
    private Hand dominantHand;
    private Valve.VR.SteamVR_Action_Boolean input;
    private DateTime timeSinceLastTrigger;

    // Start is called before the first frame update
    void Start()
    {
        dominantHand = GameObject.Find(ApplicationModel.dominantHand).GetComponent<Hand>();
        input = SteamVR_Input.GetBooleanAction("GrabGrip");
        timeSinceLastTrigger = DateTime.Now;
    }

    private void OnTriggerStay(Collider other)
    {
        ArObject arComponent = other.gameObject.GetComponent<ArObject>();
        if (arComponent != null)
        {
            dominantHand.ShowGrabHint();
            if (input.lastStateDown)
            {
                System.TimeSpan diff = DateTime.Now.Subtract(timeSinceLastTrigger);
                if (diff.TotalSeconds > 0.1)
                {
                    arComponent.hideShowAttributes();
                    dominantHand.HideGrabHint();
                    timeSinceLastTrigger = DateTime.Now;
                }

                
            }
        }

        EventIconController eventIcon = other.gameObject.GetComponent<EventIconController>();
        if(eventIcon!=null && (eventIcon.getRecordedEvent().getActionName() == "Move" || eventIcon.getRecordedEvent().getActionName() == "Drag"))
        {
            dominantHand.ShowGrabHint();
            if (input.lastStateDown)
            {
                System.TimeSpan diff = DateTime.Now.Subtract(timeSinceLastTrigger);
                if (diff.TotalSeconds > 0.1)
                {
                    eventIcon.inspect();
                    dominantHand.HideGrabHint();
                    timeSinceLastTrigger = DateTime.Now;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        dominantHand.HideGrabHint();
    }
}

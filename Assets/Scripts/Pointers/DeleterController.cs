﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class DeleterController : MonoBehaviour
{
    private Hand dominantHand;
    private Valve.VR.SteamVR_Action_Boolean input;
    private GameObject arParent;
    private GameObject selectorReferent;
    private InteractionArObjectsRecorder arObjectsRecorder;

    // Start is called before the first frame update
    void Start()
    {
        selectorReferent = GameObject.Find("SelectorReference");
        arParent = GameObject.Find("ARCreations");
        dominantHand = GameObject.Find(ApplicationModel.dominantHand).GetComponent<Hand>();
        transform.parent = GameObject.Find("RightHand").transform;
        transform.localPosition = new Vector3(0f, 0f, 0.05f);
        transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        input = SteamVR_Input.GetBooleanAction("GrabGrip");
        arObjectsRecorder = GameObject.FindObjectOfType<InteractionArObjectsRecorder>();
    }

    /*// Update is called once per frame
    void Update()
    {
        
    }*/

    private void OnTriggerStay(Collider other)
    {
        bool cond1 = (other.gameObject.tag == "ArObjects");
        bool cond21 = (other.gameObject.transform.parent == arParent.transform || other.gameObject.transform.parent == selectorReferent.transform);
        bool cond22 = (other.gameObject.GetComponent<TwinController>() != null);
        bool cond23 = (other.gameObject.GetComponent<EventIconController>() != null);
        bool cond24 = (other.gameObject.GetComponent<MovePositionController>() != null);
        bool cond25 = (other.gameObject.GetComponent<LastMovePosition>() != null);

        if (cond1 && (cond21 || cond22 || cond23 || cond24 || cond25))
        {
            dominantHand.ShowGrabHint();

            if (input.lastStateDown)
            {
                if(ApplicationModel.recordingStatus=="On" && ApplicationModel.recordedTarget== "ArObjects" && other.gameObject.GetComponent<ArObject>()!=null)
                {
                    arObjectsRecorder.addHide(other.gameObject);
                    other.gameObject.SetActive(false);
                }
                else
                {
                    CircleDispatcher dispatch = null;
                    if (other.gameObject.GetComponent<EventIconController>() != null)
                    {
                        EventIconController eventIconController = other.gameObject.GetComponent<EventIconController>();
                        if (other.gameObject.transform.parent != null)
                        {
                            dispatch = other.gameObject.transform.parent.gameObject.GetComponent<CircleDispatcher>();
                        }
                        if (eventIconController.getPrevious() == null && eventIconController.getNext() != null)
                        {
                            Destroy(eventIconController.getNext().GetComponent<EventIconController>().getBezier());
                        }
                        else if (eventIconController.getPrevious() != null && eventIconController.getNext() != null)
                        {
                            eventIconController.getPrevious().GetComponent<EventIconController>().setNextEventIcon(eventIconController.getNext(), true);
                        }

                    }

                    if (!cond24 && !cond25)
                    {
                        Destroy(other.gameObject);
                    }
                    else if(cond24)
                    {
                        other.gameObject.GetComponent<MovePositionController>().deletePos();
                    }
                    else if (cond25)
                    {
                        other.gameObject.GetComponent<LastMovePosition>().makeContinuous();
                    }
                    
                    
                    if (dispatch != null)
                    {
                        dispatch.haveToUpdate();
                    }
                }
                dominantHand.HideGrabHint();

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "ArObjects" || other.gameObject.tag == "TriggerObjects" || other.gameObject.tag == "AnimationObjects" || other.gameObject.tag == "Link")
        {
            dominantHand.HideGrabHint();
        }
    }

}

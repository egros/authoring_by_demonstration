﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationModel : MonoBehaviour
{
    static public string mode="simple";
    static public string recordingStatus = "Off";
    static public string recordedTarget = ""; // "ArObjects" / "BothHands" / "Puppet" / "LeftHand" / "RightHand"
    static public Vector3 handPosition;
    static public string form = "sphere";
    static public string dominantHand = "RightHand" ;
    static public string nonDominantHand = "LeftHand";
    static public bool wimOpened = false;
    static public int reccordingWait = 1;
    static public int idCpt = 3;
    static public int iconIdx = 0;
    static public string sceneToTest = "currentScene.json";
    static public string vrSceneName = "";
    static public bool backToVrScene = false;
    static public bool isIntro = true;
}

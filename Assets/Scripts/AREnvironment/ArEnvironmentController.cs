﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.Extras;

public class ArEnvironmentController : MonoBehaviour
{
    private Camera vrCamera;
    private Valve.VR.SteamVR_Action_Boolean input;
    private ExtractFromFile extractor;
    private bool needToSetLinks;
    private GameObject palette;
    private SceneHandler sceneHandler;

    // Start is called before the first frame update
    void Start()
    {
        ApplicationModel.mode = "Test";
        if (GameObject.Find("VRCamera") != null)
        {
            vrCamera = GameObject.Find("VRCamera").GetComponent<Camera>();
            vrCamera.cullingMask = ~(1 << LayerMask.NameToLayer("EventIcon"));
        }
        palette = GameObject.Find("PaletteCanvas");
        palette.SetActive(false);
        sceneHandler = GameObject.FindObjectOfType<SceneHandler>();
        sceneHandler.enableLaser(false);


        input = SteamVR_Input.GetBooleanAction("Record");
        extractor = ScriptableObject.CreateInstance<ExtractFromFile>();
        extractor.readFile(ApplicationModel.sceneToTest);
        needToSetLinks = true;
        ArObject[] arObjects = GameObject.FindObjectsOfType<ArObject>();
        foreach (ArObject ar in arObjects)
        {
            if (!ar.getIsVisibleAtStart())
            {
                EventIconController[] eventIcons = ar.gameObject.GetComponentsInChildren<EventIconController>();
                foreach (EventIconController e in eventIcons)
                {
                    e.gameObject.transform.SetParent(ar.gameObject.transform.parent);
                }
                ar.gameObject.SetActive(false);
            }
            ar.hideAttributes();
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (needToSetLinks)
        {
            needToSetLinks = false;
            extractor.setLinksOnUpdate();
        }
        if (input.lastStateDown)
        {
            palette.SetActive(true);
            sceneHandler.enableLaser(true);
            vrCamera.cullingMask = -1;
            ApplicationModel.backToVrScene = true;
            ApplicationModel.mode = "simple";
            SceneManager.LoadScene(ApplicationModel.vrSceneName);
        }
    }
}

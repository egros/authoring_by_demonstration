﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrEnvironmentController : MonoBehaviour
{
    private ExtractFromFile extractor;
    private bool needToSetLinks;
    public Vector3 playerPosition;

    // Start is called before the first frame update
    void Start()
    {
        
        ApplicationModel.isIntro = false;
        GameObject.FindObjectOfType<SceneHandler>().Start();
        InteractionControllerRecorder[] handRecorders = GetComponentsInChildren<InteractionControllerRecorder>();
        foreach (InteractionControllerRecorder rec in handRecorders)
        {
            rec.Start();
        }
        if (ApplicationModel.backToVrScene)
        {
            ApplicationModel.backToVrScene = false;
            extractor = ScriptableObject.CreateInstance<ExtractFromFile>();
            extractor.readFile(ApplicationModel.sceneToTest);
            needToSetLinks = true;
        }
        else
        {
            GameObject.Find("Player").transform.position = playerPosition;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (needToSetLinks)
        {
            needToSetLinks = false;
            extractor.setLinksOnUpdate();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderMaker : MonoBehaviour
{
    private bool needToMakeCollider;

    public void planCollider() 
    {
        needToMakeCollider = true;
    }

    public void Update()
    {
        if (needToMakeCollider)
        {
            makeCollider();
            needToMakeCollider = false;
        }
    }
    public void makeCollider()
    {
        string name = "Events\\TestMeshLimits";
        UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
        GeometryAnalyzer geoAnalyzer = new GeometryAnalyzer();
        /*minNmax extremities = geoAnalyzer.getLocalMinNmax(gameObject);
        Vector3 center = geoAnalyzer.getLocalCenterPoint(gameObject);
        BoxCollider collider = gameObject.AddComponent<BoxCollider>();
        float deltaX = Mathf.Abs(extremities.max.x - extremities.min.x);
        float deltaY = Mathf.Abs(extremities.max.y - extremities.min.y);
        float deltaZ = Mathf.Abs(extremities.max.z - extremities.min.z);
        collider.size = new Vector3(deltaX, deltaY, deltaZ);
        collider.center = center;
        collider.isTrigger = true;*/

        // Test mesh
        minNmax objBounds = new minNmax(gameObject.transform.position, gameObject.transform.position);
        Renderer rend = gameObject.GetComponent<Renderer>();
        if (rend != null)
        {
            objBounds = new minNmax(rend.bounds.min, rend.bounds.max);
        }
        else
        {
            foreach(Renderer r in gameObject.GetComponentsInChildren<Renderer>())
            {
                minNmax rBounds = new minNmax(r.bounds.min, r.bounds.max);
                objBounds = geoAnalyzer.compareMinNmax(objBounds, rBounds);
            }
        }
        Vector3 c = new Vector3((objBounds.max.x + objBounds.min.x) / 2, (objBounds.max.y + objBounds.min.y) / 2, (objBounds.max.z + objBounds.min.z) / 2);

        BoxCollider collider = gameObject.AddComponent<BoxCollider>();
        Vector3 minP = gameObject.transform.InverseTransformPoint(objBounds.min);
        Vector3 maxP = gameObject.transform.InverseTransformPoint(objBounds.max);
        float deltaX = Mathf.Abs(maxP.x - minP.x);
        float deltaY = Mathf.Abs(maxP.y - minP.y);
        float deltaZ = Mathf.Abs(maxP.z - minP.z);
        collider.size = new Vector3(deltaX, deltaY, deltaZ);
        collider.center = gameObject.transform.InverseTransformPoint(c);
        collider.isTrigger = true;


        /*GameObject maxPoint = Instantiate((GameObject)assetToGet, objBounds.max, Quaternion.identity);
        GameObject minPoint = Instantiate((GameObject)assetToGet, objBounds.min, Quaternion.identity);
        GameObject centerPoint = Instantiate((GameObject)assetToGet, c, Quaternion.identity);
        maxPoint.transform.SetParent(gameObject.transform);
        minPoint.transform.SetParent(gameObject.transform);
        centerPoint.transform.SetParent(gameObject.transform);*/
    }

}

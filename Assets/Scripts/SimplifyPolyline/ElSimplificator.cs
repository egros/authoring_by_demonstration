﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElSimplificator : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    LineRenderer line;
    [SerializeField]
    //TrajReader tr;
    public List<Vector3> listTraj;
    [SerializeField]
    bool HighQualityEnabled;
    [SerializeField]
    float tolerence;
    [SerializeField]
    Material mat;
    [SerializeField]
    bool doneOnce;
    void Start()
    {
        //Simplify();
        doneOnce = false;
        HighQualityEnabled = true;
        tolerence = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if (!doneOnce)
            {
                Debug.Log("Let's simplify");
                Simplify();
                doneOnce = true;
            }
        }
    }

    public IList<Vector3> Simplify()
    {
        //List<Vector3> listTraj = tr.trajPoints;
        Vector3[] pos = listTraj.ToArray();
        //line.GetPositions(pos);
        //line.positionCount = pos.Length;
        //line.SetPositions(pos);
        IList<Vector3> simplifiedPoints = SimplificationHelpers.Simplify<Vector3>(
                    pos,
                    (p1, p2) => p1 == p2,
                    (p) => p.x,
                    (p) => p.y,
                    (p) => p.z,
                    tolerence,
                    HighQualityEnabled
                    );

        /*foreach (Vector3 p in simplifiedPoints)
        {
            Debug.Log(p);
        }

        GameObject newLine = new GameObject("NewLine");
        LineRenderer l = newLine.AddComponent<LineRenderer>();
        l.startWidth = 0.05f;
        l.endWidth = 0.05f;
        Vector3[] posF = new Vector3[simplifiedPoints.Count];
        l.positionCount = posF.Length;
        for (int i=0; i<simplifiedPoints.Count; i++)
        {
            posF[i] = simplifiedPoints[i];
        }
        l.SetPositions(posF);
        l.material = mat;*/
        return simplifiedPoints;
    }
}

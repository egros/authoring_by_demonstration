﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using UnityEngine;

public class TrajReader : MonoBehaviour
{

    [SerializeField]
    TextAsset traj;
    public List<Vector3> trajPoints = new List<Vector3>();
    // Start is called before the first frame update
    void Start()
    {
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

        string[] lines = traj.text.Split('\n');
        foreach(string l in lines)
        {
            Debug.Log(l);
            string[] lineData = (l.Trim()).Split(',');
            float x, y, z;
            if (lineData.Length == 3)
            {
                float.TryParse(lineData[0].Trim(), out x);
                float.TryParse(lineData[1].Trim(), out y);
                float.TryParse(lineData[2].Trim(), out z);
                trajPoints.Add(new Vector3(x, y, z));
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

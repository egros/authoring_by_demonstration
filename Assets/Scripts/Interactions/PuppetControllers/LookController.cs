﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LookController : MonoBehaviour
{
    private InteractionPuppetRecorder recorder;
    private GameObject previousLook;
    private InteractionAnalyzer iconMaker;

    // Start is called before the first frame update
    void Start()
    {
        recorder = GetComponentInParent<InteractionPuppetRecorder>();
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
        if (recorder == null)
        {
            Debug.LogError("Couldn't find InteractionPuppetRecorder");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (iconMaker == null)
        {
            iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
        }
        if (recorder.getIsRecording())
        {
            if (other.gameObject.tag == "ArObjects" && other.gameObject.GetComponent<MovePositionController>()==null && previousLook != other.gameObject)
            {
                previousLook = other.gameObject;
                RecordedLook pickAt = ScriptableObject.CreateInstance<RecordedLook>(); //new RecordedLook();
                pickAt.setLook(previousLook);
                iconMaker.createEventIcon(pickAt);
                //recorder.isLookingAt(other.gameObject);
            }

            //recorder.isLookingAt(collision.collider.gameObject);
        }
    }

}

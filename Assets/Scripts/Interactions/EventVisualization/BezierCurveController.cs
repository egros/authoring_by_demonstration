﻿using BezierSolution;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurveController : MonoBehaviour
{
    private BezierSpline spline;
    private BezierPoint[] points;

    private GameObject anchor1;
    private GameObject anchor2;
    private SceneHandler laserAnchor;


    // Start is called before the first frame update
    void Start()
    {
        spline = GetComponentInChildren<BezierSpline>();
        points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
        
    }

    public void setPoints(GameObject point1, GameObject point2)
    {
        if (spline == null)
        {
            spline = GetComponentInChildren<BezierSpline>();
        }
        if (points == null)
        {
            points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
        }
        anchor1 = point1;
        anchor2 = point2;
        if (points.Length >= 2)
        {
            points[0].gameObject.transform.position = anchor1.transform.position;
            points[1].gameObject.transform.position = anchor2.transform.position;
        }
    }

    public void setSceneHandler(SceneHandler sceneHandler)
    {
        laserAnchor = sceneHandler;
        if (spline == null)
        {
            spline = GetComponentInChildren<BezierSpline>();
        }
        if (points == null)
        {
            points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
        }
        if (points.Length >= 2)
        {
            points[0].gameObject.transform.position = laserAnchor.getLaserHitPoint();
            points[1].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
    }

    public void setAnchor(int anchorNum, GameObject anchor)
    {
        if (anchorNum == 1)
        {
            anchor1 = anchor;
            points[0].gameObject.transform.position = anchor1.transform.position;
            points[1].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
        else if (anchorNum == 2)
        {
            anchor2 = anchor;
            points[0].gameObject.transform.position = anchor1.transform.position;
            points[1].gameObject.transform.position = anchor2.transform.position;
        }
    }

    private void Update()
    {
        if(anchor1!=null)
        {
            if (anchor1.transform.position != points[0].gameObject.transform.position)
            {
                points[0].gameObject.transform.position = anchor1.transform.position;
            }
        }
        else if(laserAnchor!=null)
        {
            points[0].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
        if (anchor2 != null)
        {
            if (anchor2.transform.position != points[1].gameObject.transform.position)
            {
                points[1].gameObject.transform.position = anchor2.transform.position;
            }
        }
        else if(laserAnchor!=null)
        {
            points[1].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
        
    }
}

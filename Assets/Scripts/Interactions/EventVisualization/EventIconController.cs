﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventIconController : MonoBehaviour
{
    public GameObject bezierCurveModel;
    public GameObject outlineSphere;

    private GameObject bezierCurve;
    private float rate = 0.3f;
    private RecordedEvent recordedEvent;
    private GameObject previousEventIcon;
    private GameObject nextEventIcon;

    private List<EventIconController> animations;
    private bool hasBeenTriggered;
    private DateTime timeTrigger;

    // Outline 
    private Outline outline;
    private Color outlineColor;
    private bool outlineEnabled;
    private bool outlineTemporaryModif;
    
    private int id;

    private List<JackBezierController> connectedJacks;
    private JackBezierController unconnectedJack;
    private GameObject vrCamera;
    private CircleCalculator circleCalcul;

    public void Start()
    {
        try
        {
            if (outlineSphere != null)
            {
                if (outlineSphere.GetComponent<Outline>() == null)
                {
                    outline = outlineSphere.AddComponent<Outline>();
                }
                else
                {
                    outline = outlineSphere.GetComponent<Outline>();
                }
                
            }
            else
            {
                outline = gameObject.AddComponent<Outline>();
            }
            
            outline.OutlineMode = Outline.Mode.OutlineAll;
            outline.OutlineColor = Color.magenta;
            outline.OutlineWidth = 5f;
            outline.enabled = false;
            outlineColor = outline.OutlineColor;
            outlineEnabled = outline.enabled;
            outlineTemporaryModif = false;
        }
        catch
        {
            outline = null;
        }

        hasBeenTriggered = false;
        if (animations == null)
        {
            animations = new List<EventIconController>();
        }

        if (connectedJacks == null)
        {
            connectedJacks = new List<JackBezierController>();
        }
        circleCalcul = ScriptableObject.CreateInstance<CircleCalculator>();
        instantiateJack();
        vrCamera = GameObject.Find("VRCamera");
    }

    public void instantiateJack()
    {
        // Instantiate unconnected Jack
        if (vrCamera == null)
        {
            vrCamera = GameObject.Find("VRCamera");
        }
        string ressourceName = "Events\\JackConnector";

        Vector3 v = getRelativeRightPoint();

        UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(ressourceName);
        GameObject connector = Instantiate((GameObject)assetToGet, v, Quaternion.identity);
        connector.transform.LookAt(transform);
        connector.transform.SetParent(transform);
        unconnectedJack = connector.GetComponent<JackBezierController>();
        unconnectedJack.setTrigger(this);
        unconnectedJack.gameObject.SetActive(false);
    }

    public void linkToAnimation(EventIconController animation)
    {
        string ressourceName = "Events\\JackConnector";
        Vector3 v = getRelativeRightPoint();
        UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(ressourceName);
        GameObject connector = Instantiate((GameObject)assetToGet, v, Quaternion.identity);
        JackBezierController jackController = connector.GetComponent<JackBezierController>();
        jackController.setTrigger(this);
        jackController.restoreLink(animation);
        if (connectedJacks == null)
        {
            connectedJacks = new List<JackBezierController>();
        }
        connectedJacks.Add(jackController);
    }

    public void displayJack()
    {
        if (unconnectedJack == null)
        {
            instantiateJack();
        }
        unconnectedJack.gameObject.SetActive(true);
    }

    public void noLongerNeedToDisplayJack(int seconds)
    {
        StartCoroutine(DisplayJackCoroutine(seconds));
    }

    IEnumerator DisplayJackCoroutine(int seconds)
    {
        yield return new WaitForSeconds(seconds);

        if (unconnectedJack.connectorIsAttached())
        {
            unconnectedJack.gameObject.SetActive(false);
        }

    }

    public Vector3 getRelativeRightPoint()
    {
        if (vrCamera == null)
        {
            vrCamera = GameObject.Find("VRCamera");
        }
        if (circleCalcul == null)
        {
            circleCalcul = ScriptableObject.CreateInstance<CircleCalculator>();
        }
        Vector3 v = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.04f);

        Vector3 relativPos = vrCamera.transform.InverseTransformPoint(transform.position);
        if (relativPos.x != 0 || relativPos.z != 0)
        {
            Vector2 v1 = new Vector2(relativPos.x, relativPos.z);
            float r = Vector2.Distance(Vector2.zero, v1);
            float R = 0.04f;
            Vector3 connectorRelPos = circleCalcul.getIntersection(relativPos, r, R);
            v = vrCamera.transform.TransformPoint(connectorRelPos);
        }
        return v;
    }

    public void replaceJackOnSpot()
    {
        GameObject connector = unconnectedJack.GetComponentInChildren<JackConnectorController>().gameObject;
        connector.transform.SetParent(unconnectedJack.gameObject.transform);

        Vector3 v1 = transform.InverseTransformPoint(connector.transform.position);
        Vector3 v2 = circleCalcul.getlineNCircleIntersect(v1, 0.04f);
        connector.transform.position = transform.TransformPoint(v2);  //getRelativeRightPoint();
        unconnectedJack.transform.rotation = Quaternion.identity;
        connector.transform.rotation = new Quaternion(0, 0, 0, 0);
        connector.transform.LookAt(transform);
        
    }

    public void connectedLastJack()
    {
        connectedJacks.Add(unconnectedJack);
        instantiateJack();
    }

    public void Update()
    {
        if (outline != null && !outlineTemporaryModif)
        {
            if (previousEventIcon == null)
            {
                outline.enabled = true;
                outline.OutlineColor = Color.magenta;
            }
            else if (nextEventIcon == null)
            {
                outline.enabled = true;
                outline.OutlineColor = Color.cyan;
            }
            else
            {
                outline.enabled = false;
            }
            outlineColor = outline.OutlineColor;
            outlineEnabled = outline.enabled;
        }
        if (ApplicationModel.mode != "Test")
        {
            hasBeenTriggered = false;
        }
        else
        {
            if (hasBeenTriggered && nextEventIcon!=null)
            {
                System.TimeSpan diff = DateTime.Now.Subtract(timeTrigger);
                float dist = Vector3.Distance(transform.position, nextEventIcon.transform.position);
                if (diff.TotalSeconds > Mathf.Max(dist*20,10))
                {
                    hasBeenTriggered = false;
                }
            }
        }
        
    }

    public void setRecordedEvent(RecordedEvent e, GameObject previous, int identifiant)
    {
        recordedEvent = e;
        recordedEvent.setEventIcon(gameObject);
        previousEventIcon = previous;
        if (previous != null)
        {
            bezierCurve = Instantiate(bezierCurveModel, transform.position, Quaternion.identity);
            bezierCurve.transform.SetParent(transform);
            bezierCurve.GetComponent<BezierCurveController>().setPoints(previous, gameObject);
            //bezierCurve.GetComponent<SplineTubeController>().setPoints(previous, gameObject);
        }
        if (!recordedEvent.getDetectorIsSet())
        {
            recordedEvent.setDetector();
        }
        id = identifiant;

    }

    public int getID()
    {
        return id;
    }

    public void setID(int i)
    {
        id = i;
    }

    public void launchSimulation()
    {
        if (recordedEvent != null && !recordedEvent.getSimulationOn())
        {
            recordedEvent.launchSimulation();
            InvokeRepeating("SlowUpdate", 0.0f, rate);
        }
    }

    public void SlowUpdate()
    {
        if (recordedEvent.getSimulationOn())
        {
            recordedEvent.animate();
        }
        else
        {
            CancelInvoke();
        }
    }


    public void setNextEventIcon(GameObject nextE, bool rearrange)
    {
        nextEventIcon = nextE;
        if (rearrange)
        {
            nextEventIcon.GetComponent<EventIconController>().rearrangeBezier(gameObject);
        }
    }

    public void rearrangeBezier(GameObject newPrevious)
    {
        previousEventIcon = newPrevious;
        if (bezierCurve == null)
        {
            bezierCurve = Instantiate(bezierCurveModel, transform.position, Quaternion.identity);
            bezierCurve.transform.SetParent(transform);
        }
        BezierCurveController bezController = bezierCurve.GetComponent<BezierCurveController>();
        //SplineTubeController bezController = bezierCurve.GetComponent<SplineTubeController>();
        bezController.setPoints(previousEventIcon,gameObject);
    }

    public GameObject getPrevious()
    {
        return previousEventIcon;
    }

    public GameObject getNext()
    {
        return nextEventIcon;
    }

    public GameObject getBezier()
    {
        return bezierCurve;
    }
    
    public bool isAnimation()
    {
        if (recordedEvent != null)
        {
            return recordedEvent.getIsAnimation();
        }
        else
        {
            return false;
        }
        
    }

    public RecordedEvent getRecordedEvent()
    {
        return recordedEvent;
    }

    public void addAnimation(EventIconController anim)
    {
        if (anim.isAnimation())
        {
            if (animations == null)
            {
                animations = new List<EventIconController>();
            }
            animations.Add(anim);
        }
    }

    public void triggerAnimation()
    {
        if (isAnimation())
        {
            if (recordedEvent != null )//&& !hasBeenTriggered)
            {
                bool nextStep = false;
                if (previousEventIcon == null)
                {
                    nextStep = true;
                }
                else if (previousEventIcon.GetComponent<EventIconController>().getHasBeenTrigered())
                {
                    nextStep = true;
                }
                if (nextStep)
                {
                    hasBeenTriggered = true;
                    recordedEvent.launchAnimation();
                }
            }
            
        }
        // If this is also a trigger => launch animations?
    }

    public void trigerDone()
    {
        bool nextStep= false;
        if (previousEventIcon == null)
        {
            nextStep = true;
        }
        else if (previousEventIcon.GetComponent<EventIconController>().getHasBeenTrigered())
        {
            nextStep = true;
        }
        if (nextStep)
        {
            hasBeenTriggered = true;
            timeTrigger = DateTime.Now;
            launchAllAnimations();
        }
        
    }

    public bool getHasBeenTrigered()
    {
        return hasBeenTriggered;
    }

    public void launchAllAnimations()
    {
        /*if (animations != null)
        {
            foreach (EventIconController e in animations)
            {
                if (e != null)
                {
                    e.triggerAnimation();
                }
            }
        }*/
        foreach (JackBezierController jack in connectedJacks)
        {
            // Launch trigger to link
            if (jack != null)
            {
                jack.triggerAnimationIcon();
            }
        }
    }

    private void OnDestroy()
    {
        if (recordedEvent != null)
        {
            recordedEvent.stopSimulation();
            if (recordedEvent.getActionName() == "Move")
            {
                RecordedMovement recordMov = (RecordedMovement)recordedEvent;
                recordMov.deleteInspector();
            }
            Destroy(recordedEvent);
        }
        
    }

    public void inspect()
    {
        recordedEvent.inspect();
    }

    public void resetAnimation()
    {
        if (recordedEvent != null)
        {
            recordedEvent.CancelAnimationExecuted();
        }
    }

    public int[] getAnimationsID()
    {
        List<int> anim = new List<int>();
        foreach(JackBezierController jack in connectedJacks)
        {
            if (jack != null)
            {
                anim.Add(jack.getAnimationID());
            }
            
        }
        return anim.ToArray();
    }

    public int getPreviousID()
    {
        if (previousEventIcon != null)
        {
            return previousEventIcon.GetComponent<EventIconController>().getID();
        }
        return -1;
    }

    public int getNextID()
    {
        if (nextEventIcon != null)
        {
            return nextEventIcon.GetComponent<EventIconController>().getID();
        }
        return -1;
    }

    public void activateOutline(Color c)
    {
        outline.enabled = true;
        outline.OutlineColor = c;
        outlineTemporaryModif = true;
    }

    public void recoverDefaultOutline()
    {
        outline.OutlineColor = outlineColor;
        outline.enabled = outlineEnabled;
        outlineTemporaryModif = false;
    }

    public int getObjectOfInterestID()
    {
        return recordedEvent.getObjectOfInterestID();
    }

    public EventDescription getDescription()
    {
        return recordedEvent.getDescription();
        /*TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        int[] anim = new int[animations.Count];
        for (int i = 0; i < animations.Count; i++)
        {
            anim[i] = animations[i].getID();
        }
        if (recordedEvent != null)
        {
            string mainType = recordedEvent.getActionName();
            GameObject target = recordedEvent.getObjectOfInterest();
            int targetID;
            if (target == null)
            {
                targetID = -1;
            }
            else
            {
                if (target.name == "RightHand" || target.name == "SphereRightHand")
                {
                    targetID = 0;
                }
                else if (target.name == "LeftHand" || target.name == "SphereLeftHand")
                {
                    targetID = 1;
                }
                else if (target.name == "WoodBody")
                {
                    targetID = 2;
                }
                else
                {
                    if (target.GetComponent<ArObject>() != null)
                    {
                        targetID = target.GetComponent<ArObject>().getID();
                    }
                    else
                    {
                        targetID = -1;
                    }
                }
            }
            
            
            int prev = -1;
            if (previousEventIcon != null)
            {
                prev = previousEventIcon.GetComponent<EventIconController>().getID();
            }
            int nextI = -1;
            if (nextEventIcon != null)
            {
                nextI = nextEventIcon.GetComponent<EventIconController>().getID();
            }

            EventDescription descr;
            float[] defaultColor;
            // Specific to each event
            if (mainType == "Show" || mainType == "Hide" || mainType == "Look")
            {
                descr = new EventDescription(mainType, id, targetID, anim, prev, nextI);
            }
            else if (mainType == "Color")
            {
                RecordedColor colorEvent= (RecordedColor)recordedEvent;
                Color c = colorEvent.getColor();
                float[] colorTab = new float[] { c.r, c.g, c.b, c.a };
                descr = new EventDescription(mainType, id, targetID, anim, prev, nextI, colorTab);

            }
            else if (mainType == "Move")
            {
                RecordedMovement move = (RecordedMovement)recordedEvent;
                string traject = translater.turnTabToString(translater.turnVectorListToTab(move.getTrajectory()));
                string rotations = translater.turnTabToString(translater.turnQuaternionListToTab(move.getRotations()));
                bool folowDir = move.getFollowDirection();
                descr = new EventDescription(mainType, id, targetID, anim, prev, nextI, traj: traject, rot: rotations, follow:folowDir);
            }
            else if (mainType == "Drag")
            {
                RecordedDrag drag = (RecordedDrag)recordedEvent;
                string traject = translater.turnTabToString(translater.turnVectorListToTab(drag.getTrajectory()));
                string rota = translater.turnTabToString(translater.turnQuaternionListToTab(drag.getRotations()));
                int hand=-1;
                if (drag.getHand() == "RightHand")
                {
                    hand = 0;
                }
                else
                {
                    hand = 1;
                }
                bool followDir = drag.getFollowDirection();
                descr = new EventDescription(mainType, id, targetID, anim, prev, nextI, holdingH: hand, traj: traject, rot: rota, follow:followDir);
            }
            else if (mainType == "Collision")
            {
                RecordedCollision collision = (RecordedCollision)recordedEvent;
                GameObject collided = collision.getColllidedObject();
                GameObject colliding = collision.getCollindingObject();
                int collidedTarget = -1;
                int collidingTarget = -1;
                if(collided.name == "RightHand" || collided.name== "SphereRightHand")
                {
                    collidedTarget = 0;
                }
                else if(collided.name == "LeftHand" || collided.name == "SphereLeftHand")
                {
                    collidedTarget = 1;
                }
                else if(collided.name == "WoodBody")
                {
                    collidedTarget = 2;
                }
                else
                {
                    collidedTarget = collided.GetComponent<ArObject>().getID();
                }

                if (colliding.name == "RightHand" || colliding.name == "SphereRightHand")
                {
                    collidingTarget = 0;
                }
                else if (colliding.name == "LeftHand" || colliding.name == "SphereLeftHand")
                {
                    collidingTarget = 1;
                }
                else if (colliding.name == "WoodBody")
                {
                    collidingTarget = 2;
                }
                else
                {
                    collidingTarget = colliding.GetComponent<ArObject>().getID();
                }
                descr = new EventDescription(mainType, id, collidingTarget, anim, prev, nextI, collidedObj: collidedTarget);

            }
            else if (mainType == "Grab")
            {
                RecordedGrab grab = (RecordedGrab)recordedEvent;
                string holdingHand = grab.getGrabbingHand().name;
                int hand = -1;
                if (holdingHand == "RightHand")
                {
                    hand = 0;
                }
                else
                {
                    hand = 1;
                }
                if (targetID == -1)
                {
                    Vector3 v = recordedEvent.getAttachPosition();
                    float[] posTab = new float[3] { v.x, v.y, v.z };
                    float[][] pos = new float[1][] { posTab };
                    string posString = translater.turnTabToString(pos);
                    descr = new EventDescription(mainType, id, targetID, anim, prev, nextI, holdingH: hand, traj: posString);
                }
                else
                {
                    descr = new EventDescription(mainType, id, targetID, anim, prev, nextI, holdingH: hand);
                }
                
            }
            else if (mainType == "Drop")
            {
                RecordedDrop drop = (RecordedDrop)recordedEvent;
                string holdingHand = drop.getHand().name;
                int hand = -1;
                if (holdingHand == "RightHand")
                {
                    hand = 0;
                }
                else
                {
                    hand = 1;
                }
                if (targetID == -1)
                {
                    Vector3 v = recordedEvent.getAttachPosition();
                    float[] posTab = new float[3] { v.x, v.y, v.z };
                    float[][] pos = new float[1][] { posTab };
                    string posString = translater.turnTabToString(pos);
                    descr = new EventDescription(mainType, id, targetID, anim, prev, nextI, holdingH: hand, traj: posString);
                }
                else
                {
                    descr = new EventDescription(mainType, id, targetID, anim, prev, nextI, holdingH: hand);
                }
            }
            else
            {
                descr = new EventDescription(mainType, id, targetID, anim, prev, nextI);
            }
            return descr;
        }
        else
        {
            //metaphor
            ZoneColliderController zoneCollider = GetComponentInParent<ZoneColliderController>();
            if (zoneCollider != null)
            {
                List<Vector3> points = zoneCollider.getPoints();
                string pointString = translater.turnTabToString(translater.turnVectorListToTab(points));
                EventDescription descr = new EventDescription("ZoneCollider", id, -1, anim, traj: pointString);
                return descr;
            }
            return new EventDescription();
        }*/
    }

    /*public string turnTabToString(float[][] tab)
    {
        string result = "[";
        for(int i = 0; i<tab.Length; i++)
        {
            float[] t = tab[i];
            string subTab = "[";
            for(int j=0; j<t.Length-1; j++)
            {
                string f = t[j].ToString();
                f = f.Replace(',', '.');
                subTab = subTab + f + ",";
            }
            string fl = t[t.Length - 1].ToString();
            fl = fl.Replace(',', '.');
            subTab = subTab + fl + "]";
            result = result + subTab;
            if (i < (tab.Length - 1))
            {
                result = result + ", ";
            }
        }
        result = result + "]";
        return result;
    }

    public float[][] turnVectorListToTab(List<Vector3> vecList)
    {
        float[][] tab = new float[vecList.Count][];
        for(int i =0; i<vecList.Count; i++)
        {
            tab[i] = new float[] { vecList[i].x, vecList[i].y, vecList[i].z };
        }
        return tab;
    }

    public float[][] turnQuaternionListToTab(List<Quaternion> quatList)
    {
        float[][] tab = new float[quatList.Count][];
        for (int i = 0; i < quatList.Count; i++)
        {
            tab[i] = new float[] { quatList[i].x, quatList[i].y, quatList[i].z, quatList[i].w };
        }
        return tab;
    }*/

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRenderer : MonoBehaviour
{
    public LineRenderer line;
    public GameObject endPoint;
    public GameObject movePosition;
    public GameObject arrow;

    private GameObject instantiatedArrow;
    private GameObject marbleParent;
    private RecordedTrajectory record;
    private List<Vector3> lineTrajectory;
    private List<MovePositionController> marbles;
    private bool isRendered;
    private bool followDirection;
    private bool playerIsTarget;

    public void Start()
    {
        followDirection = false;
        marbleParent = GameObject.Find("MarblesParent");
        RecordedEvent recordedEvent = GetComponent<EventIconController>().getRecordedEvent();
        if (recordedEvent.getActionName() == "Move" || recordedEvent.getActionName()=="Drag")
        {
            record = (RecordedTrajectory)recordedEvent;
        }

        string objName = record.getObjectOfInterest().name;
        playerIsTarget = (objName == "RightHand") || (objName == "LeftHand") || (objName == "SphereRightHand") || (objName == "SphereLeftHand") || (objName == "WoodBody");
        IList<Vector3> trajectory = record.getTrajectory();
        endPoint.transform.position = trajectory[trajectory.Count - 1];
        lineTrajectory = new List<Vector3>();
        if (playerIsTarget)
        {
            lineTrajectory.Add(record.getEventIcon().transform.position);
        }
        else
        {
            lineTrajectory.Add(trajectory[0]);
        }
        marbles = new List<MovePositionController>();
        for(int i =1; i<trajectory.Count-1; i++)
        {
            lineTrajectory.Add(trajectory[i]);
            GameObject newMarble = Instantiate(movePosition);
            endPoint.transform.SetParent(marbleParent.transform);
            newMarble.transform.SetParent(marbleParent.transform);
            newMarble.transform.position = trajectory[i];
            MovePositionController movePos = newMarble.GetComponent<MovePositionController>();
            movePos.setMovePosition(this, i);
            marbles.Add(movePos);
        }
        lineTrajectory.Add(trajectory[trajectory.Count - 1]);
        line.positionCount = lineTrajectory.Count;
        line.SetPositions(lineTrajectory.ToArray());
        //line.widthMultiplier = 0.05f;
        line.SetPosition(0, record.getObjectOfInterest().transform.position);
        if (record.getFollowDirection())
        {
            turnToContinuousMovement();
        }
        hideDetails();
        
    }

    private void Update()
    {
        if (!playerIsTarget) 
        {
            if (lineTrajectory[0] != record.getObjectOfInterest().transform.position)
            {
                lineTrajectory[0] = record.getObjectOfInterest().transform.position;
                if (!followDirection)
                {
                    line.SetPositions(lineTrajectory.ToArray());
                    record.modifyTrajectory(0, record.getObjectOfInterest().transform.position);
                }
                else
                {
                    Vector3 pos = record.getObjectOfInterest().transform.position;
                    Vector3 direction = record.getDirection();
                    Vector3 v = new Vector3(pos.x + direction.x, pos.y + direction.y, pos.z + direction.z);
                    endPoint.transform.position = v;
                }
                
                
                //
            }
        }
        
    }

    public void setPosition(int idx, Vector3 newPosition)
    {
        lineTrajectory[idx] = newPosition;
        line.SetPositions(lineTrajectory.ToArray());
        record.modifyTrajectory(idx, newPosition);
    }

    public void setLastPosition(Vector3 position)
    {
        lineTrajectory[lineTrajectory.Count - 1] = position;
        if (line != null)
        {
            line.SetPositions(lineTrajectory.ToArray());
        }
        record.modifyTrajectory(lineTrajectory.Count - 1, position);
        if (followDirection)
        {
            instantiatedArrow.transform.LookAt(endPoint.transform);
        }
    }

    public void deletePosition(int idx)
    {
        marbles.RemoveAt(idx-1);
        List<Vector3> newTrajectory = new List<Vector3>();
        newTrajectory.Add(lineTrajectory[0]);
        int i = 1;
        foreach(MovePositionController m in marbles)
        {
            m.setIndex(i);
            newTrajectory.Add(m.gameObject.transform.position);
            i++;
        }
        newTrajectory.Add(lineTrajectory[lineTrajectory.Count - 1]);
        lineTrajectory = newTrajectory;
        line.positionCount = lineTrajectory.Count;
        line.SetPositions(lineTrajectory.ToArray());
        record.deletePosition(lineTrajectory);
    }

    public void hideShowDetails()
    {
        if (isRendered)
        {
            hideDetails();
        }
        else
        {
            showDetails();
        }
    }

    public void hideDetails()
    {
        if (!followDirection)
        {
            line.gameObject.SetActive(false);
            endPoint.SetActive(false);
            foreach (MovePositionController m in marbles)
            {
                m.gameObject.SetActive(false);
            }
        }
        else
        {
            endPoint.SetActive(false);
            instantiatedArrow.SetActive(false);
        }
        isRendered = false;
    }

    public void showDetails()
    {
        if (!followDirection)
        {
            line.gameObject.SetActive(true);
            endPoint.SetActive(true);
            foreach (MovePositionController m in marbles)
            {
                m.gameObject.SetActive(true);
            }
        }
        else
        {
            endPoint.SetActive(true);
            instantiatedArrow.SetActive(true);
        }
        
        isRendered = true;
    }

    public void turnToContinuousMovement()
    {
        if (!followDirection)
        {
            foreach (MovePositionController marble in marbles)
            {
                if (marble != null)
                {
                    Destroy(marble.gameObject);
                }
            }
            if (line != null)
            {
                Destroy(line.gameObject);
            }
            //endPoint.transform.SetParent(record.getObjectOfInterest().transform);
            followDirection = true;
            instantiatedArrow = Instantiate(arrow);
            instantiatedArrow.transform.SetParent(record.getObjectOfInterest().transform);
            instantiatedArrow.transform.localPosition = new Vector3(0f, 0f, 0f);
            instantiatedArrow.transform.LookAt(endPoint.transform);
            record.switchToDirection();
        }
        
    }

    public void destroyElements()
    {
        if (endPoint != null)
        {
            Destroy(endPoint);
        }
        if (instantiatedArrow != null)
        {
            Destroy(instantiatedArrow);
        }
        foreach(MovePositionController m in marbles)
        {
            if (m != null)
            {
                Destroy(m.gameObject);
            }
            
        }
    }

    public void validateMarble(int m)
    {
        if(m>=0 && m < marbles.Count)
        {
            marbles[m].passBy(true);
        }
        
    }

    public void resetValidated()
    {
        foreach(MovePositionController m in marbles)
        {
            if (m != null)
            {
                m.passBy(false);
            }
        }
    }

}

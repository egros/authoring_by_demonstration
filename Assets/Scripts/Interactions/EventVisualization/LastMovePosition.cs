﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastMovePosition : MonoBehaviour
{
    private Vector3 oldPosition;
    public MoveRenderer moveRender;

    // Start is called before the first frame update
    void Start()
    {
        oldPosition = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (oldPosition != gameObject.transform.position)
        {
            moveRender.setLastPosition(gameObject.transform.position);
            oldPosition = gameObject.transform.position;
        }
    }

    public void makeContinuous()
    {
        moveRender.turnToContinuousMovement();
    }
}

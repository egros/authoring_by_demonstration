﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePositionController : MonoBehaviour
{
    private int idx;
    private Vector3 oldPosition;
    private MoveRenderer moveRenderer;

    public Material objMat;
    public Material testMat;
    public MeshRenderer mesh;

    public void passBy(bool passed)
    {
        if(mesh == null)
        {
            mesh = gameObject.GetComponent<MeshRenderer>();
        }
        if (passed)
        {
            mesh.material = testMat;
        }
        else
        {
            mesh.material = objMat;
        }
    }

    public void setMovePosition(MoveRenderer rend, int index)
    {
        moveRenderer = rend;
        idx = index;
        oldPosition = gameObject.transform.position;
    }

    public void setIndex(int i)
    {
        idx = i;
    }

    // Update is called once per frame
    void Update()
    {
        if(oldPosition != gameObject.transform.position)
        {
            //Change trajectory
            moveRenderer.setPosition(idx, gameObject.transform.position);
            oldPosition = gameObject.transform.position;
        }
    }

    /*private void OnDestroy()
    {
        if (moveRenderer != null)
        {
            //moveRenderer.DeletePosition(idx);
            moveRenderer.UpdateAfterDelete();
        }
    }*/

    public void deletePos()
    {
        moveRenderer.deletePosition(idx);
        Destroy(gameObject);
    }
}

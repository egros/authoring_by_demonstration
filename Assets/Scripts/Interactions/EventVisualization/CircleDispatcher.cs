﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleDispatcher : MonoBehaviour
{
    private EventIconController[] eventIcons;
    private bool needToUpdate;
    private float radius;
    private GeometryAnalyzer geoAnalyzer;
    private float height;
    private float yMax;
    private float xMin;
    private GameObject vrCamera;

    // Start is called before the first frame update
    void Start()
    {
        vrCamera = GameObject.Find("VRCamera");
        eventIcons = GetComponentsInChildren<EventIconController>();
        //needToUpdate = false;
        
    }

    public void haveToUpdate()
    {
        needToUpdate = true;
    }

    public void Update()
    {
        if (needToUpdate)
        {
            needToUpdate = false;
            updateDispatch();
            
        }

    }

    public void updateDispatch()
    {
        /*if(rX==0f && rY==0f)
        {
            geoAnalyzer = new GeometryAnalyzer();
            minNmax extr = geoAnalyzer.getMinNmax(gameObject);
            rX = Mathf.Abs(extr.max.x - extr.min.x);
            rY = Mathf.Abs(extr.max.y - extr.min.y);

        }
        //float ecart = 1f;
        Vector3 c = geoAnalyzer.getLocalCenterPoint(gameObject);
        Debug.Log("c = (" + c.x + ", " + c.y + ", " + c.z + ")");
        eventIcons = GetComponentsInChildren<EventIconController>();
        int n = eventIcons.Length;
        float angle = (2 * Mathf.PI) / n;
        //float r = (float)(Mathf.Max(transform.lossyScale.x, transform.lossyScale.y) / 2 + ecart);
        //Debug.Log("r = " + r);
        int i = 0;
        foreach(EventIconController e in eventIcons)
        {
            float x0 = c.x + Mathf.Cos(i * angle) * rX;
            float y0 = c.y + Mathf.Sin(i * angle) * rY;
            if(!float.IsNaN(x0) && !float.IsNaN(y0))
            {
                Debug.Log("x0=" + x0 + "   y0=" + y0);
                e.gameObject.transform.localPosition = new Vector3(x0, y0, 0f);
            }
            i++;
        }*/
        //middlePoint = geoAnalyzer.getCenterPoint(gameObject);



        /*// POSITION
        float R0 = (radius/2) + 0.04f;//0.4f;
        float R1 = Vector2.Distance(new Vector2(gameObject.transform.position.x, gameObject.transform.position.z), new Vector2(vrCamera.transform.position.x, vrCamera.transform.position.z));
        float x0 = gameObject.transform.position.x;
        float y0 = gameObject.transform.position.z;
        float x1 = vrCamera.transform.position.x;
        float y1 = vrCamera.transform.position.z;

        // Solution obtenue sur http://math.15873.pagesperso-orange.fr/IntCercl.html
        float n = (R1 * R1 - R0 * R0 - x1 * x1 + x0 * x0 - y1 * y1 + y0 * y0) / (2 * (y0 - y1));
        float dif = (x0 - x1) / (y0 - y1);
        float a = Mathf.Pow(dif, 2) + 1;
        float b = 2 * y0 * dif - 2 * n * dif - 2 * x0;
        float c = x0 * x0 + y0 * y0 + n * n - R0 * R0 - 2 * y0 * n;

        float delta = Mathf.Sqrt(b * b - 4 * a * c);

        float xP = (-b - delta) / (2 * a); // +/- delta 
        float yP = n - xP * dif;*/
        /*geoAnalyzer = new GeometryAnalyzer();
        minNmax extr = geoAnalyzer.getLocalMinNmax(gameObject);

        height = Mathf.Abs(extr.max.y - extr.min.y);
        yMax = extr.max.y - gameObject.transform.position.y;
        xMin = extr.min.x - gameObject.transform.position.x;
        eventIcons = GetComponentsInChildren<EventIconController>();
        int i = 0;
        int j = 0;
        int nbMax = 0;
        if (height < 0.08)
        {
            nbMax = 1;
        }
        else
        {
            nbMax = Mathf.Max(1, Mathf.FloorToInt(height/0.09f));
        }

        foreach(EventIconController e in eventIcons)
        {
            float dY = i * (height / nbMax);
            float dX = xMin -0.04f - j * 0.08f;

            Vector3 v = new Vector3( gameObject.transform.position.x + dX, (gameObject.transform.position.y + yMax) - dY, gameObject.transform.position.z);
            e.gameObject.transform.position = v;
            i++;
            if (i > nbMax)
            {
                i = 0;
                j++;
            }
        }*/
        eventIcons = GetComponentsInChildren<EventIconController>();
        Vector3 relativPosition = vrCamera.transform.InverseTransformPoint(gameObject.transform.position);
        GeometryAnalyzer geoAnalyzer = new GeometryAnalyzer();
        minNmax extremities = geoAnalyzer.getMinNmax(gameObject);
        float dX = Mathf.Abs((extremities.max.x - extremities.min.x) / 2);
        yMax = extremities.max.y;
        float nbColumn = Mathf.Abs(yMax - extremities.min.y) / 0.1f;
        if (gameObject.GetComponent<ArObject>().mainType == "Images")
        {
            BoxCollider collider = gameObject.GetComponent<BoxCollider>();
            Vector3 localBorder = new Vector3(collider.size.x / 2, collider.size.y/2, 0f);
            //Vector3 worldPos = gameObject.transform.TransformPoint(localBorder);
            dX = localBorder.x;
            yMax = localBorder.y;
            nbColumn = collider.size.y / 100;
        }
        
        int c = 1;
        int r = 0;
        bool isImage = (gameObject.GetComponent<ArObject>().mainType == "Images");
        foreach (EventIconController e in eventIcons)
        {
            if (isImage)
            {
                e.gameObject.transform.localPosition = new Vector3(dX + 100*c, yMax - 100*r, 0f);
            }
            else
            {
                Vector3 newRelPos = new Vector3(relativPosition.x + dX + 0.16f * c, relativPosition.y, relativPosition.z);
                Vector3 finalPos = new Vector3(vrCamera.transform.TransformPoint(newRelPos).x, yMax - 0.1f * r, vrCamera.transform.TransformPoint(newRelPos).z);
                e.gameObject.transform.position = finalPos;
            }
            r++;
            if (r > nbColumn)
            {
                r = 0;
                c++;
            }
        }

        

    }

}

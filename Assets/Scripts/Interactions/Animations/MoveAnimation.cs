﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAnimation : AnimationController
{
    private Vector3 resetPosition;
    private List<Vector3> trajectory;
    private bool animationOn;
    private int nextPosition;
    public override void launchAnimation()
    {
        nextPosition = 0;
    }
    
    private void LateUpdate()
    {
        if (nextPosition < trajectory.Count)
        {
            nextPosition++;
        }
    }

    public override void undoAnimation()
    {
        throw new System.NotImplementedException();
    }

}

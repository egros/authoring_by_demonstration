﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimationController : MonoBehaviour
{
    public abstract void launchAnimation();

    public abstract void undoAnimation();
}

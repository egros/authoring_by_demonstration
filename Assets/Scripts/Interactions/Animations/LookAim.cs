﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAim : MonoBehaviour
{
    public GameObject target;
    private GameObject myCamera;
    private float R0;

    // Start is called before the first frame update
    void Start()
    {
        myCamera = GameObject.Find("VRCamera");
        if (myCamera == null)
        {
            myCamera = GameObject.Find("FallbackObjects");
        }

    }

    public void setTarget(GameObject t)
    {
        target = t;
        R0 = t.GetComponent<ArObject>().getRadius()+0.3f;
    }

    public void Update()
    {   
        if (target.transform.position.z != myCamera.transform.position.z)
        {
            // POSITION
            //float R0 = 0.4f;
            float R1 = Vector2.Distance(new Vector2(target.transform.position.x, target.transform.position.z), new Vector2(myCamera.transform.position.x, myCamera.transform.position.z));
            float x0 = target.transform.position.x;
            float y0 = target.transform.position.z;
            float x1 = myCamera.transform.position.x;
            float y1 = myCamera.transform.position.z;

            // Solution obtenue sur http://math.15873.pagesperso-orange.fr/IntCercl.html
            float n = (R1 * R1 - R0 * R0 - x1 * x1 + x0 * x0 - y1 * y1 + y0 * y0) / (2 * (y0 - y1));
            //Debug.Log("N=" + n);
            float dif = (x0 - x1) / (y0 - y1);
            float a = Mathf.Pow(dif, 2) + 1;
            float b = 2 * y0 * dif - 2 * n * dif - 2 * x0;
            float c = x0 * x0 + y0 * y0 + n * n - R0 * R0 - 2 * y0 * n;

            //Debug.Log("a=" + a + "  b=" + b + "  c=" + c);

            float delta = Mathf.Sqrt(b * b - 4 * a * c);
            //Debug.Log("delta=" + delta);

            float xP = (-b + delta) / (2 * a);
            float yP = n - xP * dif;
            //Debug.Log("(xP ; yP) = (" + xP + " ; " + yP + ")");

            transform.position = new Vector3(xP, target.transform.position.y, yP);

            // ROTATION
            transform.LookAt(target.transform);

        }

        
        
    }



}

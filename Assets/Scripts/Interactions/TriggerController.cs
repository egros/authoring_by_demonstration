﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerController : MonoBehaviour
{
    public List<InteractionRecorder> recording;
    public abstract void AddAnimationController(AnimationController anim);
}

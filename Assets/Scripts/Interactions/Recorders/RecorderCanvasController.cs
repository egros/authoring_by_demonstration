﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecorderCanvasController : MonoBehaviour
{
    public Text timerText;
    public GameObject canvas;
    public GameObject infoPanel;
    public GameObject recordingIcon;
    private GameObject myCamera;
    private int timeLeft;

    // Start is called before the first frame update
    void Start()
    {
        myCamera = GameObject.Find("VRCamera");
        canvas.transform.SetParent(myCamera.transform);
        canvas.transform.localPosition = new Vector3(0f, 0f, 2f);
        canvas.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        //canvas.transform.Rotate(new Vector3(0f, 180f, 0f));
        canvas.SetActive(false);
    }

    public void startRecording()
    {
        canvas.SetActive(true);
        infoPanel.SetActive(true);
        recordingIcon.SetActive(false);
        timeLeft = ApplicationModel.reccordingWait;
        timerText.text = timeLeft.ToString();
        InvokeRepeating("decrementTime", 0f, 1f);
    }

    public void decrementTime()
    {
        timeLeft = timeLeft - 1;
        if (timeLeft >= 0)
        {
            timerText.text = timeLeft.ToString();
        }
        else
        {
            CancelInvoke();
            infoPanel.SetActive(false);
            recordingIcon.SetActive(true);
        }
    }

    public void stopReccording()
    {
        recordingIcon.SetActive(false);
        canvas.SetActive(false);
    }

}

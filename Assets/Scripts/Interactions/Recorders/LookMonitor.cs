﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookMonitor : MonoBehaviour
{
    //Renderer renderer;
    List<EventIconController> lookEventsToDetect;
    Camera vrCamera;

    // Start is called before the first frame update
    void Start()
    {
        vrCamera = GameObject.Find("VRCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {

        if (ApplicationModel.mode == "Test")
        {
            Vector3 screenPoint = vrCamera.WorldToViewportPoint(transform.position);
            bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
            if (onScreen)
            {
                foreach(EventIconController e in lookEventsToDetect)
                {
                    e.trigerDone();
                }
            }
        }
        
    }

    public void addLookToDetect(EventIconController lookEvent)
    {
        if (lookEventsToDetect == null)
        {
            lookEventsToDetect = new List<EventIconController>();
        }
        lookEventsToDetect.Add(lookEvent);
    }
}

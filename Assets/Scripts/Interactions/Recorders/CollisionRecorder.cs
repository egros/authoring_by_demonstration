﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class CollisionRecorder : MonoBehaviour
{
    private InteractionRecorder recorder;
    private GameObject previousCollision;
    private Dictionary<int, EventIconController> collisionsToDetect;

    private Dictionary<int, RecordedCollision> collisionDict;
    private InteractionAnalyzer iconMaker;

    public void Start()
    {
        collisionDict = new Dictionary<int, RecordedCollision>();
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
    }

    public void setRecorder(InteractionRecorder record)
    {
        
        recorder = record;
        if (collisionsToDetect == null)
        {
            collisionsToDetect = new Dictionary<int, EventIconController>();
        }
    }

    public void addCollisionToDetect(EventIconController eventIcon)
    {
        if (collisionsToDetect == null)
        {
            collisionsToDetect = new Dictionary<int, EventIconController>();
        }
        RecordedCollision collisionEvent = (RecordedCollision)eventIcon.getRecordedEvent();
        GameObject collidedObject = collisionEvent.getColllidedObject();
        if (collidedObject != null)
        {
            ArObject arComponent = collidedObject.GetComponent<ArObject>();
            collisionsToDetect.Add(arComponent.getID(), eventIcon);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (recorder != null)
        {
            if (recorder.getIsRecording())
            {
                if (other.gameObject.GetComponent<ArObject>()!=null && other.gameObject != previousCollision)
                {
                    TwinController twinController = GetComponent<TwinController>();
                    if (twinController == null && transform.parent!=null && transform.parent.name != "ARCreations")
                    {
                        string n1 = gameObject.name;
                        string n2 = other.gameObject.name;
                        //recorder.addCollision(other, gameObject); //other=collider  ; gameobject=collidingObject
                        //MoveRecorder movRcd = gameObject.GetComponent<MoveRecorder>();
                        /*if (movRcd != null)
                        {
                            if (movRcd.getLastMoveSize() > 1)
                            {
                                movRcd.registerMove();
                                //addMove(collidingObject);
                            }
                        }*/
                        RecordedCollision lastCollision = ScriptableObject.CreateInstance<RecordedCollision>(); //new RecordedCollision();
                        lastCollision.setCollision(other, gameObject);

                        int objID = other.gameObject.GetComponent<ArObject>().getID();
                        if (!collisionDict.ContainsKey(objID))
                        {
                            collisionDict.Add(objID, lastCollision);
                            iconMaker.createEventIcon(lastCollision);
                        }
                        else if(collisionDict.ContainsKey(objID) && collisionDict[objID] == null)
                        {
                            collisionDict[objID] = lastCollision;
                            iconMaker.createEventIcon(lastCollision);
                        }
                        //recordedEvents.Add(lastCollision);
                    }
                    // Otherwise the twin will already have added the collision
                    previousCollision = other.gameObject;
                }
            }
        }
        if (ApplicationModel.mode == "Test")
        {
            if (collisionsToDetect == null)
            {
                collisionsToDetect = new Dictionary<int, EventIconController>();
            }
            ArObject arComponent = other.gameObject.GetComponent<ArObject>();
            if (arComponent != null)
            {
                if (collisionsToDetect.ContainsKey(arComponent.getID()))
                {
                    EventIconController ev = collisionsToDetect[arComponent.getID()];
                    if (ev != null)
                    {
                        ev.trigerDone();
                    }
                }
                else
                {
                    CollisionRecorder colRec = other.gameObject.GetComponent<CollisionRecorder>();
                    if (colRec != null && GetComponent<ArObject>()!=null)
                    {
                        colRec.checkCollision(GetComponent<ArObject>().getID());
                    }
                }

            }
        }
    }

    public void checkCollision(int i)
    {
        if (collisionsToDetect != null)
        {
            if (collisionsToDetect.ContainsKey(i))
            {
                EventIconController ev = collisionsToDetect[i];
                if (ev != null)
                {
                    ev.trigerDone();
                }
            }
        }
    }

}

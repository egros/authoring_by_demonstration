﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionArObjectsRecorder : InteractionRecorder
{
    private MoveRecorder[] moveRecorders;
    private CollisionRecorder[] collisionRecorders;

    private List<RecordedEvent> hideNshowToCancel;

    private InteractionAnalyzer iconMaker;
    // Start is called before the first frame update
    void Start()
    {
        recordedEvents = new List<RecordedEvent>();
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
        //Test sauvegarde
        //ExtractFromFile test = ScriptableObject.CreateInstance<ExtractFromFile>();
        //test.readFile();

    }

    public override void startRecording()
    {
        isRecording = true;
        recordedEvents = new List<RecordedEvent>();
        moveRecorders = GetComponentsInChildren<MoveRecorder>();
        hideNshowToCancel = new List<RecordedEvent>();
        foreach(MoveRecorder m in moveRecorders)
        {
            m.setRecorder(this);
            m.startMotoring();
        }
        collisionRecorders = GetComponentsInChildren<CollisionRecorder>();
        foreach(CollisionRecorder c in collisionRecorders)
        {
            c.setRecorder(this);
        }
    }

    public override void stopRecording()
    {
        foreach(MoveRecorder m in moveRecorders)
        {
            m.stopMonitoring();
        }
        int i = hideNshowToCancel.Count - 1;
        while (i >= 0)
        {
            hideNshowToCancel[i].cancelModifications();
            i--;
        }
        isRecording = false;
        //return recordedEvents;
    }



    /*public void addColor(Color color, GameObject target)
    {
        RecordedColor colorEvent = ScriptableObject.CreateInstance<RecordedColor>();
        colorEvent.setColorEvent(target, color);
        recordedEvents.Add(colorEvent);
    }*/

    public void addShow(GameObject showedObject)
    {
        // an object appears in the scene
        RecordedShow showEvent = ScriptableObject.CreateInstance<RecordedShow>();
        showEvent.setShow(showedObject);
        iconMaker.createEventIcon(showEvent);
        hideNshowToCancel.Add(showEvent);
        //recordedEvents.Add(showEvent);
    }

    public void addHide(GameObject hiddenObject)
    {
        // an object is hidden
        RecordedHide hideEvent = ScriptableObject.CreateInstance<RecordedHide>();//new RecordedHide();
        hideEvent.setHideEvent(hiddenObject);
        iconMaker.createEventIcon(hideEvent);
        hideNshowToCancel.Add(hideEvent);
        //recordedEvents.Add(hideEvent);
    }

    /*public void addEnterZone(RecordedZoneEnter enterEvent)
    {
        recordedEvents.Add(enterEvent);
    }*/

    /*public void addExitZone(RecordedZoneExit exitEvent)
    {
        recordedEvents.Add(exitEvent);
    }*/

    /*public void addGeneratorAction()
    {
        throw new System.NotImplementedException();
    }*/

    /*public override void addCollision(Collider collider, GameObject collidingObject)
    {
        MoveRecorder movRcd = collidingObject.GetComponent<MoveRecorder>();
        if (movRcd != null)
        {
            if (movRcd.getLastMoveSize() > 1)
            {
                addMove(collidingObject);
            }
        }
        RecordedCollision lastCollision = ScriptableObject.CreateInstance<RecordedCollision>(); //new RecordedCollision();
        lastCollision.setCollision(collider, collidingObject);
        recordedEvents.Add(lastCollision);
    }*/

    /*public override void addMove(GameObject movedObject)
    {
        List<TransformReccord> lastMove = movedObject.GetComponent<MoveRecorder>().endMove();
        //List<Vector3> lastMove = movedObject.GetComponent<MoveRecorder>().endMove();
        RecordedMovement record = ScriptableObject.CreateInstance<RecordedMovement>();//new RecordedMovement();
        TwinController twinController = movedObject.GetComponent<TwinController>();
        record.setTrajectory(lastMove, movedObject, true);
        if (twinController == null)
        {
            recordedEvents.Add(record);
        }
        // Otherwise the twin will already have added the move
    }*/

    
}

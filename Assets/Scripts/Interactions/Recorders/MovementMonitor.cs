﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct TrajectNIcon
{
    public RecordedTrajectory recordedTraj;
    public EventIconController eventIcon;

    public TrajectNIcon(RecordedTrajectory trajectory, EventIconController icon)
    {
        recordedTraj = trajectory;
        eventIcon = icon;
    }
}

public class MovementMonitor : MonoBehaviour
{
    private List<TrajectNIcon> moveNdragToMonitor;
    private bool somethingIsAttach;
    private bool isTest;

    private void Start()
    {
        moveNdragToMonitor = new List<TrajectNIcon>();
        somethingIsAttach = false;
        isTest = false;

    }


    public void addMonitoring(RecordedTrajectory traj, EventIconController icon)
    {
        TrajectNIcon newCouple = new TrajectNIcon(traj, icon);
        moveNdragToMonitor.Add(newCouple);
    }

    // Update is called once per frame
    void Update()
    {
        if (ApplicationModel.mode == "Test")
        {
            if (!isTest)
            {
                isTest = true;
            }
            foreach (TrajectNIcon t in moveNdragToMonitor)
            {
                if (t.eventIcon != null)
                {
                    bool response = t.recordedTraj.check(transform.position);
                    if (response)
                    {
                        t.eventIcon.trigerDone();
                    }
                }
            }
        }
        else
        {
            if (isTest)
            {
                isTest = false;
                foreach(TrajectNIcon t in moveNdragToMonitor)
                {
                    t.recordedTraj.resetValidated();
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionPuppetRecorder : InteractionRecorder
{
    public List<MoveRecorder> moveRecorders;
    public CollisionRecorder[] collisionRecorders;

    private void Start()
    {
        recordedEvents = new List<RecordedEvent>();
        foreach(MoveRecorder m in moveRecorders)
        {
            m.setRecorder(this);
        }
        foreach(CollisionRecorder c in collisionRecorders)
        {
            c.setRecorder(this);
        }
    }

    public override void startRecording()
    {
        isRecording = true;
        recordedEvents = new List<RecordedEvent>();
        foreach (MoveRecorder mr in moveRecorders)
        {
            mr.startMotoring();
        }
    }

    public override void stopRecording()
    {
        isRecording = false;
        foreach (MoveRecorder mr in moveRecorders)
        {
            mr.stopMonitoring();
        }
        //return recordedEvents;
    }

    /*public void isLookingAt(GameObject lookedObject)
    {
        RecordedLook pickAt = ScriptableObject.CreateInstance<RecordedLook>(); //new RecordedLook();
        pickAt.setLook(lookedObject);
        recordedEvents.Add(pickAt);
    }*/

    /*public override void addMove(GameObject movedObject)
    {
        List<TransformReccord> lastMove = movedObject.GetComponent<MoveRecorder>().endMove();
        //List<Vector3> lastMove = movedObject.GetComponent<MoveRecorder>().endMove();
        RecordedMovement record = ScriptableObject.CreateInstance<RecordedMovement>(); //new RecordedMovement();
        record.setTrajectory(lastMove, movedObject, false);
        recordedEvents.Add(record);
    }*/

    /*public override void addCollision(Collider collider, GameObject collidingObject)
    {
        MoveRecorder movRcd = collidingObject.GetComponent<MoveRecorder>();
        if (movRcd!=null)
        {
            if (movRcd.getLastMoveSize() > 1)
            {
                addMove(collidingObject);
            }
        }
        RecordedCollision lastCollision = ScriptableObject.CreateInstance<RecordedCollision>(); //new RecordedCollision();
        lastCollision.setCollision(collider, collidingObject);
        recordedEvents.Add(lastCollision);
        
    }*/

    /*public void addEnterZone(RecordedZoneEnter enterEvent)
    {
        recordedEvents.Add(enterEvent);
    }*/

    /*public void addExitZone(RecordedZoneExit exitEvent)
    {
        recordedEvents.Add(exitEvent);
    }*/
}

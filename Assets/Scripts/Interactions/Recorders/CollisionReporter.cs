﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionReporter : MonoBehaviour
{
    public Zone3DColliderController parentController;

    public void OnTriggerEnter(Collider other)
    {
        parentController.enterDetected(other);
    }

    public void OnTriggerExit(Collider other)
    {
        parentController.exitDetected(other);
    }
}

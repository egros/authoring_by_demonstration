﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractionRecorder : MonoBehaviour
{
    protected bool isRecording;
    protected List<RecordedEvent> recordedEvents;
    public abstract void startRecording();
    public abstract void stopRecording();

    public bool getIsRecording()
    {
        return isRecording;
    }

    //public abstract void addMove(GameObject movedObject);

    //public abstract void addCollision(Collider collider, GameObject collidingObject);
    public List<RecordedEvent> getRecordedEvents()
    {
        return recordedEvents;
    }
}

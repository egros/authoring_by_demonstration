﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionBothControllers : InteractionRecorder
{
    private InteractionControllerRecorder dominantHandRecorder;
    private InteractionControllerRecorder nonDominantRecorder;

    private void Start()
    {
        if (GameObject.Find(ApplicationModel.dominantHand) != null)
        {
            dominantHandRecorder = GameObject.Find(ApplicationModel.dominantHand).GetComponent<InteractionControllerRecorder>();
        }
        if (GameObject.Find(ApplicationModel.nonDominantHand) != null)
        {
            nonDominantRecorder = GameObject.Find(ApplicationModel.nonDominantHand).GetComponent<InteractionControllerRecorder>();
        }
        
    }

    public void addEvent(RecordedEvent recordedEvent)
    {
        recordedEvents.Add(recordedEvent);
    }

    /*public override void addCollision(Collider collider, GameObject collidingObject)
    {
        throw new System.NotImplementedException();
    }*/

    /*public override void addMove(GameObject movedObject)
    {
        throw new System.NotImplementedException();
    }*/

    public override void startRecording()
    {
        if (dominantHandRecorder == null)
        {
            dominantHandRecorder = GameObject.Find(ApplicationModel.dominantHand).GetComponent<InteractionControllerRecorder>();
        }
        if(nonDominantRecorder == null)
        {
            nonDominantRecorder = GameObject.Find(ApplicationModel.nonDominantHand).GetComponent<InteractionControllerRecorder>();
        }
        recordedEvents = new List<RecordedEvent>();
        isRecording = true;
        dominantHandRecorder.startRecording();
        nonDominantRecorder.startRecording();
    }

    public override void stopRecording()
    {
        isRecording = false;
        dominantHandRecorder.stopRecording();
        nonDominantRecorder.stopRecording();
        //return recordedEvents;
    }

    
}

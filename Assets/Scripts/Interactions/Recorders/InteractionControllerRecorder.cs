﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class InteractionControllerRecorder : InteractionRecorder
{
    //private string move;
    //private List<string> whatHappened;

    private MoveRecorder moveRecorder;

    private GameObject collidedObject;
    private GameObject grabbedObject;
    private Vector3 iniDragPos;

    private GameObject handCollider;

    private Valve.VR.SteamVR_Action_Boolean input;
    private InteractionBothControllers bothHandsRecorder;

    private Dictionary<int, RecordedCollision> collisionDict;
    private Dictionary<int, RecordedDrop> dropDict;
    private Dictionary<int, RecordedGrab> grabDict;

    private List<RecordedGrab> emptyGrabs;
    private List<RecordedDrop> emptyDrops;
    private List<RecordedDrag> emptyDrags;


    private List<RecordedDrag> dragToCancel;
    private InteractionAnalyzer iconMaker;

    // Start is called before the first frame update
    public void Start()
    {
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();

        bothHandsRecorder = GameObject.FindObjectOfType<InteractionBothControllers>();
        input = SteamVR_Input.GetBooleanAction("GrabGrip");
        recordedEvents = new List<RecordedEvent>();

        moveRecorder = GetComponent<MoveRecorder>();
        moveRecorder.setRecorder(this);

        if(gameObject.name == ApplicationModel.dominantHand)
        {
            handCollider = GameObject.Find("HandColliderRight(Clone)");
            
        }
        else if(gameObject.name == ApplicationModel.nonDominantHand)
        {
            handCollider = GameObject.Find("HandColliderLeft(Clone)");
        }
        if (handCollider != null)
        {
            handCollider.AddComponent<HandCollisionController>();
            handCollider.GetComponent<HandCollisionController>().setHand(gameObject.name);
        }

        if (collisionDict == null)
        {
            collisionDict = new Dictionary<int, RecordedCollision>();
        }
        if (dropDict == null)
        {
            dropDict = new Dictionary<int, RecordedDrop>();
        }
        if (grabDict == null)
        {
            grabDict = new Dictionary<int, RecordedGrab>();
        }
        if (emptyGrabs == null)
        {
            emptyGrabs = new List<RecordedGrab>();
        }
        if (emptyDrops == null)
        {
            emptyDrops = new List<RecordedDrop>();
        }
        if (emptyDrags == null)
        {
            emptyDrags = new List<RecordedDrag>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (handCollider == null)
        {
            if (gameObject.name == ApplicationModel.dominantHand)
            {
                handCollider = GameObject.Find("HandColliderRight(Clone)");

            }
            else if (gameObject.name == ApplicationModel.nonDominantHand)
            {
                handCollider = GameObject.Find("HandColliderLeft(Clone)");
            }
            if (handCollider != null)
            {
                handCollider.AddComponent<HandCollisionController>();
                handCollider.GetComponent<HandCollisionController>().setHand(gameObject.name);
            }
        }
        if(ApplicationModel.mode == "Test")
        {
            if(input.lastStateDown && gameObject.GetComponentInChildren<ArObject>() == null)
            {
                foreach(RecordedGrab g in emptyGrabs)
                {
                    if(g.getGrabbingHand().name==gameObject.name && Vector3.Distance(g.getAttachPosition(), gameObject.transform.position) < 0.1f)
                    {
                        if (g.getEventIcon() != null)
                        {
                            g.getEventIcon().GetComponent<EventIconController>().trigerDone();
                        }
                        break;
                    }
                }
            }
            if (input.lastStateUp && gameObject.GetComponentInChildren<ArObject>() == null)
            {
                Debug.Log("Empty drop length = "+emptyDrops.Count);
                foreach (RecordedDrop d in emptyDrops)
                {
                    if (d.getHand().name == gameObject.name && Vector3.Distance(d.getAttachPosition(), gameObject.transform.position) < 0.1f)
                    {
                        if (d.getEventIcon() != null)
                        {
                            d.getEventIcon().GetComponent<EventIconController>().trigerDone();
                        }
                        else
                        {
                            Debug.Log("no eventIcon");
                        }
                        break;
                    }
                    else
                    {
                        Debug.Log("Too far or not the right hand");
                    }
                }
            }
        }

        if (isRecording)
        {
            //move = move + "\n" + transform.position.ToString(); 
            //Grab
            if (input.lastStateDown && input.activeDevice.ToString()==gameObject.name)
            {
                if (grabbedObject == null)
                {
                    if (moveRecorder.getLastMoveSize() > 1)
                    {
                        addMove(gameObject);
                    }
                    RecordedGrab grab = ScriptableObject.CreateInstance<RecordedGrab>();//new RecordedGrab();
                    grab.setGrabbedObject(null, transform.position, gameObject);
                    emptyGrabs.Add(grab);
                    /*if (ApplicationModel.recordedTarget == "BothHands")
                    {
                        bothHandsRecorder.addEvent(grab);
                    }
                    else
                    {
                        recordedEvents.Add(grab);
                    }*/
                    //whatHappened.Add("Grabing nothing");
                    iconMaker.createEventIcon(grab);
                }
            }
            //Drop
            if (input.lastStateUp)
            {
                if (grabbedObject == null)
                {
                    if (moveRecorder.getLastMoveSize() > 1)
                    {
                        addMove(gameObject);
                    }
                    RecordedDrop drop = ScriptableObject.CreateInstance<RecordedDrop>();//new RecordedDrop();
                    drop.setDroppedObject(null, transform.position, gameObject);
                    emptyDrops.Add(drop);
                    /*if (ApplicationModel.recordedTarget == "BothHands")
                    {
                        bothHandsRecorder.addEvent(drop);
                    }
                    else
                    {
                        recordedEvents.Add(drop);
                    }*/
                    iconMaker.createEventIcon(drop);
                }
            }
        }
    }

    public void addDrag(GameObject draggedObject)
    {
        List<TransformReccord> trajectory = moveRecorder.endMove();
        RecordedDrag dragEvent = ScriptableObject.CreateInstance<RecordedDrag>();//new RecordedDrag();
        dragEvent.setElements(grabbedObject, trajectory, gameObject.name, iniDragPos);
        iconMaker.createEventIcon(dragEvent);
        dragToCancel.Add(dragEvent);
    }

    public void addMove(GameObject movedObject)
    {
        
        if (grabbedObject != null)
        {
            //addDrag(grabbedObject);
        }
        else
        {
            List<TransformReccord> lastMove = moveRecorder.endMove();
            /*RecordedMovement moveEvent = ScriptableObject.CreateInstance<RecordedMovement>(); //new RecordedMovement();
            moveEvent.setTrajectory(lastMove, movedObject, false);
            if (ApplicationModel.recordedTarget == "BothHands")
            {
                bothHandsRecorder.addEvent(moveEvent);
            }
            else
            {
                recordedEvents.Add(moveEvent);
            }*/
            //whatHappened.Add("Moved from " + lastMove[0] + " to " + lastMove[lastMove.Count-1]);
        }
    }

    public override void startRecording()
    {
        //whatHappened = new List<string>();
        dragToCancel = new List<RecordedDrag>();
        recordedEvents = new List<RecordedEvent>();
        moveRecorder.startMotoring();
        //whatHappened.Add("Start");
        isRecording = true;
        if (iconMaker == null)
        {
            iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
        }
    }

    public override void stopRecording()
    {
        addMove(gameObject);
        isRecording = false;
        //move = "";
        moveRecorder.stopMonitoring();
        int i = dragToCancel.Count - 1;
        while (i >= 0)
        {
            dragToCancel[i].cancelModifications();
            i--;
        }
        //whatHappened.Add("End of recording");
        //return recordedEvents;
    }

    public void grabbing(GameObject grabbed)
    {
        if (moveRecorder.getLastMoveSize() > 1)
        {
            addMove(gameObject);
        }
        grabbedObject = grabbed;
        RecordedGrab grabEvent = ScriptableObject.CreateInstance<RecordedGrab>();
        Vector3 v = new Vector3(transform.position.x-grabbedObject.transform.position.x, transform.position .y- grabbedObject.transform.position.y, transform.position.z - grabbedObject.transform.position.z);
        grabEvent.setGrabbedObject(grabbedObject, v, gameObject);
        iniDragPos = grabbed.transform.position;
        /*if (ApplicationModel.recordedTarget == "BothHands")
        {
            bothHandsRecorder.addEvent(grabEvent);
        }
        else
        {
            recordedEvents.Add(grabEvent);
        }*/
        if(grabbed!=null && grabbed.GetComponent<ArObject>() != null)
        {
            int objID = grabbed.GetComponent<ArObject>().getID();
            if (!grabDict.ContainsKey(objID))
            {
                grabDict.Add(objID, grabEvent);
                iconMaker.createEventIcon(grabEvent);
            }
            else if(grabDict.ContainsKey(objID) && grabDict[objID] == null)
            {
                grabDict[objID] = grabEvent;
                iconMaker.createEventIcon(grabEvent);
            }
        }
        else
        {
            iconMaker.createEventIcon(grabEvent);
        }
        //whatHappened.Add("Grabbing " + grabbedObject.name);
    }


    public void dropping(GameObject dropped)
    {
        if (moveRecorder.getLastMoveSize() > 1)
        {
            //addMove(gameObject);
            addDrag(dropped);
        }
        RecordedDrop dropEvent = ScriptableObject.CreateInstance<RecordedDrop>();
        dropEvent.setDroppedObject(dropped, grabbedObject.transform.position, gameObject);
        /*if (ApplicationModel.recordedTarget == "BothHands")
        {
            bothHandsRecorder.addEvent(dropEvent);
        }
        else
        {
            //recordedEvents.Add(dropEvent);
        }*/
        if (dropped != null && dropped.GetComponent<ArObject>()!=null)
        {
            int objID = dropped.GetComponent<ArObject>().getID();
            if (!dropDict.ContainsKey(objID))
            {
                dropDict.Add(objID, dropEvent);
                iconMaker.createEventIcon(dropEvent);
            }
            else if(dropDict.ContainsKey(objID) && dropDict[objID] == null)
            {
                dropDict[objID] = dropEvent;
                iconMaker.createEventIcon(dropEvent);
            }
        }
        else
        {
            iconMaker.createEventIcon(dropEvent);
        }
        
        grabbedObject = null;
    }

    public void newCollision(Collider collision)
    {
        int objID = -1;
        if (collision.gameObject.GetComponent<ArObject>() != null)
        {
            objID = collision.gameObject.GetComponent<ArObject>().getID();
        }
        if (moveRecorder.getLastMoveSize() > 1)
        {
            moveRecorder.registerMove();
            //addMove(gameObject);
        }
        collidedObject = collision.gameObject;
        RecordedCollision collisionEvent = ScriptableObject.CreateInstance<RecordedCollision>();
        collisionEvent.setCollision(collision, gameObject);

        if (!collisionDict.ContainsKey(objID))
        {
            collisionDict.Add(objID, collisionEvent);
            iconMaker.createEventIcon(collisionEvent);
        }
        else if(collisionDict.ContainsKey(objID) && collisionDict[objID] == null)
        {
            collisionDict[objID] = collisionEvent;
            iconMaker.createEventIcon(collisionEvent);
        }

    }

    /*public override void addCollision(Collider collider, GameObject collidingObject)
    {
        if (moveRecorder.getLastMoveSize() > 1)
        {
            addMove(gameObject);
        }
        collidedObject = collider.gameObject;
        RecordedCollision collisionEvent = ScriptableObject.CreateInstance<RecordedCollision>(); 
        collisionEvent.setCollision(collider, collidingObject);
        if (ApplicationModel.recordedTarget == "BothHands")
        {
            bothHandsRecorder.addEvent(collisionEvent);
        }
        else
        {
            //recordedEvents.Add(collisionEvent);
            
        }
        //whatHappened.Add(" Collision with " + collidedObject.name);
    }*/

    /*public void endCollision(Collision collision)
    {
        collidedObject = null;
    }*/

    public void addRecordedDrop(RecordedDrop dropEvent)
    {
        if (dropEvent.getObjectOfInterestID() == -1)
        {
            emptyDrops.Add(dropEvent);
        }
        else
        {
            dropDict.Add(dropEvent.getObjectOfInterestID(), dropEvent);
        }
    }

    public void addRecordedGrab(RecordedGrab grabEvent)
    {
        if (grabEvent.getObjectOfInterestID() == -1)
        {
            emptyGrabs.Add(grabEvent);
        }
        else
        {
            grabDict.Add(grabEvent.getObjectOfInterestID(), grabEvent);
        }
    }

}

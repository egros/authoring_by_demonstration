﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRecorder : MonoBehaviour
{
    private bool isMoving;
    private int stayStill;
    private List<List<Vector3>> positions;
    private int positionIndex;
    private int moveIndex;
    private List<List<TransformReccord>> transformReccords;
    private bool isPuppet;

    private InteractionRecorder parentRecorder;
    private InteractionAnalyzer iconMaker;

    private List<RecordedMovement> lastSessionRecorded;

    private float rate = 0.1f; //every 0.2 seconds

    // Start is called before the first frame update
    void Start()
    {
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
        positionIndex = 0;
        moveIndex = 0;
        stayStill = 0;
        if(gameObject.name == "WoodBody")
        {
            isPuppet = true;
        }
        else
        {
            isPuppet = false;
        }

        /*parentRecorder = GetComponent<InteractionRecorder>();
        if (parentRecorder == null)
        {
            parentRecorder = GetComponentInParent<InteractionRecorder>();
        }*/
    }

    public void setRecorder(InteractionRecorder recorder)
    {
        parentRecorder = recorder;
    }

    public void startMotoring()
    {
        lastSessionRecorded = new List<RecordedMovement>();
        positions = new List<List<Vector3>>();
        List<Vector3> firstMove = new List<Vector3>();
        Vector3 pos = transform.position;
        if (isPuppet)
        {
            pos = new Vector3(pos.x, 1f, pos.z);
        }
        firstMove.Add(pos);
        positions.Add(firstMove);
        positionIndex = 0;
        moveIndex = 0;
        stayStill = 0;

        // Transforms
        transformReccords = new List<List<TransformReccord>>();
        List<TransformReccord> firstTransformMove = new List<TransformReccord>();
        firstTransformMove.Add(new TransformReccord(pos, transform.rotation));
        transformReccords.Add(firstTransformMove);

        InvokeRepeating("SlowUpdate", 0.0f, rate);
    }

    public void restartMonitoring()
    {
        endMove();
        InvokeRepeating("SlowUpdate", 0.0f, rate);
    }

    public void stopMonitoring()
    {
        int i = lastSessionRecorded.Count-1;
        while (i >= 0)
        {
            lastSessionRecorded[i].cancelModifications();
            i--;
        }
        CancelInvoke();
    }

    private void SlowUpdate()
    {
        //Rotation

        //Move
        Vector3 pos = transform.position;
        if (isPuppet)
        {
            pos = new Vector3(pos.x, 1f, pos.z);
        }
        if (Vector3.Distance(positions[positionIndex][moveIndex], pos) > 0.025f)
        {
            if (!isMoving)
            {
                isMoving = true;
            }
            positions[positionIndex].Add(pos);

            // Transform reccord
            transformReccords[positionIndex].Add(new TransformReccord(pos, transform.rotation));
            
            moveIndex++;
        }
        else
        {
            if (isMoving)
            {
                stayStill++;
                if (stayStill > 3)
                {
                    stayStill = 0;
                    if (parentRecorder != null)
                    {
                        //parentRecorder.addMove(gameObject);
                        registerMove();
                    }
                    else
                    {
                        Debug.Log(gameObject.name + " has no parent reccorder");
                    }
                    
                }
                else
                {
                    positions[positionIndex].Add(pos);

                    //Transform reccord
                    transformReccords[positionIndex].Add(new TransformReccord(pos, transform.rotation));

                    moveIndex++;
                }
            }
        }
    }

    public void registerMove()
    {
        TwinController twinController = gameObject.GetComponent<TwinController>();
        if (twinController == null)
        {
            if(parentRecorder is InteractionControllerRecorder)
            {
                InteractionControllerRecorder contRecorder = (InteractionControllerRecorder)parentRecorder;
                contRecorder.addMove(gameObject);
            }
            else
            {
                List<TransformReccord> lastMove = endMove();
                RecordedMovement record = ScriptableObject.CreateInstance<RecordedMovement>();
                
                record.setTrajectory(lastMove, gameObject, true);
                if (iconMaker == null)
                {
                    iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
                }
                iconMaker.createEventIcon(record);
                lastSessionRecorded.Add(record);
            }
            //recordedEvents.Add(record);
            
        }
    }

    //public List<Vector3> endMove()
    public List<TransformReccord> endMove()
    {
        Vector3 pos = transform.position;
        if (isPuppet)
        {
            pos = new Vector3(pos.x, 1f, pos.z);
        }
        isMoving = false;
        List<Vector3> nextMove = new List<Vector3>();
        nextMove.Add(pos);
        positions.Add(nextMove);

        // Transform reccord
        List<TransformReccord> nextTransform = new List<TransformReccord>();
        nextTransform.Add(new TransformReccord(pos, transform.rotation));
        transformReccords.Add(nextTransform);

        positionIndex++;
        moveIndex = 0;
        //return positions[positionIndex - 1];
        return transformReccords[positionIndex-1];
    }

    public int getLastMoveSize()
    {
        return moveIndex;
    }

}

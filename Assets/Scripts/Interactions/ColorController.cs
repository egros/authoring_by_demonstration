﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorController : MonoBehaviour
{
    public ColorPickerTriangle colorPicker;
    private Material mat;

    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
        colorPicker.SetNewColor(mat.color);
    }


    private void Update()
    {
        mat.color = colorPicker.TheColor;
    }
}

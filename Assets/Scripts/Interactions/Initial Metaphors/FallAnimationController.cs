﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class FallAnimationController : AnimationController
{
    public Valve.VR.SteamVR_Action_Boolean input;

    private GameObject fallingObject;
    private Vector3 initialPosition;
    private Quaternion initialRotation;
    private GameObject rightHand;
    private string oldMode;

    private DebugController debugger;

    // Start is called before the first frame update
    void Start()
    {
        debugger = GameObject.Find("Debugger").GetComponent<DebugController>();

        rightHand = GameObject.Find("RightHand");
        //Debug.Log("Previous parent : "+transform.parent);
        transform.parent = rightHand.transform;
        transform.localPosition = new Vector3(0, 0f, 0f);
        transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
        oldMode = ApplicationModel.mode;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp("f"))
        {
            launchAnimation();
        }
        if (ApplicationModel.mode != oldMode)
        {
            if (ApplicationModel.mode == "Test")
            {
                //Hide renderer
                Debug.Log("Need to hide fall renderer");
                Debug.Log("Initial position : "+ fallingObject.transform.position);
                initialPosition = fallingObject.transform.position;
                initialRotation = fallingObject.transform.rotation;
            }
            if (oldMode == "Test")
            {
                Debug.Log("Let's undo animation");
                undoAnimation();
            }
            oldMode = ApplicationModel.mode;
        }

    }

    public override void launchAnimation()
    {
        Debug.Log("Careful it is raining!");
        Rigidbody rBody = fallingObject.GetComponent<Rigidbody>();
        if (rBody != null)
        {
            rBody.isKinematic = false;
            rBody.useGravity = true;
        }
        else
        {
            fallingObject.AddComponent<Rigidbody>();
            rBody = fallingObject.GetComponent<Rigidbody>();
            rBody.useGravity = true;
            rBody.isKinematic = false;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "ArObjects" && ApplicationModel.mode == "InstallFall")
        {
            debugger.display("Click to attach");
            rightHand.GetComponent<Hand>().ShowGrabHint();
        }
        if (other.gameObject.tag == "ArObjects" && input.lastStateDown && ApplicationModel.mode=="InstallFall")
        {
            rightHand.GetComponent<Hand>().HideGrabHint();
            debugger.display("Attached!");
            fallingObject = other.gameObject;
            transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            transform.parent = fallingObject.transform;
            transform.localPosition = new Vector3(0f, transform.localPosition.y,0f);
            initialPosition = fallingObject.transform.position;
            initialRotation = fallingObject.transform.rotation;
            ApplicationModel.mode = "simple";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "ArObjects" && ApplicationModel.mode == "InstallFall")
        {
            rightHand.GetComponent<Hand>().HideGrabHint();
        }
    }

    public override void undoAnimation()
    {
        fallingObject.transform.position = initialPosition;
        fallingObject.transform.rotation = initialRotation;
        Rigidbody rBody = fallingObject.GetComponent<Rigidbody>();
        rBody.isKinematic = true;
        rBody.useGravity = false;
        Debug.Log("Set position back to : "+initialPosition);
    }


}

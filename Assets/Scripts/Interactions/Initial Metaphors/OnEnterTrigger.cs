﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnterTrigger : TriggerController
{

    public Valve.VR.SteamVR_Action_Boolean input;
    private List<AnimationController> animationsToTrigger;
    public GameObject area;


    private SphereCollider myCollider;
    private bool editing;
    private float oldDistance;
    private float initialRadius;
    private float initialColliderRadius;
    private GameObject rightHand;
    private GameObject leftHand;


    private DebugController debugger;
    // Start is called before the first frame update
    void Start()
    {
        debugger = GameObject.Find("Debugger").GetComponent<DebugController>();

        
        myCollider = GetComponent<SphereCollider>();
        editing = true;

        rightHand = GameObject.Find("RightHand");
        leftHand = GameObject.Find("LeftHand");
        oldDistance = getHandsDistance();
        initialRadius = area.transform.localScale.x;
        initialColliderRadius = myCollider.radius;

        animationsToTrigger = new List<AnimationController>();
    }

    private void Update()
    {
        if (Input.GetKeyUp("s"))
        {
            setAreaRadius(1.1f);
            Debug.Log("Increase radius");
        }
        if (Input.GetKeyUp("q"))
        {
            setAreaRadius(0.9f);
            Debug.Log("Decrease radius");
        }
        if(ApplicationModel.mode == "onEnterCreation" & input.lastStateDown & editing==true)
        {
            ApplicationModel.mode = "simple";
            editing = false;
            Debug.Log("OnEnterArea set!");
            debugger.display("OnEnterArea set!");
        }
        if (ApplicationModel.mode == "onEnterCreation" & editing==true)
        {
            float newDistance = getHandsDistance();
            float ratio = newDistance / oldDistance;
            debugger.display("ratio = " + ratio);
            setAreaRadius(ratio);
        }
    }

    private float getHandsDistance()
    {
        float squareDif = Mathf.Pow(rightHand.transform.position.x - leftHand.transform.position.x, 2) + Mathf.Pow(rightHand.transform.position.y - leftHand.transform.position.y, 2) + Mathf.Pow(rightHand.transform.position.z - leftHand.transform.position.z, 2);
        return Mathf.Sqrt(squareDif);

    }

    public void edit()
    {
        editing = true;
        initialRadius = area.transform.localScale.x;
    }


    public void setAreaRadius(float delta)
    {
        area.transform.localScale = new Vector3(initialRadius * delta, 0f, initialRadius * delta);
        myCollider.radius = initialColliderRadius * delta;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "BodyCollider" && ApplicationModel.mode=="Test")
        {
            //debugger.display("Hit player! ");
            foreach(AnimationController anim in animationsToTrigger)
            {
                anim.launchAnimation();
            }
        }
        
    }

    public override void AddAnimationController(AnimationController anim)
    {
        animationsToTrigger.Add(anim);
        Debug.Log("Just added an animation controller");
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LinkController : MonoBehaviour
{
    private GameObject triggerObject;
    private GameObject animationObject;
    private GameObject rightHand;
    public Valve.VR.SteamVR_Action_Boolean input;

    void Start()
    {
        //attached = false;
        rightHand = GameObject.Find("RightHand");
        setPosition();
    }

    public void addTrigger(GameObject trig)
    {
        triggerObject = trig;
    }

    public void addAnimation(GameObject anim)
    {
        animationObject = anim;
    }

    public void setPosition()
    {
        GameObject objA=null;
        GameObject objB=null;
        if (triggerObject != null)
        {
            objA = triggerObject;
            if (animationObject != null)
            {
                objB = animationObject;
            }
            else
            {
                objB = rightHand;
            }
        }
        else
        {
            if (animationObject != null)
            {
                objA = rightHand;
                objB = animationObject;
            }
            else
            {
                transform.localScale = new Vector3(0.1f, 0.01f, 0.1f);
                transform.localPosition = rightHand.transform.position;
            }
        }

        if(objA!=null && objB != null)
        {
            float deltaX = objA.transform.position.x - objB.transform.position.x;
            float deltaY = objA.transform.position.y - objB.transform.position.y;
            float deltaZ = objA.transform.position.z - objB.transform.position.z;

            float distanceXYZ = Mathf.Sqrt(Mathf.Pow(deltaX,2)+ Mathf.Pow(deltaY, 2)+ Mathf.Pow(deltaZ, 2));
            transform.position = Vector3.Lerp(objA.transform.position, objB.transform.position, 0.5f);
            transform.localScale = new Vector3(0.1f, distanceXYZ/2, 0.1f);
            transform.up = objA.transform.position - objB.transform.position;
        }

    }
    // Update is called once per frame
    void Update()
    {
        setPosition();
    }

    private void OnTriggerStay(Collider other)
    {
        //ADD MODE + INPUT TO VALIDATE
        if (ApplicationModel.mode == "SetLink" && (triggerObject == null || animationObject == null))
        {
            if (other.gameObject.tag == "TriggerObjects" || other.gameObject.tag == "AnimationObjects")
            {
                float distance = Vector3.Distance(other.gameObject.transform.position, rightHand.transform.position);
                
                if (isInside(rightHand.transform.position, other.gameObject))
                {
                    if (other.gameObject.tag == "TriggerObjects" && triggerObject == null)
                    {
                        rightHand.GetComponent<Hand>().ShowGrabHint();
                        if (input.lastStateDown)
                        {
                            triggerObject = other.gameObject;
                            rightHand.GetComponent<Hand>().HideGrabHint();
                        }
                    }
                    else if (other.gameObject.tag == "AnimationObjects" && animationObject == null)
                    {
                        rightHand.GetComponent<Hand>().ShowGrabHint();
                        if (input.lastStateDown)
                        {
                            animationObject = other.gameObject;
                            rightHand.GetComponent<Hand>().HideGrabHint();
                        }
                        
                    }
                }
            }
            if(triggerObject!=null && animationObject != null)
            {
                triggerObject.GetComponent<TriggerController>().AddAnimationController(animationObject.GetComponent<AnimationController>());
                ApplicationModel.mode = "simple";
            }
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "TriggerObjects" || other.gameObject.tag== "AnimationObjects")
        {
            rightHand.GetComponent<Hand>().HideGrabHint();
        }
        
    }

    private bool isInside(Vector3 pos, GameObject obj)
    {
        float dX = obj.transform.localScale.x + 0.1f;
        float dY = obj.transform.localScale.y + 0.1f;
        float dZ = obj.transform.localScale.z + 0.1f;
        
        if(pos.x>(obj.transform.position.x-dX) && pos.x < (obj.transform.position.x + dX) && pos.y > (obj.transform.position.y - dY) && pos.y < (obj.transform.position.y + dY) && pos.z > (obj.transform.position.z - dZ) && pos.z < (obj.transform.position.z + dZ))
        {
            return true;
        }
        return false;
    }
}

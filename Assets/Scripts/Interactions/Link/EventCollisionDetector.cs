﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCollisionDetector : MonoBehaviour
{
    private EventAnimationLinkController link;
    private bool isMonitoring;

    // Start is called before the first frame update
    void Start()
    {
        link = null;
        isMonitoring = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setLink(EventAnimationLinkController l)
    {
        link = l;
    }

    public void setMonitoring(bool monitoring)
    {
        isMonitoring = monitoring;
    }

}

﻿using BezierSolution;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JackBezierController : MonoBehaviour
{
    public JackConnectorController jackConnector;
    public BezierSpline spline;
    private BezierPoint[] points;

    private EventIconController trigger;
    private EventIconController action;

    // Start is called before the first frame update
    void Start()
    {
        points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
    }

    public void setTrigger(EventIconController triggerIcon)
    {
        trigger = triggerIcon;
        if (points == null)
        {
            points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
        }
        updateBezier();
    }

    public void updateBezier()
    {
        //Vector3 v = new Vector3(trigger.transform.position.x, trigger.transform.position.y, trigger.transform.position.z + 0.04f);
        points[0].gameObject.transform.position = trigger.transform.position;
        points[1].gameObject.transform.position = jackConnector.gameObject.transform.position;
    }

    public void Update()
    {
        updateBezier();
    }

    public void restoreLink(EventIconController e)
    {
        action = e;
        // place connector on other event
        jackConnector.attachToAction(e);
    }

    public EventIconController getAction()
    {
        return action;
    }

    public int getEventID()
    {
        return trigger.getID();
    }

    public void unpluged()
    {
        if (action == null)
        {
            // This is the unconnected jack => goes back to trigger
            trigger.replaceJackOnSpot();
            trigger.noLongerNeedToDisplayJack(5);
        }
        else
        {
            // Destroy connector
            Destroy(gameObject);
        }
    }

    public void pluged(EventIconController actionIcon)
    {
        if (action == null)
        {
            // add jack to trigger list and create new unconnected jack
            trigger.connectedLastJack();
        }
        action = actionIcon;
    }

    public void OnDestroy()
    {
        if (trigger != null)
        {
            Destroy(jackConnector.gameObject);
        }
    }

    public bool connectorIsAttached()
    {
        if (jackConnector.gameObject.transform.IsChildOf(gameObject.transform))
        {
            return true;
        }
        return false;
    }

    public void triggerAnimationIcon()
    {
        action.triggerAnimation();
    }

    public int getAnimationID()
    {
        if (action == null)
        {
            return -1;
        }
        else
        {
            return action.getID();
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class EventAnimationLinkController : MonoBehaviour
{
    private EventIconController trigger;
    private EventIconController animationTrigered;
    private GameObject holdingParent;
    private HandCollisionController handCollision;
    private BezierCurveController bezier;
    private bool isAnchored;
    private SceneHandler sceneHandler;

    // Start is called before the first frame update
    void Start()
    {
        //isAnchored = false;
        sceneHandler = GameObject.FindObjectOfType<SceneHandler>();
        if (!isAnchored)
        {
            if (sceneHandler.isLinkMonitoring())
            {
                Debug.Log("Destroying link in start");
                Destroy(gameObject);
            }
            else
            {
                holdingParent = GameObject.Find(ApplicationModel.dominantHand);
                //change holding parent in case of laser
                bezier = GetComponent<BezierCurveController>();
                bezier.setSceneHandler(sceneHandler);
                sceneHandler.setLink(this);
            }
        }
        
    }

    public bool getIsAnchored()
    {
        return isAnchored;
    }

    public bool triggerIsNull()
    {
        if (trigger == null)
        {
            return true;
        }
        return false;
    }

    public bool animationIsNull()
    {
        if (animationTrigered == null)
        {
            return true;
        }
        return false;
    }

    public void setTriggerAnimation(EventIconController trig, EventIconController anim)
    {
        trigger = trig;
        animationTrigered = anim;
        trigger.addAnimation(animationTrigered);
        isAnchored = true;
        if (bezier == null)
        {
            bezier = GetComponent<BezierCurveController>();
        }
        bezier.setPoints(trigger.gameObject, animationTrigered.gameObject);
    }

    public void addTrigger(EventIconController triggerIcon)
    {
        trigger = triggerIcon;
        //bezier.setPoints(trigger.gameObject, holdingParent);
        bezier.setAnchor(1, trigger.gameObject);
    }

    public void addAnimation(EventIconController anim)
    {
        if (anim.isAnimation() && trigger!=null)
        {
            animationTrigered = anim;
            trigger.addAnimation(animationTrigered);
            sceneHandler.stopMonitoring();
            //handCollision.setMonitoring(false);
            //bezier.setPoints(trigger.gameObject, animationTrigered.gameObject);
            bezier.setAnchor(2, animationTrigered.gameObject);
            isAnchored = true;
            
        }
    }

    public EventIconController getTrigger()
    {
        return trigger;
    }

    private void Update()
    {
        if(isAnchored && (animationTrigered==null || trigger == null))
            // either the trigger eventIcon or action eventIcon has been destroyed
        {
            Destroy(gameObject);
        }
    }


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JackConnectorController : MonoBehaviour
{
    public JackBezierController linkController;
    private EventIconController collidingEvent;
    private CircleCalculator circleCalcul;
    private Vector3 localAttachPoint;

    public void Start()
    {
        collidingEvent = null;
        circleCalcul = ScriptableObject.CreateInstance<CircleCalculator>();
    }

    public void OnTriggerEnter(Collider other)
    {
        EventIconController icon = other.GetComponent<EventIconController>();
        if(icon!=null && icon.getID() != linkController.getEventID() && icon.isAnimation())
        {
            // Can be linked if released
            collidingEvent = icon;
            collidingEvent.activateOutline(Color.yellow);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        EventIconController icon = other.GetComponent<EventIconController>();
        if (icon != null && icon.getID() != linkController.getEventID() && icon.isAnimation() && collidingEvent!=null)
        {
            collidingEvent.recoverDefaultOutline();
            collidingEvent = null;
            // If used to be attached to the eventIcon
        }
        
    }

    public void OnDisable()
    {
        if (collidingEvent != null)
        {
            collidingEvent.recoverDefaultOutline();
        }
    }

    public void attachToAction(EventIconController icon)
    {
        if (circleCalcul == null)
        {
            circleCalcul = ScriptableObject.CreateInstance<CircleCalculator>();
        }
        transform.SetParent(icon.transform);
        transform.LookAt(icon.transform);
        transform.Rotate(new Vector3(0, 180, 0));
        Vector3 pos = circleCalcul.getlineNCircleIntersect(transform.localPosition, 0.1f);
        transform.localPosition = pos;
        localAttachPoint = pos;
    }

    public void onRelease()
    {
        if (collidingEvent != null)
        {
            // Add collidingEvent as animation

            // And attach the jack connector to it
            transform.SetParent(collidingEvent.transform);
            transform.LookAt(collidingEvent.transform);
            transform.Rotate(new Vector3(0, 180, 0));
            Vector3 pos = circleCalcul.getlineNCircleIntersect(transform.localPosition, 0.1f);
            transform.localPosition = pos;
            localAttachPoint = pos;

            collidingEvent.recoverDefaultOutline();
            linkController.pluged(collidingEvent);
            // put in the right position
        }
        else
        {
            // Remove collidingEvent as animation
            if (linkController != null)
            {
                transform.SetParent(linkController.gameObject.transform);
                linkController.unpluged();
            }
            else
            {
                Destroy(gameObject);
            }
            // Go back to trigger and disappear
        }
    }
}

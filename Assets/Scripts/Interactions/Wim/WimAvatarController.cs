﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class WimAvatarController : MonoBehaviour
{
    public SteamVR_Action_Boolean input;

    private Interactable interactable;
    private GameObject initialParent;
    private bool isAttached;
    private string holdingHand;
    private GameObject myCamera;
    private GameObject player;
    private WimController wim;
    private Vector3 delta;

    // Start is called before the first frame update
    void Start()
    {
        interactable = GetComponent<Interactable>();
        isAttached = false;
        myCamera = GameObject.Find("VRCamera");
        player = GameObject.Find("Player");
    }

    public void setWim(GameObject worldInMini)
    {
        wim = worldInMini.GetComponent<WimController>();
        delta = wim.getDelta();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAttached)
        {
            transform.localPosition = new Vector3(myCamera.transform.position.x - delta.x, 0, myCamera.transform.position.z - delta.z);
            transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            transform.Rotate(new Vector3(0f, myCamera.transform.localRotation.eulerAngles.y, 0f));
        }
        else if(isAttached && input.lastStateUp && input.activeDevice.ToString() == holdingHand)
        {
            isAttached = false;
            transform.SetParent(initialParent.transform);
            initialParent = null;
            holdingHand = null;
            transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
            Vector3 rotation = transform.localRotation.eulerAngles;
            transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            transform.Rotate(new Vector3(0f, rotation.y, 0f));
            player.transform.position = new Vector3(transform.localPosition.x + delta.x, 0f, transform.localPosition.z +delta.z);//transform.localPosition;
            wim.displayInFrontOfPlayer();
        }
    }


    public virtual void HandHoverUpdate(Hand hand)
    {
        GrabTypes grabType = hand.GetGrabStarting();

        if (grabType != GrabTypes.None && !isAttached)//grab
        {

            if (transform.parent != null)
            {
                initialParent = transform.parent.gameObject;
            }
            else
            {
                initialParent = null;
            }
            holdingHand = hand.gameObject.name;
            transform.SetParent(hand.transform);
            isAttached = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WimTwinPuppetController : MonoBehaviour
{
    private GameObject wim;
    public GameObject realTwin;

    private Vector3 oldTwinPosition;
    private Quaternion oldTwinRotation;

    private Vector3 oldRealPosition;
    private Quaternion oldRealRotation;

    public void setWimReferent(GameObject referent)
    {
        wim = referent;

        /*Vector3 relativePosition = wim.transform.TransformPoint(realTwin.transform.position);
        transform.position = relativePosition;
        transform.rotation = realTwin.transform.rotation;*/

        oldTwinPosition = wim.transform.InverseTransformPoint(transform.position);
        oldTwinRotation = transform.rotation;

        oldRealPosition = realTwin.transform.position;
        oldRealRotation = realTwin.transform.rotation;
    }

    public void adjust()
    {
        Vector3 relativePosition = wim.transform.TransformPoint(realTwin.transform.position);
        transform.position = relativePosition;
        transform.rotation = realTwin.transform.rotation;

        oldTwinPosition = wim.transform.InverseTransformPoint(transform.position);
        oldTwinRotation = transform.rotation;

        oldRealPosition = realTwin.transform.position;
        oldRealRotation = realTwin.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if(oldTwinPosition != wim.transform.InverseTransformPoint(transform.position) || oldTwinRotation != transform.rotation)
        {
            realTwin.transform.position = wim.transform.InverseTransformPoint(transform.position);
            realTwin.transform.rotation = transform.rotation;
        }
        else if(oldRealPosition!=realTwin.transform.position || oldRealRotation != realTwin.transform.rotation)
        {
            Vector3 relativePosition = wim.transform.TransformPoint(realTwin.transform.position);
            transform.position = relativePosition;
            transform.rotation = realTwin.transform.rotation;

        }

        oldTwinPosition = wim.transform.InverseTransformPoint(transform.position);
        oldTwinRotation = transform.rotation;
        oldRealPosition = realTwin.transform.position;
        oldRealRotation = realTwin.transform.rotation;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwinController : MonoBehaviour
{
    public GameObject twin;
    private GameObject parent;
    private Vector3 oldTwinPosition;
    private Vector3 oldPosition;
    private Quaternion oldTwinRotation;
    private Quaternion oldRotation;

    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent.gameObject;
        oldPosition = transform.position;
        oldRotation = transform.rotation;

        if (twin != null)
        {
            oldTwinPosition = twin.transform.position;
            oldTwinRotation = twin.transform.rotation;
        }
    }

    public void setTwin(GameObject t)
    {
        twin = t;
        oldTwinPosition = twin.transform.position;
        oldTwinRotation = twin.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (twin == null)
        {
            Destroy(gameObject);
        }
        else
        {
            if (oldPosition != transform.position || oldRotation != transform.rotation)
            {
                Vector3 localPos = parent.transform.InverseTransformPoint(transform.position);
                twin.transform.localPosition = localPos;
                twin.transform.rotation = transform.rotation;
            }
            else if (oldTwinPosition != twin.transform.position || oldTwinRotation != twin.transform.rotation)
            {
                transform.localPosition = twin.transform.position;
                transform.rotation = twin.transform.rotation;
            }

            oldPosition = transform.position;
            oldRotation = transform.rotation;
            oldTwinPosition = twin.transform.position;
            oldTwinRotation = twin.transform.rotation;
        }
    }

    private void OnDestroy()
    {
        if (ApplicationModel.wimOpened && twin!=null)
        {
            Destroy(twin);
        }
        
    }

    public GameObject getTwin()
    {
        return twin;
    }
}

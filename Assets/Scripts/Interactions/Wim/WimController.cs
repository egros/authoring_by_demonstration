﻿using DitzelGames.FastIK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class WimController : MonoBehaviour
{
    private float scale;

    public GameObject realWorld;
    public GameObject wimPuppet;
    private GameObject arCreations;
    private FastIKLook lookController;

    public GameObject avatarPrefab;
    private GameObject avatar;

    private GameObject miniWorld;
    private GameObject miniArCreations;

    public SteamVR_Action_Boolean inputNav;
    private GameObject myCamera;
    private bool isDisplayed;

    private bool wimPuppetIsDisplayed;
    private WimTwinPuppetController[] twinPcontrollers;

    // Start is called before the first frame update
    void Start()
    {
        arCreations = GameObject.Find("ARCreations");
        myCamera = GameObject.Find("VRCamera");
        scale = 0.1f;
        isDisplayed = false;
        twinPcontrollers = wimPuppet.GetComponentsInChildren<WimTwinPuppetController>();

        lookController = wimPuppet.GetComponentInChildren<FastIKLook>();
        lookController.enabled = false;
        wimPuppet.SetActive(false);
        wimPuppetIsDisplayed = false;
    }

    public Vector3 getDelta()
    {
        return realWorld.transform.position;
    }

    public void displayWIM()
    {
        // Non Interactive Environment
        miniWorld = GameObject.Instantiate(realWorld);
        miniWorld.transform.localScale = new Vector3(scale, scale, scale);
        miniWorld.transform.SetParent(transform);
        miniWorld.transform.localPosition = new Vector3(0f, 0f, 0f);
        eraseInRealCopychildren(transform);

        // Interactive environment
        miniArCreations = GameObject.Instantiate(arCreations);
        miniArCreations.transform.localScale = new Vector3(scale, scale, scale);
        miniArCreations.transform.SetParent(transform);
        miniArCreations.transform.localPosition = new Vector3(0f, 0f, 0f);
        linkArObjects(arCreations.transform, miniArCreations.transform);

        // Avatar
        avatar = GameObject.Instantiate(avatarPrefab);
        avatar.transform.localScale = new Vector3(scale, scale, scale);
        avatar.transform.SetParent(miniWorld.transform);
        avatar.GetComponent<WimAvatarController>().setWim(gameObject);

        // WimPuppet
        if(ApplicationModel.recordingStatus=="On" && ApplicationModel.recordedTarget== "Puppet")
        {
            wimPuppet.SetActive(true);
            lookController.enabled = false;
            foreach(WimTwinPuppetController t in twinPcontrollers)
            {
                t.setWimReferent(miniWorld);
            }
            //wimPuppet.transform.localScale = puppetScale;
            wimPuppet.transform.SetParent(miniWorld.transform);
            wimPuppet.transform.position = GameObject.Find("WoodBody").transform.position;
            wimPuppetIsDisplayed = true;
            lookController.enabled = true;
            lookController.resetVariables();
            foreach (WimTwinPuppetController t in twinPcontrollers)
            {
                t.adjust();
            }
        }

        displayInFrontOfPlayer();
    }

    public void displayInFrontOfPlayer()
    {
        float angle = Mathf.PI * (myCamera.transform.rotation.eulerAngles.y / 180f);

        float cosY = Mathf.Cos(angle);
        float sinY = Mathf.Sin(angle);
        transform.position = new Vector3(myCamera.transform.position.x + sinY*0.5f, 0.5f, myCamera.transform.position.z + cosY*0.5f);
    }

    public void linkArObjects(Transform real, Transform twin)
    {
        int nbRealChildren = real.childCount;
        int nbTwinChildren = twin.childCount;
        if(nbRealChildren == nbTwinChildren)
        {
            for(int i=0; i<nbRealChildren; i++)
            {
                GameObject realChild = real.GetChild(i).gameObject;
                GameObject twinChild = twin.GetChild(i).gameObject;

                if (twinChild.GetComponent<EventIconController>() == null)
                {
                    if (twinChild.GetComponent<ArObject>() != null)
                    {
                        twinChild.GetComponent<ArObject>().enabled = false;
                        twinChild.GetComponent<MoveRecorder>().enabled = false;
                        twinChild.GetComponent<CollisionRecorder>().enabled = false;
                    }
                    // Add twinComponent
                    twinChild.AddComponent<TwinController>();
                    twinChild.GetComponent<TwinController>().setTwin(realChild);
                    linkArObjects(realChild.transform, twinChild.transform);
                }
                else
                {
                    Destroy(twinChild);
                }
                

            }
        }
    }

    public void eraseInRealCopychildren(Transform parent)
    {
        foreach(Transform child in parent)
        {
            if(child.gameObject.tag == "HideInWIM")
            {
                Destroy(child.gameObject);
            }
            else
            {
                if (child.GetComponent<Collider>() != null && child.name!= "WimWoodBody" && child.parent.name!= "WimWoodBody")
                {
                    Destroy(child.GetComponent<Collider>());
                }
                eraseInRealCopychildren(child);
            }
        }
    }

    public void eraseWIM()
    {
        isDisplayed = false;
        wimPuppet.transform.SetParent(transform);
        wimPuppet.SetActive(false);
        Destroy(miniWorld);
        Destroy(miniArCreations);
        
    }

    public void openClose()
    {
        if (isDisplayed)
        {
            ApplicationModel.wimOpened = false;
        }
        else
        {
            displayWIM();
            isDisplayed = true;
            ApplicationModel.wimOpened = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!ApplicationModel.wimOpened && isDisplayed)
        {
            eraseWIM();
        }
        if (inputNav.lastStateDown)
        {
            if (isDisplayed)
            {
                ApplicationModel.wimOpened = false;
            }
            else
            {
                displayWIM();
                isDisplayed = true;
                ApplicationModel.wimOpened = true;
            }
        }
        if (isDisplayed)
        {
            if(!wimPuppetIsDisplayed && ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "Puppet")
            {
                wimPuppet.SetActive(true);
                lookController.enabled = false; 
                foreach (WimTwinPuppetController t in twinPcontrollers)
                {
                    t.setWimReferent(miniWorld);
                }
                wimPuppet.transform.localPosition = GameObject.Find("WoodBody").transform.position;
                wimPuppetIsDisplayed = true;
                lookController.enabled = true;
                lookController.resetVariables();
            }
            else if(wimPuppetIsDisplayed && !(ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "Puppet"))
            {
                wimPuppet.SetActive(false);
            }
        }
    }
}

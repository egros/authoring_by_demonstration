﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedZoneEnter : RecordedEvent
{
    GameObject ghost;
    AreaColliderController zone;
    Vector3 insidePoint;
    Vector3 outsidePoint;
    bool isInside;
    float formerDistance;

    public void setEvent(GameObject enteringObject, AreaColliderController CollisionZone)
    {
        objectOfInterest = enteringObject;
        attachParent = CollisionZone.gameObject;
        attachPosition = CollisionZone.getAttachPosition();
        zone = CollisionZone;
        isAnimation = false;
        actionName = "EnterZone";
        insidePoint = zone.getInsidePoint();
        outsidePoint = zone.getOutsidePoint();

        IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
        objectOfInterestID = idRetriever.retrieveObjectID(objectOfInterest);
    }

    public override void launchAnimation()
    {
        // Cannot be an animation
    }

    public override void launchSimulation()
    {
        Vector3 ghostPosition = outsidePoint;
        if (ghost == null)
        {
            // Hands colliders
            GameObject leftHandCollider = GameObject.Find("HandColliderLeft(Clone)");
            GameObject rightHandCollider = GameObject.Find("HandColliderRight(Clone)");

            if (objectOfInterestID==2)
            {
                string woodName = "Events\\WoodGhost";
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(woodName);
                ghost = Instantiate((GameObject)assetToGet, outsidePoint, Quaternion.identity);
            }
            else if(objectOfInterestID==0)
            {
                string ghostName = "Events\\RightHandGhostVariant";
                Debug.Log("Charging : " + ghostName);
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(ghostName);
                if (assetToGet != null)
                {
                    ghost = Instantiate((GameObject)assetToGet, outsidePoint, Quaternion.identity);
                }
                else
                {
                    Debug.LogError("Cannot get the asset");
                }
            }
            else if(objectOfInterestID == 1)
            {
                string ghostName = "Events\\LeftHandGhostVariant";
                Debug.Log("Charging : " + ghostName);
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(ghostName);
                if (assetToGet != null)
                {
                    ghost = Instantiate((GameObject)assetToGet, outsidePoint, Quaternion.identity);
                }
                else
                {
                    Debug.LogError("Cannot get the asset");
                }
            }
            else
            {
                GhostMaker ghostMaker = ScriptableObject.CreateInstance<GhostMaker>();
                ghost = ghostMaker.makeGhost(objectOfInterest);
            }
        }
        else
        {
            ghost.SetActive(true);
        }
        if (objectOfInterest.name == "WoodBody")
        {
            ghostPosition = new Vector3(outsidePoint.x, 1f, outsidePoint.z);
        }
        ghost.transform.SetParent(eventIcon.transform);
        ghost.transform.position = ghostPosition;
        
        simulationOn = true;
        formerDistance = Vector3.Distance(ghost.transform.position, ghostPosition);
    }

    public override void animate()
    {
        if (simulationOn)
        {
            Vector3 ghostPosition = insidePoint;
            if(objectOfInterest.name == "WoodBody")
            {
                ghostPosition = new Vector3(insidePoint.x, 1f, insidePoint.z);
            }
            ghost.transform.position = Vector3.MoveTowards(ghost.transform.position, ghostPosition, 0.1f);

            float currentDistance = Vector3.Distance(ghost.transform.position, ghostPosition);
            isInside = zone.isInside(ghost.transform.position);
            if (isInside && currentDistance>=formerDistance)
            {
                stopSimulation();
            }
            formerDistance = currentDistance;
        }
        else
        {
            stopSimulation();
        }
    }

    public override void stopSimulation()
    {
        simulationOn = false;
        if (ghost != null)
        {
            ghost.SetActive(false);
        }
    }

    public override string ToString()
    {
        throw new System.NotImplementedException();
    }

    public override EventDescription getDescription()
    {
        // Hands colliders
        GameObject leftHandCollider = GameObject.Find("HandColliderLeft(Clone)");
        GameObject rightHandCollider = GameObject.Find("HandColliderRight(Clone)");

        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = objectOfInterestID;
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, collidedObj: zone.getID());

        return descr;
    }

}

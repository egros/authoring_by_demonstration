﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedLook : RecordedEvent
{
    //private GameObject lookedObject;
    private GameObject ghost;
    private int counter;
    

    public void setLook(GameObject looked)
    {
        actionName = "Look";
        objectOfInterest = looked;
        attachParent = objectOfInterest;
        attachPosition = new Vector3(0.1f, -0.1f, 0f);
        isAnimation = false;
    }

    public GameObject getLook()
    {
        return objectOfInterest;
    }

    public override void cancelModifications()
    {
        //Nothing;
    }

    public override string ToString()
    {
        return "Is looking at " + objectOfInterest.name;
    }

    public override void launchSimulation()
    {
        if (ghost == null)
        {
            string name = "Events\\LookGhost";
            UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
            ghost = Instantiate((GameObject)assetToGet, objectOfInterest.transform.position, Quaternion.identity);
            ghost.GetComponent<LookAim>().setTarget(objectOfInterest);
        }
        else
        {
            ghost.SetActive(true);
        }
        counter = 0;
        simulationOn = true;
    }

    public override void animate()
    {
        base.animate();
        if (simulationOn)
        {
            counter++;
            if (counter > 7)
            {
                ghost.SetActive(false);
                simulationOn = false;
            }
        }
    }

    public override void stopSimulation()
    {
        if (ghost != null)
        {
            ghost.SetActive(false);
            simulationOn = false;
        }
        
    }

    public override void launchAnimation()
    {
        Debug.Log("Can't be animation");
    }

    public override void setDetector()
    {
        LookMonitor monitor = objectOfInterest.GetComponent<LookMonitor>();
        if (monitor == null)
        {
            monitor = objectOfInterest.AddComponent<LookMonitor>();
        }
        EventIconController eventI = eventIcon.GetComponent<EventIconController>();
        if (eventI != null)
        {
            monitor.addLookToDetect(eventI);
        }
        else
        {
            Debug.Log("No event Icon to add");
        }

    }

    public override EventDescription getDescription()
    {
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = objectOfInterest.GetComponent<ArObject>().getID();
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI);

        return descr;
    }

}

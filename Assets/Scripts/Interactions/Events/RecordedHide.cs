﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedHide : RecordedEvent
{
    private int cpt;
    private GameObject arParent;

    public void setHideEvent(GameObject hiddenObject)
    {
        actionName = "Hide";
        objectOfInterest = hiddenObject;
        attachParent = hiddenObject;
        arParent = hiddenObject.transform.parent.gameObject;
        isAnimation = true;
    }

    public override void cancelModifications()
    {
        objectOfInterest.SetActive(true);
        objectOfInterest.GetComponent<ArObject>().setAttributesToDefault();
    }

    public override void CancelAnimationExecuted()
    {
        base.CancelAnimationExecuted();
        objectOfInterest.SetActive(true);
        eventIcon.transform.SetParent(objectOfInterest.transform);
    }

    public override void launchAnimation()
    {
        arParent = objectOfInterest.transform.parent.gameObject;
        eventIcon.transform.SetParent(arParent.transform);
        eventIcon.GetComponent<EventIconController>().trigerDone();
        objectOfInterest.SetActive(false);
        
    }

    public override void launchSimulation()
    {
        arParent = objectOfInterest.transform.parent.gameObject;
        //eventIcon.transform.SetParent(arParent.transform);
        //objectOfInterest.SetActive(false);
        objectOfInterest.GetComponent<ArObject>().setTransparency(false);
        cpt = 0;
        simulationOn = true;
    }

    public override void animate()
    {
        if (simulationOn)
        {
            if (cpt < 4)
            {
                cpt++;
            }
            else
            {
                //objectOfInterest.SetActive(true);
                //eventIcon.transform.SetParent(objectOfInterest.transform);
                ArObject ar = objectOfInterest.GetComponent<ArObject>();
                ar.setTransparency(ar.getIsVisibleAtStart());
                simulationOn = false;
            }
        }
    }

    public override void stopSimulation()
    {
        simulationOn = false;
        if (objectOfInterest != null)
        {
            objectOfInterest.SetActive(true);
        }
    }

    public override string ToString()
    {
        return "Hidde " + objectOfInterest.name;
    }

    public override EventDescription getDescription()
    {
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = objectOfInterest.GetComponent<ArObject>().getID();
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI);

        return descr;
    }
}

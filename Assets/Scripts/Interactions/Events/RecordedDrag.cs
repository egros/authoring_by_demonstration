﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class RecordedDrag : RecordedTrajectory
{
    private string hand;
    private float distance;

    private GameObject handGhost;
    private GameObject draggedGhost;
    private Vector3 draggedIniPos;

    //private Material ghostMaterial;
    //public void setElements(GameObject dragged, List<Vector3> traject, string holdingHand, Vector3 dragObInitialPoss)
    public void setElements(GameObject dragged, List<TransformReccord> traject, string holdingHand, Vector3 dragObInitialPoss)
    {
        //ghostMaterial= Resources.Load<Material>("Materials\\ghostMat");
        draggedIniPos = dragObInitialPoss;
        actionName = "Drag";
        weight = 5;
        objectOfInterest = dragged;
        attachParent = objectOfInterest;

        simplifyTrajectory(traject);

        hand = holdingHand;
        isAnimation = false;
        distance = Vector3.Distance(startPosition, endPosition);
    }

    public GameObject getDraggedObject()
    {
        return objectOfInterest;
    }

    public override bool handCheck()
    {
        if(objectOfInterest.transform.parent.gameObject.name == hand)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public string getHand()
    {
        return hand;
    }

    public Vector3 getStartPosition()
    {
        return startPosition;
    }

    public Vector3 getEndPosition()
    {
        return endPosition;
    }

    public float getDistance()
    {
        return distance;
    }

    public override string ToString()
    {
        return "Drag "+ objectOfInterest.name+" from " + startPosition.ToString() + " to " + endPosition.ToString();
    }

    public override void cancelModifications()
    {
        objectOfInterest.transform.position = startPosition;
        objectOfInterest.transform.rotation = rotations[0];
    }

    public override void launchSimulation()
    {
        simulationOn = true;
        nextPosition = 0;
        if (handGhost == null)
        {
            string name = "Events\\" + hand + "GhostClosed";
            UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
            handGhost = Instantiate((GameObject)assetToGet, trajectory[0], Quaternion.identity);
            handGhost.transform.SetParent(eventIcon.transform);
        }
        else
        {
            handGhost.SetActive(true);
            handGhost.transform.position = trajectory[0];

        }
        if (draggedGhost == null)
        {
            GhostMaker ghostMaker = ScriptableObject.CreateInstance<GhostMaker>();
            draggedGhost = ghostMaker.makeGhost(objectOfInterest);
            draggedGhost.transform.position = draggedIniPos;
            draggedGhost.transform.SetParent(handGhost.transform);
        }
        else
        {
            draggedGhost.SetActive(true);
        }
    }


    public override void animate()
    {
        //Debug.Log("Animate()");
        if (simulationOn)
        {
            if (followDirection)
            {
                if (nextPosition < 10)
                {
                    Vector3 targetPt = new Vector3(handGhost.transform.position.x + directionVector.x, handGhost.transform.position.y + directionVector.y, handGhost.transform.position.z + directionVector.z);
                    // source : https://gamedevbeginner.com/the-right-way-to-lerp-in-unity-with-examples/
                    float time = 0;
                    Vector3 startPosition = handGhost.transform.position;
                    float duration = 0.2f;
                    while (time < duration)
                    {
                        handGhost.transform.position = Vector3.Lerp(startPosition, targetPt, time / duration);
                        time += Time.deltaTime;
                    }
                    //handGhost.transform.position = Vector3.MoveTowards(handGhost.transform.position, targetPt, 0.2f);
                    nextPosition++;
                }
                else
                {
                    handGhost.SetActive(false);
                    draggedGhost.SetActive(false);
                    simulationOn = false;
                }
            }
            else
            {
                if (nextPosition < trajectory.Count)
                {
                    float step = 0.2f;
                    // source : https://gamedevbeginner.com/the-right-way-to-lerp-in-unity-with-examples/
                    float time = 0;
                    Vector3 startPosition = handGhost.transform.position;
                    float duration = 0.2f;
                    while (time < duration)
                    {
                        handGhost.transform.position = Vector3.Lerp(startPosition, trajectory[nextPosition], time / duration);
                        time += Time.deltaTime;
                    }
                    //handGhost.transform.position = Vector3.MoveTowards(handGhost.transform.position, trajectory[nextPosition], step);
                    nextPosition++;
                }
                else
                {
                    handGhost.SetActive(false);
                    draggedGhost.SetActive(false);
                    simulationOn = false;
                }
            }
        }
    }

    public override void stopSimulation()
    {
        if (draggedGhost != null)
        {
            draggedGhost.SetActive(false);
        }
        if (handGhost != null)
        {
            handGhost.SetActive(false);
        }
        simulationOn = false;
    }


    public override void launchAnimation()
    {
        Debug.Log("Can't be animation");
    }

    public override void setDetector()
    {
        GameObject.Find(hand).GetComponent<MovementMonitor>().addMonitoring(this, eventIcon.GetComponent<EventIconController>());
        if (objectOfInterest != null)
        {
            objectOfInterest.GetComponent<SimpleAttach>().addDragEvent(eventIcon.GetComponent<EventIconController>());
        }
    }

    public override EventDescription getDescription()
    {
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = objectOfInterest.GetComponent<ArObject>().getID();
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        string traject = translater.turnTabToString(translater.turnVectorListToTab(trajectory));
        string rota = translater.turnTabToString(translater.turnQuaternionListToTab(rotations));
        int handID = -1;
        if (hand == "RightHand")
        {
            handID = 0;
        }
        else
        {
            handID = 1;
        }
        bool followDir = followDirection;

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, holdingH: handID, traj: traject, rot: rota, follow: followDir);

        return descr;
    }
}

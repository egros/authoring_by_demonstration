﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RecordedEvent : ScriptableObject
{
    protected GameObject objectOfInterest;
    protected int objectOfInterestID;
    protected int weight;
    protected GameObject attachParent;
    protected Vector3 attachPosition;
    protected bool simulationOn;
    protected GameObject eventIcon;
    protected bool isAnimation;
    protected string actionName;
    protected bool detectorIsSet;
    protected bool animationExecuted;

    public virtual void setEventIcon(GameObject e)
    {
        eventIcon = e;
        detectorIsSet = false;
        animationExecuted = false;
    }

    public int getObjectOfInterestID()
    {
        return objectOfInterestID;
    }

    public abstract override string ToString();

    public abstract EventDescription getDescription();

    public virtual void cancelModifications(){}

    public abstract void launchSimulation();

    public abstract void stopSimulation();

    public abstract void launchAnimation();


    public virtual void animate(){}

    public int getWeight()
    {
        return weight;
    }

    public GameObject getAttachParent()
    {
        return attachParent;
    }

    public Vector3 getAttachPosition()
    {
        return attachPosition;
    }

    public bool getSimulationOn()
    {
        return simulationOn;
    }

    public string getActionName()
    {
        return actionName;
    }

    public bool getIsAnimation()
    {
        return isAnimation;
    }

    public bool getDetectorIsSet()
    {
        return detectorIsSet;
    }

    public virtual void CancelAnimationExecuted()
    {
        animationExecuted = false;
    }

    public virtual void setDetector(){}


    public GameObject getObjectOfInterest()
    {
        return objectOfInterest;
    }

    public virtual void inspect()
    {
        // Show event details
    }

    public GameObject getEventIcon()
    {
        return eventIcon;
    }
}

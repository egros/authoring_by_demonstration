﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class RecordedMovement : RecordedTrajectory
{
    private bool replaceMovedObject;
    private GameObject ghost;
    //private Material ghostMaterial;
    
    public void setTrajectory(List<TransformReccord> trajectories, GameObject whatIsMoved, bool toReplace)
    {
        
        //ghostMaterial = Resources.Load<Material>("Materials\\ghostMat");
        actionName = "Move";
        objectOfInterest = whatIsMoved;
        replaceMovedObject = toReplace;
        weight = 1;

        simplifyTrajectory(trajectories);

        if (objectOfInterest.name==ApplicationModel.dominantHand || objectOfInterest.name==ApplicationModel.nonDominantHand || objectOfInterest.name == "SphereRightHand" || objectOfInterest.name == "SphereLeftHand" || objectOfInterest.name=="WoodBody")
        {
            attachParent = null;
            attachPosition = startPosition;
            isAnimation = false;
        }
        else
        {
            isAnimation = true;
            attachParent = objectOfInterest;
        }
        animationExecuted = false;

        IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
        objectOfInterestID = idRetriever.retrieveObjectID(objectOfInterest);
    }


    public override string ToString()
    {
        return "Movement from " + startPosition.ToString() + " to " + endPosition.ToString();
    }

    public string getPositions()
    {
        string moves = "";
        foreach(Vector3 v in trajectory)
        {
            moves= moves +"\n" + v.ToString();
        }
        return moves;
    }

    public override void cancelModifications()
    {
        if (replaceMovedObject)
        {
            objectOfInterest.transform.position = startPosition;
            objectOfInterest.transform.rotation = rotations[0];
        }
    }

    public override void launchSimulation()
    {
        simulationOn = true;
        nextPosition = 1;
        if (ghost == null)
        {
            if (objectOfInterestID==0 ||objectOfInterestID==1)
            {
                // Create a ghost hand
                string name ="";
                if(objectOfInterest.name== ApplicationModel.dominantHand || objectOfInterest.name == "SphereRightHand")
                {
                    name = "Events\\" + ApplicationModel.dominantHand + "GhostVariant";
                }
                else
                {
                    name = "Events\\" + ApplicationModel.nonDominantHand + "GhostVariant";
                }
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                ghost = Instantiate((GameObject)assetToGet, trajectory[0], Quaternion.identity);
                ghost.transform.SetParent(eventIcon.transform);
            }
            else if (objectOfInterestID==2)
            {
                string woodName = "Events\\WoodGhost";
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(woodName);
                ghost = Instantiate((GameObject)assetToGet, trajectory[0], Quaternion.identity);
                ghost.transform.SetParent(eventIcon.transform);
            }
            else
            {
                GhostMaker ghostMaker = ScriptableObject.CreateInstance<GhostMaker>();
                ghost = ghostMaker.makeGhost(objectOfInterest);
            }
        }
        else
        {
            ghost.SetActive(true);
            
        }

        GhostArranger ghostArranger = ScriptableObject.CreateInstance<GhostArranger>();
        ghostArranger.suppressUnwantedChildren(ghost);
        //place the object at the first position of the trajectory
        ghost.transform.SetParent(eventIcon.transform);
        ghost.transform.position = trajectory[0];
        ghost.transform.rotation = rotations[0];
    }

    public override void animate()
    {
        if (simulationOn)
        {
            if (!followDirection)
            {
                if (nextPosition < trajectory.Count || nextPosition < rotations.Count)
                {
                    if (nextPosition < trajectory.Count)
                    {
                        // source : https://gamedevbeginner.com/the-right-way-to-lerp-in-unity-with-examples/
                        float time = 0;
                        Vector3 startPosition = ghost.transform.position;
                        float duration = 0.2f;
                        while (time < duration)
                        {
                            ghost.transform.position = Vector3.Lerp(startPosition, trajectory[nextPosition], time / duration);
                            time += Time.deltaTime;
                        }
                    }
                    if (nextPosition < rotations.Count)
                    {
                        ghost.transform.rotation = rotations[nextPosition];
                    }
                    nextPosition++;
                }
                else
                {
                    if (ghost == objectOfInterest)
                    {
                        objectOfInterest.transform.position = trajectory[0];
                    }
                    else
                    {
                        ghost.SetActive(false);
                    }
                    simulationOn = false;
                }
            }
            else
            {
                if (nextPosition < 10)
                {
                    Vector3 targetPt = new Vector3(ghost.transform.position.x + directionVector.x, ghost.transform.position.y + directionVector.y, ghost.transform.position.z + directionVector.z);
                    // source : https://gamedevbeginner.com/the-right-way-to-lerp-in-unity-with-examples/
                    float time = 0;
                    Vector3 startPosition = ghost.transform.position;
                    float duration = 0.2f;
                    while (time < duration)
                    {
                        ghost.transform.position = Vector3.Lerp(startPosition, targetPt, time / duration);
                        time += Time.deltaTime;
                    }
                    nextPosition++;
                }
                else
                {
                    simulationOn = false;
                    ghost.SetActive(false);
                }
                
            }
            

        }
    }

    public override void stopSimulation()
    {
        if (ghost != null)
        {
            ghost.SetActive(false);
        }
        simulationOn = false;
    }


    public override void launchAnimation()
    {
        if (isAnimation && animationExecuted==false)
        {
            if (followDirection)
            {
                objectOfInterest.GetComponent<ArObject>().startFollowingDirection(directionVector);
                animationExecuted = true;
            }
            else
            {
                objectOfInterest.transform.position = trajectory[0];
                objectOfInterest.GetComponent<ArObject>().followTrajectory(trajectory, rotations, eventIcon.GetComponent<EventIconController>());
                animationExecuted = true;
            }
            
        }
    }


    public override void setDetector()
    {
        if(objectOfInterestID==0)
        {
            GameObject.Find("RightHand").GetComponent<MovementMonitor>().addMonitoring(this, eventIcon.GetComponent<EventIconController>());
        }
        else if(objectOfInterestID==1)
        {
            GameObject.Find("LeftHand").GetComponent<MovementMonitor>().addMonitoring(this, eventIcon.GetComponent<EventIconController>());
        }
        else if(objectOfInterestID==2)
        {
            GameObject.Find("VRCamera").GetComponent<MovementMonitor>().addMonitoring(this, eventIcon.GetComponent<EventIconController>());
        }

    }
    public override EventDescription getDescription()
    {
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = objectOfInterestID;

        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        string traject = translater.turnTabToString(translater.turnVectorListToTab(trajectory));
        string rotat = translater.turnTabToString(translater.turnQuaternionListToTab(rotations));
        bool folowDir = followDirection;

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, traj: traject, rot: rotat, follow: folowDir);

        return descr;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class RecordedDrop : RecordedEvent
{
    //private GameObject droppedObject;
    private GameObject hand;
    private GameObject ghost;
    private GameObject openGhost;
    private GameObject droppedGhost;
    //private Material ghostMaterial;
    private Vector3 handAttachPosition;
    private int step;


    public void setDroppedObject(GameObject drop, Vector3 position, GameObject dropHand)
    {
        actionName = "Drop";
        weight = 3;
        objectOfInterest = drop;
        //attachParent = droppedObject;
        attachPosition = position;
        handAttachPosition = new Vector3(attachPosition.x, attachPosition.y + 0.1f, attachPosition.z);
        hand = dropHand;
        isAnimation = false;
        //ghostMaterial = Resources.Load<Material>("Materials\\ghostMat");
        if (objectOfInterest == null)
        {
            objectOfInterestID = -1;
        }
        else
        {
            IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
            objectOfInterestID = idRetriever.retrieveObjectID(objectOfInterest);
        }
    }

    public GameObject getHand()
    {
        return hand;
    }



    public GameObject getDroppedObject()
    {
        return objectOfInterest;
    }

    public override string ToString()
    {
        if (objectOfInterest != null)
        {
            return "Drop " + objectOfInterest.name;
        }
        else
        {
            return "Drop nothing";
        }
        
    }

    public override void launchSimulation()
    {
        string grabbedname = "";
        if (objectOfInterest == null)
        {
            grabbedname = "nothing";
        }
        else
        {
            grabbedname = objectOfInterest.name;
        }
        simulationOn = true;
        if (ghost == null)
        {
            string name = "Events\\" + hand.name + "GhostClosed";
            UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
            ghost = Instantiate((GameObject)assetToGet, handAttachPosition, Quaternion.identity);
            //ghost.transform.Translate(new Vector3(0.081157f, 0.008636f, 0.038124f));
            ghost.transform.SetParent(eventIcon.transform);
        }
        else
        {
            ghost.SetActive(true);
            ghost.transform.position = handAttachPosition;
            //ghost.transform.Translate(new Vector3(0.081157f, 0.008636f, 0.038124f));

        }
        //place the object at the first position of the trajectory
        if (objectOfInterest == null)
        {
            ghost.transform.position = attachPosition;
        }
        else
        {
            if (droppedGhost == null)
            {
                GhostMaker ghostMaker = ScriptableObject.CreateInstance<GhostMaker>();
                droppedGhost = ghostMaker.makeGhost(objectOfInterest);
                droppedGhost.transform.position = attachPosition;
                droppedGhost.transform.SetParent(eventIcon.transform);
            }
            else
            {
                droppedGhost.SetActive(true);
            }
            

        }
        step = 0;
    }

    public void openHand()
    {
        if (openGhost == null)
        {
            string name = "Events\\" + hand.name + "GhostVariant";
            UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
            openGhost = Instantiate((GameObject)assetToGet, handAttachPosition, Quaternion.identity);
            openGhost.transform.SetParent(eventIcon.transform);
            
        }
        else
        {
            openGhost.SetActive(true);
        }
        if (objectOfInterest == null)
        {
            openGhost.transform.position = attachPosition;
        }
        ghost.SetActive(false);
    }

    public override void animate()
    {
        if (simulationOn)
        {
            step++;
            if (step == 5)
            {
                openHand();
            }
            else if (step == 10)
            {
                openGhost.SetActive(false);
                if (droppedGhost != null)
                {
                    droppedGhost.SetActive(false);
                }
                simulationOn = false;
                step = 0;
            }
        }
    }

    public override void stopSimulation()
    {
        if (ghost != null)
        {
            ghost.SetActive(false);
        }
        if (droppedGhost != null)
        {
            droppedGhost.SetActive(false);
        }
        if (openGhost != null)
        {
            openGhost.SetActive(false);
        }
        simulationOn = false;
    }

    public override void launchAnimation()
    {
        Debug.Log("Can't be animation");
    }

    public override void setDetector()
    {
        if (objectOfInterest != null)
        {
            objectOfInterest.GetComponent<SimpleAttach>().addDropEvent(eventIcon.GetComponent<EventIconController>());
        }
        
    }
    public override EventDescription getDescription()
    {
        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = -1;
        if (objectOfInterest != null)
        {
            targ = objectOfInterest.GetComponent<ArObject>().getID();
        }
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        string holdingHand = hand.name;
        int handID = -1;
        if (holdingHand == "RightHand")
        {
            handID = 0;
        }
        else
        {
            handID = 1;
        }
        EventDescription descr;
        if (targ == -1)
        {
            Vector3 v = attachPosition;
            float[] posTab = new float[3] { v.x, v.y, v.z };
            float[][] pos = new float[1][] { posTab };
            string posString = translater.turnTabToString(pos);
            descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, holdingH: handID, traj: posString);
        }
        else
        {
            descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, holdingH: handID);
        }

        return descr;
    }

}

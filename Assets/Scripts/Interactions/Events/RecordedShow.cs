﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedShow : RecordedEvent
{
    private int cpt;
    public void setShow(GameObject showedObject)
    {
        actionName = "Show";
        objectOfInterest = showedObject;
        attachParent = showedObject;
        isAnimation = true;
        IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
        objectOfInterestID = idRetriever.retrieveObjectID(objectOfInterest);
    }

    public override void launchAnimation()
    {
        objectOfInterest.SetActive(true);
        eventIcon.transform.SetParent(objectOfInterest.transform);
        eventIcon.GetComponent<EventIconController>().trigerDone();
    }

    public override void launchSimulation()
    {
        objectOfInterest.GetComponent<ArObject>().setTransparency(true);
        cpt = 0;
        simulationOn = true;
    }

    public override void animate()
    {
        ArObject ar = objectOfInterest.GetComponent<ArObject>();
        if (simulationOn)
        {
            if (cpt < 4)
            {
                cpt++;
            }
            else
            {
                ar.setTransparency(ar.getIsVisibleAtStart());
                simulationOn = false;
            }
        }
    }

    public override void stopSimulation()
    {
        simulationOn = false;
    }

    public override string ToString()
    {
        return "Show " + objectOfInterest.name;
    }

    public override void cancelModifications()
    {
        objectOfInterest.GetComponent<ArObject>().setAttributesToDefault();
    }

    public override EventDescription getDescription()
    {
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = objectOfInterest.GetComponent<ArObject>().getID();
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI);

        return descr;
    }
}

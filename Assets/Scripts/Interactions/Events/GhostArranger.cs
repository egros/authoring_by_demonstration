﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostArranger : ScriptableObject
{
    public void suppressUnwantedChildren(GameObject ghost)
    {
        Destroy(ghost.GetComponent<ArObject>());
        foreach (Transform t in ghost.transform)
        {
            AttributeController attr = t.gameObject.GetComponent<AttributeController>();
            EventIconController eventIcon = t.gameObject.GetComponent<EventIconController>();
            if (attr != null || eventIcon != null)
            {
                Destroy(t.gameObject);
            }
            else
            {
                suppressUnwantedChildren(t.gameObject);
            }
        }
    }
}

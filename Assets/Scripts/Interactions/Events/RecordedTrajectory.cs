﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RecordedTrajectory : RecordedEvent
{
    protected List<Vector3> trajectory;
    protected List<Quaternion> rotations;
    protected Vector3 startPosition;
    protected Vector3 endPosition;

    protected MoveRenderer moveInspector;
    protected bool followDirection;
    protected Vector3 directionVector;

    protected int nextPosition;
    protected int stepToCheck;
    protected float valueToSuplant;
    protected DateTime lastValidated;

    public override void setEventIcon(GameObject e)
    {
        base.setEventIcon(e);
        moveInspector = eventIcon.gameObject.GetComponentInChildren<MoveRenderer>();
        stepToCheck = 0;
        valueToSuplant = 0f;
        lastValidated = DateTime.Now;
    }

    protected void simplifyTrajectory(List<TransformReccord> trajectories)
    {
        List<Vector3> traject = new List<Vector3>();
        trajectory = new List<Vector3>();
        rotations = new List<Quaternion>();

        foreach (TransformReccord t in trajectories)
        {
            traject.Add(t.getPosition());
            rotations.Add(t.getRotation());
        }

        //Simplify Polyline
        ElSimplificator simplificator = objectOfInterest.AddComponent<ElSimplificator>();
        simplificator.listTraj = traject;
        IList<Vector3> simplifiedTraj = simplificator.Simplify();
        foreach (Vector3 v in simplifiedTraj)
        {
            trajectory.Add(v);
        }
        startPosition = trajectory[0];
        endPosition = trajectory[trajectory.Count - 1];

        // Always a trajectory at first
        followDirection = false;
    }

    public List<Vector3> getTrajectory()
    {
        return trajectory;
    }

    public List<Quaternion> getRotations()
    {
        return rotations;
    }

    public void deletePosition(List<Vector3> newTrajectory)
    {
        trajectory = newTrajectory;
    }

    public void modifyTrajectory(int idx, Vector3 newPosition)
    {
        trajectory[idx] = newPosition;
        if (idx == trajectory.Count - 1)
        {
            endPosition = newPosition;
        }
        if (followDirection)
        {
            updateDirection();
        }
    }

    public void updateDirection()
    {
        Vector3 p1 = moveInspector.endPoint.transform.position;
        Vector3 p2 = objectOfInterest.transform.position;

        directionVector = new Vector3((p1.x - p2.x), (p1.y - p2.y), (p1.z - p2.z));
    }

    public void setFollowDirection(bool follow)
    {
        followDirection = follow;
    }

    public bool getFollowDirection()
    {
        return followDirection;
    }

    public void switchToDirection()
    {
        followDirection = true;
        updateDirection();
    }

    public void deleteInspector()
    {
        moveInspector.destroyElements();
        Destroy(moveInspector);
    }

    public override void inspect()
    {
        moveInspector.hideShowDetails();
    }

    public virtual bool handCheck()
    {
        return false;
    }

    public bool check(Vector3 pos)
    {
        if((actionName=="Drag" && handCheck()) || actionName=="Move")
        {
            if (stepToCheck == 0)
            {
                lastValidated = DateTime.Now;
                valueToSuplant = 0f;
            }
            System.TimeSpan diff = DateTime.Now.Subtract(lastValidated);
            if (diff.TotalSeconds > 2)
            {
                stepToCheck = 0;
                moveInspector.resetValidated();
            }
            if (!followDirection)
            {
                Vector3 comparisonPoint = trajectory[stepToCheck + 1];
                Vector3 targetPos = pos;
                float marginDistance = 0.15f;
                if (objectOfInterest.name == "WoodBody")
                {
                    comparisonPoint = new Vector3(comparisonPoint.x, 0f, comparisonPoint.z);
                    targetPos = new Vector3(pos.x, 0f, pos.z);
                    marginDistance = 0.3f;
                }
                if (Vector3.Distance(comparisonPoint, targetPos) < marginDistance)
                {
                    if (stepToCheck < trajectory.Count - 2)
                    {
                        moveInspector.validateMarble(stepToCheck);
                    }
                    stepToCheck++;
                    lastValidated = DateTime.Now;
                    if (stepToCheck == trajectory.Count - 1)
                    {
                        stepToCheck = 0;
                        return true;
                    }
                }

            }
            else
            {
                // Si vecteur par rapport au point de départ correct et croissant
                Vector3 directionTaken = new Vector3(pos.x - trajectory[0].x, pos.y - trajectory[0].y, pos.z - trajectory[0].z);
                float dX = directionTaken.x / directionVector.x;
                float dY = directionTaken.y / directionVector.y;
                float dZ = directionTaken.z / directionVector.z;

                float diffXY = Mathf.Abs(dX - dY);
                float diffXZ = Mathf.Abs(dX - dZ);
                float diffYZ = Mathf.Abs(dZ - dY);

                if(diffXY<0.05f && diffXZ<0.05f && diffYZ < 0.05f && dX>valueToSuplant)
                {
                    stepToCheck++;
                    lastValidated = DateTime.Now;
                    valueToSuplant = dX;
                    if (stepToCheck == 5)
                    {
                        return true;
                    }
                }
            }
            
            
        }
        return false;
    }

    public void resetValidated()
    {
        moveInspector.resetValidated();
    }

    public Vector3 getDirection()
    {
        return directionVector;
    }

}

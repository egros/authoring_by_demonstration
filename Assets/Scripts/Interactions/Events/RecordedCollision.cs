﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class RecordedCollision : RecordedEvent
{
    private GameObject collidingObject;
    private GameObject collidedObject;
    private GameObject ghost;
    private Material ghostMaterial;
    private int counter;

    public void setCollision(Collider collide, GameObject collidingOb)
    {
        ghostMaterial = Resources.Load<Material>("Materials\\ghostMat");
        actionName = "Collision";
        weight = 2;
        collidedObject = collide.gameObject;
        collidingObject = collidingOb;

        objectOfInterest = collidingObject;
        if (collidingOb.name == ApplicationModel.dominantHand || collidingOb.name == ApplicationModel.nonDominantHand || collidingOb.name== "SphereRightHand" || collidingOb.name== "SphereLeftHand" || collidingOb.name == "WoodBody")
        {
            attachParent = collide.gameObject;
            isAnimation = false;
        }
        else
        {
            attachParent = collidingOb;
            isAnimation = true;
        }

        IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
        objectOfInterestID = idRetriever.retrieveObjectID(objectOfInterest);
        //attachPosition = new Vector3(- 0.1f, 0f, 0f);
    }

    public void setCollision(GameObject collided, GameObject collidingOb)
    {
        ghostMaterial = Resources.Load<Material>("Materials\\ghostMat");
        actionName = "Collision";
        weight = 2;
        collidedObject = collided;
        collidingObject = collidingOb;

        objectOfInterest = collidingObject;
        if (collidingOb.name == ApplicationModel.dominantHand || collidingOb.name == ApplicationModel.nonDominantHand || collidingOb.name == "SphereRightHand" || collidingOb.name == "SphereLeftHand" || collidingOb.name == "WoodBody")
        {
            attachParent = collided;
            isAnimation = false;
        }
        else
        {
            attachParent = collidingOb;
            isAnimation = true;
        }
    }


    public GameObject getColllidedObject()
    {
        return collidedObject;
    }

    public GameObject getCollindingObject()
    {
        return collidingObject;
    }

    public override string ToString()
    {
        return collidingObject.name + " is colliding " + collidedObject.name;
    }

    public override void launchSimulation()
    {
        if(ghost==null)
        {
            if (objectOfInterestID==0 || objectOfInterestID==1)
            {
                string name = "";
                if(objectOfInterestID==0)
                {
                    name = "Events\\" + ApplicationModel.dominantHand + "GhostVariant";
                }
                else
                {
                    name = "Events\\" + ApplicationModel.nonDominantHand + "GhostVariant";
                }
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
                ghost = Instantiate((GameObject)assetToGet, collidedObject.transform.position, Quaternion.identity);
                ghost.transform.SetParent(eventIcon.transform);
                
            }
            else if (objectOfInterestID==2)
            {
                string woodName = "Events\\WoodGhost";
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(woodName);
                ghost = Instantiate((GameObject)assetToGet, collidedObject.transform.position, Quaternion.identity);
                ghost.transform.SetParent(eventIcon.transform);
            }
            else
            {
                GhostMaker ghostMaker = ScriptableObject.CreateInstance<GhostMaker>();
                ghost = ghostMaker.makeGhost(collidingObject);
                ghost.transform.position = collidedObject.transform.position;
                
            }
        }
        else
        {
            ghost.SetActive(true);
        }
        ghost.transform.position = new Vector3(collidedObject.transform.position.x, collidedObject.transform.position.y, collidedObject.transform.position.z - 0.4f);
        simulationOn = true;
        counter = 0;
    }

    public override void animate()
    {
        if (counter < 5)
        {
            counter++;
            ghost.transform.position = new Vector3(ghost.transform.position.x, ghost.transform.position.y, ghost.transform.position.z + 0.08f);
        }
        else
        {
            simulationOn = false;
            ghost.SetActive(false);
        }
    }

    public override void stopSimulation()
    {
        simulationOn = false;
        if (ghost != null)
        {
            ghost.SetActive(false);
        }
    }

    public override void launchAnimation()
    {
        if (isAnimation)
        {
            Debug.Log("Launch Animation");
        }
    }


    public override void setDetector()
    {
        if (objectOfInterestID==0 || objectOfInterestID==1)
        {
            GameObject handCollider = null;
            string handName = "";
            if (objectOfInterestID==0)
            {
                handCollider = GameObject.Find("HandColliderRight(Clone)");
                handName = "RightHand";
                
            }
            else
            {
                handCollider = GameObject.Find("HandColliderLeft(Clone)");
                handName = "LeftHand";
            }
            if (handCollider != null)
            {
                HandCollisionController handCollision = handCollider.GetComponent<HandCollisionController>();
                if (handCollision == null)
                {
                    handCollision = handCollider.AddComponent<HandCollisionController>();
                    handCollision.setHand(handName);
                }
                if (eventIcon.GetComponent<EventIconController>() != null)
                {
                    handCollision.addCollisionToDetect(eventIcon.GetComponent<EventIconController>());
                }
                else
                {
                    Debug.Log("EventIcon has no event icon controller");
                }

            }

        }
        else if (objectOfInterestID==2)
        {
            // Find body collider
            CollisionRecorder colRecorder = GameObject.Find("BodyCollider").GetComponent<CollisionRecorder>();
            if (colRecorder != null)
            {
                colRecorder.addCollisionToDetect(eventIcon.GetComponent<EventIconController>());
                detectorIsSet = true;
            }
        }
        else
        {
            // Find the object's collision recorder
            CollisionRecorder colisionRecord = collidingObject.GetComponent<CollisionRecorder>();
            if (colisionRecord == null)
            {
                colisionRecord = collidingObject.AddComponent<CollisionRecorder>();
            }
            if (colisionRecord != null)
            {
                colisionRecord.addCollisionToDetect(eventIcon.GetComponent<EventIconController>());
                detectorIsSet = true;
            }
            else
            {
                Debug.Log("Can't find detector");
            }
        }
    }

    public override EventDescription getDescription()
    {
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        GameObject collided = collidedObject;
        GameObject colliding = collidingObject;
        IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
        int collidedTarget = idRetriever.retrieveObjectID(collided);
        int collidingTarget = idRetriever.retrieveObjectID(colliding);

        EventDescription descr = new EventDescription(mType, identifiant, collidingTarget, anim, previous, nextI, collidedObj: collidedTarget);

        return descr;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedColor : RecordedEvent
{
    private MeshRenderer mesh;
    private Color color;
    private int step;
    private HelperCoroutine helperCorout;
    private ArObject arComponent;

    public void setColorEvent(GameObject target, Color newColor)
    {
        actionName = "Color";
        objectOfInterest = target;
        attachParent = objectOfInterest;
        arComponent = objectOfInterest.GetComponent<ArObject>();
        mesh = target.GetComponent<MeshRenderer>();
        color = newColor;
        isAnimation = true;
        helperCorout = GameObject.FindObjectOfType<HelperCoroutine>();
    }

    public Color getColor()
    {
        return color;
    }

    public override void launchAnimation()
    {
        mesh.material.color = color;
        if (ApplicationModel.mode == "Test")
        {
            helperCorout.StartCoroutine(TriggerCoroutine());
        }
        else
        {
            helperCorout.StopAllCoroutines();
            mesh.material.color = arComponent.getColorAtStart();
        }
        
    }

    IEnumerator TriggerCoroutine()
    {
        yield return new WaitForSeconds(1);
        eventIcon.GetComponent<EventIconController>().trigerDone();
        helperCorout.StopCoroutine(TriggerCoroutine());
    }


    public override void animate()
    {
        if (simulationOn)
        {
            if(step < 5)
            {
                mesh.material.color = color;
                step++;
            }
            else
            {
                mesh.material.color = arComponent.getColorAtStart();
                simulationOn = false;
            }
        }
    }

    public override void launchSimulation()
    {
        mesh.material.color = color;
        step = 0;
        simulationOn = true;
    }

    public override void stopSimulation()
    {
        if (mesh != null)
        {
            mesh.material.color = arComponent.getColorAtStart();
        }
    }

    public override string ToString()
    {
        return "Color object in " + color.ToString();
    }

    public override void CancelAnimationExecuted()
    {
        base.CancelAnimationExecuted();
        objectOfInterest.GetComponent<ArObject>().defaultColor();
    }

    public override EventDescription getDescription()
    {
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = objectOfInterest.GetComponent<ArObject>().getID();
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();
        float[] colorTab = new float[] { color.r, color.g, color.b, color.a };

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, coloration:colorTab);

        return descr;
    }

}

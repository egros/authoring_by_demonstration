﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedZoneExit : RecordedEvent
{
    GameObject ghost;
    AreaColliderController zone;
    Vector3 insidePoint;
    Vector3 outsidePoint;
    bool isInside;
    float formerDistance;

    public void setEvent(GameObject exitingObject, AreaColliderController collisionZone)
    {
        objectOfInterest = exitingObject;
        attachParent = collisionZone.gameObject;
        attachPosition = collisionZone.getAttachPosition();
        isAnimation = false;
        actionName = "ExitZone";
        zone = collisionZone;
        insidePoint = zone.getInsidePoint();
        outsidePoint = zone.getOutsidePoint();

        IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
        objectOfInterestID = idRetriever.retrieveObjectID(objectOfInterest);
    }

    public override void launchAnimation()
    {
        // Cannot be animation
    }

    public override void launchSimulation()
    {
        Vector3 ghostPosition = insidePoint;
        if (ghost == null)
        {
            // Hands colliders
            GameObject leftHandCollider = GameObject.Find("HandColliderLeft(Clone)");
            GameObject rightHandCollider = GameObject.Find("HandColliderRight(Clone)");
            
            if (objectOfInterest.name == "WoodBody")
            {
                string woodName = "Events\\WoodGhost";
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(woodName);
                ghost = Instantiate((GameObject)assetToGet, outsidePoint, Quaternion.identity);
            }
            else if (objectOfInterest.transform.IsChildOf(leftHandCollider.transform) || objectOfInterest.name == "SphereLeftHand")
            {
                string ghostName = "Events\\" + ApplicationModel.nonDominantHand + "GhostVariant";
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(ghostName);
                ghost = Instantiate((GameObject)assetToGet, outsidePoint, Quaternion.identity);
            }
            else if (objectOfInterest.transform.IsChildOf(rightHandCollider.transform) || objectOfInterest.name == "SphereRightHand")
            {
                string ghostName = "Events\\" + ApplicationModel.dominantHand + "GhostVariant";
                UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(ghostName);
                ghost = Instantiate((GameObject)assetToGet, outsidePoint, Quaternion.identity);
            }
            else
            {
                GhostMaker ghostMaker = ScriptableObject.CreateInstance<GhostMaker>();
                ghost = ghostMaker.makeGhost(objectOfInterest);
            }
        }
        else
        {
            ghost.SetActive(true);
        }
        if (objectOfInterest.name == "WoodBody")
        {
            ghostPosition = new Vector3(insidePoint.x, 1f, insidePoint.z);
        }
        ghost.transform.SetParent(eventIcon.transform);
        ghost.transform.position = ghostPosition;
        simulationOn = true;
        formerDistance = Vector3.Distance(ghost.transform.position, outsidePoint);
    }

    public override void animate()
    {
        if (simulationOn)
        {
            Vector3 ghostPosition = outsidePoint;
            if (objectOfInterest.name == "WoodBody")
            {
                ghostPosition = new Vector3(outsidePoint.x, 1f, outsidePoint.z);
            }
            ghost.transform.position = Vector3.MoveTowards(ghost.transform.position, ghostPosition, 0.1f);

            float currentDistance = Vector3.Distance(ghost.transform.position, ghostPosition);
            isInside = zone.isInside(ghost.transform.position); //.PointInPolygon(ghost.transform.position.x, ghost.transform.position.z);
            if (!isInside && currentDistance >= formerDistance)
            {
                stopSimulation();
            }
            formerDistance = currentDistance;
        }
        else
        {
            ghost.SetActive(false);
        }
    }

    public override void stopSimulation()
    {
        simulationOn = false;
        if (ghost != null)
        {
            ghost.SetActive(false);
        }
    }

    public override string ToString()
    {
        throw new System.NotImplementedException();
    }

    public override EventDescription getDescription()
    {
        // Hands colliders
        GameObject leftHandCollider = GameObject.Find("HandColliderLeft(Clone)");
        GameObject rightHandCollider = GameObject.Find("HandColliderRight(Clone)");

        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = -1;
        if (objectOfInterest.name == "WoodBody")
        {
            targ = 2;
        }
        else if (objectOfInterest.transform.IsChildOf(leftHandCollider.transform) || objectOfInterest.name == "SphereLeftHand")
        {
            targ = 1;
        }
        else if (objectOfInterest.transform.IsChildOf(rightHandCollider.transform) || objectOfInterest.name == "SphereRightHand")
        {
            targ = 0;
        }
        else
        {
            targ = objectOfInterest.GetComponent<ArObject>().getID();
        }
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        EventDescription descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, collidedObj: zone.getID());

        return descr;
    }
}

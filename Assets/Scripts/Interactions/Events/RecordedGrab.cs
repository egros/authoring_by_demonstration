﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedGrab : RecordedEvent
{
    //private GameObject grabbedObject;
    private GameObject grabbingHand;
    private GameObject ghost;
    private GameObject closeGhost;
    public int step;
    
    public void Start()
    {
        objectOfInterest = null;
        
    }

    public void setGrabbedObject(GameObject grabbed, Vector3 position, GameObject graabingH)
    {
        actionName = "Grab";
        weight = 4;
        objectOfInterest = grabbed;
        attachParent = objectOfInterest;
        attachPosition = position;
        grabbingHand = graabingH;
        isAnimation = false;
        if (objectOfInterest == null)
        {
            objectOfInterestID = -1;
        }
        else
        {
            IDRetriever idRetriever = ScriptableObject.CreateInstance<IDRetriever>();
            objectOfInterestID = idRetriever.retrieveObjectID(objectOfInterest);
        }
    }

    public GameObject getGrabbedObject()
    {
        return objectOfInterest;
    }

    public GameObject getGrabbingHand()
    {
        return grabbingHand;
    }

    public override string ToString()
    {
        string name = "";
        if (objectOfInterest != null)
        {
            name = objectOfInterest.name;
        }
        return "Grab " + name;
    }

    public override void launchSimulation()
    {
        string grabbedname = "";
        if (objectOfInterest == null)
        {
            grabbedname = "nothing";
        }
        else
        {
            grabbedname = objectOfInterest.name;
        }
        simulationOn = true;
        if (ghost == null)
        {
            string name = "Events\\" + grabbingHand.name + "GhostVariant";
            UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
            ghost = Instantiate((GameObject)assetToGet, attachPosition, Quaternion.identity);
            ghost.transform.SetParent(eventIcon.transform);
        }
        else
        {
            ghost.SetActive(true);

        }
        //place the object at the first position of the trajectory
        if (objectOfInterest == null)
        {
            ghost.transform.position = attachPosition;
        }
        else
        {
            Vector3 v = new Vector3(objectOfInterest.transform.position.x+attachPosition.x, objectOfInterest.transform.position.y + attachPosition.y, objectOfInterest.transform.position.z - attachPosition.z);
            ghost.transform.position = v;
        }
        step = 0;

    }

    public void closeHand()
    {
        if (closeGhost == null)
        {
            string name = "Events\\" + grabbingHand.name + "GhostClosed";
            Debug.Log("Searching for " + name);
            UnityEngine.Object assetToGet = Resources.Load<UnityEngine.Object>(name);
            closeGhost = Instantiate((GameObject)assetToGet, ghost.transform.position, Quaternion.identity);
            closeGhost.transform.Translate(new Vector3(0.081157f, 0.008636f, 0.038124f));
            closeGhost.transform.SetParent(eventIcon.transform);
        }
        else
        {
            closeGhost.SetActive(true);
            closeGhost.transform.position = ghost.transform.position;
            closeGhost.transform.Translate(new Vector3(0.081157f, 0.008636f, 0.038124f));
        }
        
        ghost.SetActive(false);
    }

    public override void animate()
    {
        if (simulationOn)
        {
            step++;
            if (step == 5)
            {
                closeHand();
            }
            else if (step == 10)
            {
                closeGhost.SetActive(false);
                simulationOn = false;
                step = 0;
            }
        }
    }

    public override void stopSimulation()
    {
        if (ghost != null)
        {
            ghost.SetActive(false);
        }
        if (closeGhost != null)
        {
            closeGhost.SetActive(false);
        }        simulationOn = false;
    }


    public override void launchAnimation()
    {
        Debug.Log("Can't be animation");
    }

    public override void setDetector()
    {
        if (objectOfInterest != null)
        {
            SimpleAttach attach = objectOfInterest.GetComponent<SimpleAttach>();
            if (eventIcon != null)
            {
                EventIconController icon = eventIcon.GetComponent<EventIconController>();
                if (attach != null)
                {
                    attach.addGrabEvent(icon);
                }
                else
                {
                    Debug.Log("Attach is null");
                }
            }
            else
            {
                Debug.Log("Icon is null");
            }
        }
        
    }
    public override EventDescription getDescription()
    {
        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        EventIconController iconContr = eventIcon.GetComponent<EventIconController>();
        string mType = actionName;
        int identifiant = iconContr.getID();
        int targ = -1;
        if (objectOfInterest != null)
        {
            targ = objectOfInterest.GetComponent<ArObject>().getID();
        }
        int[] anim = iconContr.getAnimationsID();
        int previous = iconContr.getPreviousID();
        int nextI = iconContr.getNextID();

        string holdingHand = grabbingHand.name;
        int hand = -1;
        if (holdingHand == "RightHand")
        {
            hand = 0;
        }
        else
        {
            hand = 1;
        }
        EventDescription descr;
        if (targ==-1)
        {
            Vector3 v = attachPosition;
            float[] posTab = new float[3] { v.x, v.y, v.z };
            float[][] pos = new float[1][] { posTab };
            string posString = translater.turnTabToString(pos);
            descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, holdingH: hand, traj: posString);
        }
        else
        {
            descr = new EventDescription(mType, identifiant, targ, anim, previous, nextI, holdingH: hand);
        }

        return descr;
    }
}

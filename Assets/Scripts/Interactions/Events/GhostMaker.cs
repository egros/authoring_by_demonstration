﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class GhostMaker : ScriptableObject
{
    private Material ghostMaterial;

    public GameObject makeGhost(GameObject initialObject)
    {
        ghostMaterial = Resources.Load<Material>("Materials\\ghostMat");
        GameObject ghost = GameObject.Instantiate(initialObject);
        //Destroy(initialObject.GetComponent<ArObject>());

        if (ghost.GetComponent<MeshRenderer>() != null)
        {
            ghost.GetComponent<MeshRenderer>().material = ghostMaterial;
        }
        ghost.GetComponent<SimpleAttach>().enabled = false;
        ghost.GetComponent<Interactable>().enabled = false;
        ghost.GetComponent<MoveRecorder>().enabled = false;
        ghost.GetComponent<CollisionRecorder>().enabled = false;
        ghost.GetComponent<ArObject>().enabled = false;
        Destroy(ghost.GetComponent<CircleDispatcher>());
        Destroy(ghost.GetComponent<Rigidbody>());
        Destroy(ghost.GetComponent<Interactable>());
        Destroy(ghost.GetComponent<Collider>());
        Destroy(ghost.GetComponent<ArObject>());

        GhostArranger ghostArranger = ScriptableObject.CreateInstance<GhostArranger>();
        ghostArranger.suppressUnwantedChildren(ghost);

        return ghost;
    }
}

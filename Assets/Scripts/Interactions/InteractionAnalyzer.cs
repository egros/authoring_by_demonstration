﻿using DitzelGames.FastIK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class InteractionAnalyzer : MonoBehaviour
{
    public GameObject eventIcon;
    public GameObject moveIcon;
    public GameObject collisionIcon;
    public GameObject grabIcon;
    public GameObject dropIcon;
    public GameObject dragIcon;
    public GameObject lookIcon;
    public GameObject showIcon;
    public GameObject hideIcon;
    public GameObject colorIcon;
    public GameObject enterZIcon;
    public GameObject exitZIcon;

    private GameObject myCamera;
    private GameObject puppet;

    private InteractionRecorder recording;
    private bool reccordindDecountFinished;
    private string oldStatus;
    private GameObject arCreations;

    private InteractionControllerRecorder dominantHandRecorder;
    private InteractionControllerRecorder nonDominantRecorder;
    private InteractionBothControllers bothHandsRecorder;
    private InteractionPuppetRecorder puppetRecorder;
    private FastIKLook lookControl;
    private InteractionArObjectsRecorder arObjectsRecorder;

    private Valve.VR.SteamVR_Action_Boolean input;
    public RecorderCanvasController recorderCanvas;
    private Hand nonDominantHand;

    public GameObject previousIcon;


    private GameObject stopButton;

    // Start is called before the first frame update
    void Start()
    {
        input = SteamVR_Input.GetBooleanAction("Record");
        oldStatus = ApplicationModel.recordingStatus;

        GameObject spine = GameObject.Find("spine.006");
        if (spine != null)
        {
            lookControl = spine.GetComponent<FastIKLook>();
        }
        dominantHandRecorder = GameObject.Find(ApplicationModel.dominantHand).GetComponent<InteractionControllerRecorder>();
        nonDominantRecorder = GameObject.Find(ApplicationModel.nonDominantHand).GetComponent<InteractionControllerRecorder>();
        bothHandsRecorder = GameObject.FindObjectOfType<InteractionBothControllers>();
        puppetRecorder = GameObject.Find("WoodBody").GetComponent<InteractionPuppetRecorder>();
        arObjectsRecorder = GameObject.Find("ARCreations").GetComponent<InteractionArObjectsRecorder>();

        stopButton = GameObject.Find("StopRecording");
        stopButton.transform.SetParent(GameObject.Find(ApplicationModel.nonDominantHand).transform);
        stopButton.transform.localPosition = new Vector3(0.3f, -0.06f, -0.449f) ;
        stopButton.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        stopButton.transform.Rotate(new Vector3(60f, 0f,0f));
        stopButton.SetActive(false);

        puppet = GameObject.Find("WoodBody");
        lookControl.enabled = false;
        puppet.SetActive(false);
        myCamera = GameObject.Find("VRCamera");
        if (myCamera == null)
        {
            myCamera = GameObject.Find("FallbackObjects");
        }

        arCreations = GameObject.Find("ARCreations");
        nonDominantHand = GameObject.Find(ApplicationModel.nonDominantHand).GetComponent<Hand>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKeyUp("r"))
        {
            if (ApplicationModel.recordingStatus != "On")
            {
                ApplicationModel.recordingStatus = "On";
                ApplicationModel.recordedTarget = ApplicationModel.dominantHand;
            }
            else
            {
                ApplicationModel.recordingStatus = "Analyze";
            }
        }*/
        if (input.lastStateDown && ApplicationModel.recordingStatus=="On" && reccordindDecountFinished)
        {
            ApplicationModel.recordingStatus = "Analyze";
            
        }
        if(oldStatus != ApplicationModel.recordingStatus)
        {
            oldStatus = ApplicationModel.recordingStatus;
            
            if (ApplicationModel.recordingStatus == "On")
            {
                previousIcon = null;
                reccordindDecountFinished = false;
                recorderCanvas.startRecording();
                StartCoroutine(MyCoroutine());
                
            }
            else if(ApplicationModel.recordingStatus == "Analyze")
            {
                ControllerButtonHints.HideTextHint(nonDominantHand, input);
                stopButton.SetActive(false);
                stopRecording();
                recorderCanvas.stopReccording();
            }
            else
            {
                // Reset recording
                // Cancel all modifications during recording
                recording = null;
            }
        }
    }

    IEnumerator MyCoroutine()
    {
        yield return new WaitForSeconds(ApplicationModel.reccordingWait);
        stopButton.SetActive(true);
        startRecording();
        reccordindDecountFinished = true;
    }

    public void startRecording()
    {
        ControllerButtonHints.ShowTextHint(nonDominantHand, input, "Press here to stop recording", true);
        //Start un InteractionRecorder
        if (ApplicationModel.recordedTarget == ApplicationModel.dominantHand)
        {
            recording = dominantHandRecorder;
        }
        else if(ApplicationModel.recordedTarget == ApplicationModel.nonDominantHand)
        {
            recording = nonDominantRecorder;
        }
        else if(ApplicationModel.recordedTarget == "BothHands")
        {
            recording = bothHandsRecorder;
        }
        else if(ApplicationModel.recordedTarget == "Puppet")
        {
            puppet.SetActive(true);
            lookControl.enabled = false;
            float yRotation = myCamera.transform.rotation.eulerAngles.y - puppet.transform.rotation.eulerAngles.y;
            double angle = Math.PI * (myCamera.transform.rotation.eulerAngles.y / 180f);
            float cosY = (float)Math.Cos(angle);
            float sinY = (float)Math.Sin(angle);
            puppet.transform.position = new Vector3(myCamera.transform.position.x + sinY / 1.4f, puppet.transform.position.y, myCamera.transform.position.z + cosY / 1.4f);
            puppet.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            puppet.transform.Rotate(new Vector3(0f, myCamera.transform.rotation.eulerAngles.y - 180f, 0f));
            lookControl.enabled = true;
            lookControl.resetVariables();
            recording = puppetRecorder;
        }
        else if(ApplicationModel.recordedTarget == "ArObjects")
        {
            recording = arObjectsRecorder;
        }
        if (recording != null)
        {
            recording.startRecording();
        }
        else
        {
            Debug.Log("Couldn't find recording of " + ApplicationModel.recordedTarget);
        }
        
    }

    public void createEventIcon(RecordedEvent record)
    {
        GameObject instantiatedEventIcon;
        GameObject icon = eventIcon;

        // Get the right icon
        string actionName = record.getActionName();
        if (actionName == "Move")
        {
            icon = moveIcon;
        }
        else if (actionName == "Grab")
        {
            icon = grabIcon;
        }
        else if (actionName == "Drop")
        {
            icon = dropIcon;
        }
        else if (actionName == "Collision")
        {
            icon = collisionIcon;
        }
        else if (actionName == "Drag")
        {
            icon = dragIcon;
        }
        else if (actionName == "Look")
        {
            icon = lookIcon;
        }
        else if (actionName == "Hide")
        {
            icon = hideIcon;
        }
        else if (actionName == "Show")
        {
            icon = showIcon;
        }
        else if (actionName == "Color")
        {
            icon = colorIcon;
        }
        else if (actionName == "EnterZone")
        {
            icon = enterZIcon;
        }
        else if (actionName == "ExitZone")
        {
            icon = exitZIcon;
        }

        if (record.getAttachParent() != null)
        {
            Vector3 position = record.getAttachParent().transform.position;
            if (actionName == "EnterZone" || actionName == "ExitZone")
            {
                position = record.getAttachPosition();
            }
            instantiatedEventIcon = Instantiate(icon, position, Quaternion.identity);

            instantiatedEventIcon.transform.SetParent(record.getAttachParent().transform);
            CircleDispatcher dispatch = record.getAttachParent().GetComponent<CircleDispatcher>();
            if (dispatch != null)
            {
                dispatch.updateDispatch();
            }
            if (actionName == "EnterZone" || actionName == "ExitZone")
            {
                record.getAttachParent().GetComponent<AreaColliderController>().dispatchEventIcons();
            }
        }
        else
        {
            instantiatedEventIcon = Instantiate(icon, record.getAttachPosition(), Quaternion.identity);
            instantiatedEventIcon.transform.SetParent(arCreations.transform);
        }
        instantiatedEventIcon.GetComponent<EventIconController>().setRecordedEvent(record, previousIcon, ApplicationModel.iconIdx);
        
        ApplicationModel.iconIdx++;
        if (previousIcon != null)
        {
            previousIcon.GetComponent<EventIconController>().setNextEventIcon(instantiatedEventIcon, false);
        }

        previousIcon = instantiatedEventIcon;

    }

    public void stopRecording()
    {
        // Get recorded events
        recording.stopRecording();
        puppet.SetActive(false);
        //Analyze
        /*int mainEvent=0;
        int idx = 0;
        foreach(RecordedEvent e in recordedEvents)
        {
            if (recordedEvents[mainEvent].getWeight() <= e.getWeight())
            {
                mainEvent = idx;
            }
            idx++;
        }

        //Display results
        GameObject previousEventIcon=null;
        GameObject instantiatedEventIcon;
        foreach (RecordedEvent finalEvent in recordedEvents)
        {
            GameObject icon=eventIcon;
            string actionName = finalEvent.getActionName();
            if (actionName == "Move")
            {
                icon = moveIcon;
            }
            else if (actionName == "Grab")
            {
                icon = grabIcon;
            }
            else if (actionName == "Drop")
            {
                icon = dropIcon;
            }
            else if (actionName == "Collision")
            {
                icon = collisionIcon;
            }
            else if(actionName=="Drag"){
                icon = dragIcon;
            }
            else if (actionName == "Look")
            {
                icon = lookIcon;
            }
            else if (actionName == "Hide")
            {
                icon = hideIcon;
            }
            else if (actionName == "Show")
            {
                icon = showIcon;
            }
            else if(actionName == "Color")
            {
                icon = colorIcon;
            }
            else if (actionName == "EnterZone")
            {
                icon = enterZIcon;
            }
            else if (actionName == "ExitZone")
            {
                icon = exitZIcon;
            }

            if (finalEvent.getAttachParent() != null)
            {
                Vector3 position = finalEvent.getAttachParent().transform.position;
                if(actionName == "EnterZone" || actionName == "ExitZone")
                {
                    position = finalEvent.getAttachPosition();
                }
                instantiatedEventIcon = Instantiate(icon, position, Quaternion.identity);
                
                instantiatedEventIcon.transform.SetParent(finalEvent.getAttachParent().transform);
                CircleDispatcher dispatch = finalEvent.getAttachParent().GetComponent<CircleDispatcher>();
                if (dispatch != null)
                {
                    dispatch.updateDispatch();
                }
                if(actionName=="EnterZone" || actionName == "ExitZone")
                {
                    finalEvent.getAttachParent().GetComponent<ZoneColliderController>().dispatchEventIcons();
                }
            }
            else
            {
                instantiatedEventIcon = Instantiate(icon, finalEvent.getAttachPosition(), Quaternion.identity);
                instantiatedEventIcon.transform.SetParent(arCreations.transform);
            }
            instantiatedEventIcon.GetComponent<EventIconController>().setRecordedEvent(finalEvent, previousEventIcon, ApplicationModel.iconIdx);
            ApplicationModel.iconIdx++;
            if (previousEventIcon != null)
            {
                previousEventIcon.GetComponent<EventIconController>().setNextEventIcon(instantiatedEventIcon, false);
            }
            previousEventIcon = instantiatedEventIcon;
        }*/
        

        ApplicationModel.recordingStatus = "Off";
        ApplicationModel.recordedTarget = "";
        /*int i = recordedEvents.Count - 1;
        while (i >= 0)
        {
            recordedEvents[i].cancelModifications();
            i = i - 1;
        }*/
    }

}

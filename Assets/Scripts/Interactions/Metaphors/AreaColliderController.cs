﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AreaColliderController : MonoBehaviour
{
    protected int id;
    protected Dictionary<int, RecordedZoneEnter> enterEvents;
    protected Dictionary<int, RecordedZoneExit> exitEvents;
    protected InteractionAnalyzer iconMaker;
    public abstract Vector3 getOutsidePoint();

    public abstract Vector3 getInsidePoint();

    public abstract Vector3 getAttachPosition();

    public abstract bool isInside(Vector3 position);

    public int getID()
    {
        return id;
    }

    public abstract void dispatchEventIcons();

    public abstract EventDescription getDescription();

    public abstract void addEnterEvent(RecordedZoneEnter enter);

    public abstract void addExitEvent(RecordedZoneExit exit);
}

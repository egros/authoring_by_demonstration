﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Zone3DColliderController : AreaColliderController
{
    public GameObject marble1;
    public GameObject marble2;
    public GameObject marble3;
    //public GameObject centerMarble;

    public TubeRenderer tube1;
    public TubeRenderer tube2;
    public TubeRenderer tube3;
    public TubeRenderer tube4;
    public TubeRenderer tube5;

    public BoxCollider colliderBox;

    //private SceneHandler laserControler;
    //private SteamVR_Action_Boolean input;
    private CubeCalculator cubeCalculator;

    private Vector3[] vertex;
    private Vector3 originPoint;

    // Hands Colliders
    private GameObject leftHandCollider;
    private GameObject rightHandCollider;

    // Start is called before the first frame update
    void Start()
    {
        //laserControler = GameObject.FindObjectOfType<SceneHandler>();
        //input = SteamVR_Input.GetBooleanAction("RightGrab");
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();

        cubeCalculator = ScriptableObject.CreateInstance<CubeCalculator>();
        originPoint = marble1.transform.position;
        //vertex = cubeCalculator.localPointsFromAnchors(marble2.transform.localPosition, marble3.transform.localPosition);
        if (vertex == null)
        {
            vertex = new Vector3[]
        {
            new Vector3(0,0,0),
            new Vector3(0,10,0),
            new Vector3(10,10,0),
            new Vector3(10,0,0),
            new Vector3(0,0,10),
            new Vector3(0,10,10),
            new Vector3(10,10,10),
            new Vector3(10,0,10)
        };
            id = ApplicationModel.iconIdx;
            ApplicationModel.iconIdx++;
        }

        if (enterEvents == null)
        {
            enterEvents = new Dictionary<int, RecordedZoneEnter>();
        }
        if (exitEvents == null)
        {
            exitEvents = new Dictionary<int, RecordedZoneExit>();
        }

        // Hands colliders
        leftHandCollider = GameObject.Find("HandColliderLeft(Clone)");
        rightHandCollider = GameObject.Find("HandColliderRight(Clone)");
    }

    public void setAllVertex(Vector3[] vertexList)
    {
        if (cubeCalculator == null)
        {
            cubeCalculator = ScriptableObject.CreateInstance<CubeCalculator>();
        }

        marble1.transform.position = vertexList[0];
        marble2.transform.localPosition = vertexList[2];
        marble3.transform.localPosition = vertexList[3];

        vertex = new Vector3[8];
        vertex[0] = Vector3.zero;
        for(int i = 1; i<8; i++)
        {
            vertex[i] = vertexList[i];
        }
        setTubes();
        updateCollider();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 relativPosM2 = marble1.transform.InverseTransformPoint(marble2.transform.position);
        Vector3 relativPosM3 = marble1.transform.InverseTransformPoint(marble3.transform.position);
        if(marble1.transform.position != originPoint)
        {
            // Local positions of the other vertex should be the same
            originPoint = marble1.transform.position;
            
            if (marble1.transform.GetComponentInParent<Zone3DColliderController>() != null)
            {
                marble1.transform.localPosition = Vector3.zero;
            }
            transform.position = originPoint;
            updateCollider();
        }
        else if(relativPosM2 != vertex[2])
        {
            Vector2 vC = new Vector2(vertex[2].x, vertex[2].z);
            Vector2 vG = new Vector2(vertex[6].x, vertex[6].z);
            float distBG = Vector2.Distance(vC, vG);
            /*bool trigoDir = cubeCalculator.getTrigoDirection(vertex[2], vertex[6]);
            Vector3 pointG = cubeCalculator.recalculateGfromC(relativPosM2, distBG, trigoDir);*/
            PolarCoordinate pC = cubeCalculator.getPolarCoordinate(vertex[2].x, vertex[2].z);
            PolarCoordinate pG = cubeCalculator.getPolarCoordinate(vertex[6].x, vertex[6].z);

            bool cond1 = (((pG.alpha >= 0 && pC.alpha >= 0) || (pG.alpha <= 0 && pC.alpha <= 0)) && pG.alpha < pC.alpha);
            bool cond2 = ((pC.alpha > 0 && pG.alpha < 0) && vC.x > 0) || ((pC.alpha < 0 && pG.alpha > 0) && vC.x < 0);
            if (cond1 || cond2)
            {
                distBG = -distBG;
            }
            Vector2 v = cubeCalculator.getPointG(relativPosM2.x, relativPosM2.z, distBG);
            marble3.transform.localPosition = new Vector3(v.x, vertex[2].y, v.y);
            //marble3.transform.localPosition = new Vector3(marble2.transform.localPosition.x + dx, marble2.transform.localPosition.y, marble2.transform.localPosition.z + dz);
            updateVertex();
            updateCollider();
        }
        else if(relativPosM3 != vertex[6])
        {
            Vector3 pointC = cubeCalculator.recalculateCFromG(relativPosM2, relativPosM3);
            marble2.transform.localPosition = pointC;
            updateVertex();
            updateCollider();
        }
    }

    public void updateVertex()
    {
        Vector3 relativPosM2 = marble1.transform.InverseTransformPoint(marble2.transform.position);
        Vector3 relativPosM3 = marble1.transform.InverseTransformPoint(marble3.transform.position);
        vertex = cubeCalculator.localPointsFromAnchors(relativPosM2, relativPosM3);
        setTubes();
    }
    
    public void updateCollider()
    {
        Vector3 center = cubeCalculator.getCenter(vertex[2], vertex[6]);
        //colliderBox.center = center;
        colliderBox.center = Vector3.zero;
        colliderBox.transform.localPosition = center;
        //centerMarble.transform.localPosition = center;
        float sx = Vector3.Distance(vertex[2], vertex[1]);
        float sy = Vector3.Distance(vertex[2], vertex[3]);
        float sz = Vector3.Distance(vertex[2], vertex[6]);
        colliderBox.size = new Vector3(sx, sy, sz);

        //Rotation around Y axis
        colliderBox.transform.rotation = new Quaternion(0, 0, 0, 0);

        float dist = Vector2.Distance(Vector2.zero, new Vector2(vertex[2].x, vertex[2].z));
        float cosTheta = vertex[2].x / dist;
        float theta = Mathf.Acos(cosTheta) * (180/Mathf.PI);
        if (vertex[2].z > 0)
        {
            theta = -theta;
        }
        Vector3 thetaAngle = new Vector3(0, theta, 0);
        colliderBox.transform.Rotate(thetaAngle);
        
    }

    public void setTubes()
    {
        Vector3[] t1Points = new Vector3[] { vertex[0], vertex[1], vertex[2], vertex[3], vertex[0], vertex[4] };
        tube1.points = t1Points;

        Vector3[] t2Points = new Vector3[] { vertex[6], vertex[7], vertex[4], vertex[5], vertex[6], vertex[2] };
        tube2.points = t2Points;

        Vector3[] t3Points = new Vector3[] { vertex[1], vertex[5]};
        tube3.points = t3Points;

        Vector3[] t4Points = new Vector3[] { vertex[3], vertex[7]};
        tube4.points = t4Points;

        tube5.points = new Vector3[] { };
        //updateTube5();
    }

    public void updateTube5()
    {
        Vector3 v = marble1.transform.InverseTransformPoint(marble2.transform.position);
        Vector3 p1 = Vector3.zero;
        Vector3 p2 = Vector3.zero;
        Vector3 p3 = v;
        float x1 = v.x - 20f;
        float x2 = v.x + 20f;
        if (v.x == 0)
        {
            p1 = new Vector3(x1, v.y, v.z);
            p2 = new Vector3(x2, v.y, v.z);
        }
        else if (v.z == 0)
        {
            p1 = new Vector3(v.x, v.y, v.z - 20f);
            p2 = new Vector3(v.x, v.y, v.z + 20f);
        }
        else
        {
            LineEquation eq = cubeCalculator.perpendicularEquation(v);
            p1 = new Vector3(x1, v.y, eq.a*x1 + eq.b);
            p2 = new Vector3(x2, v.y, eq.a * x2 + eq.b);
            p3 = new Vector3(v.x, v.y, eq.a * v.x + eq.b);
        }
        
        tube5.points = new Vector3[] { };
    }

    public void enterDetected(Collider other)
    {
        if (ApplicationModel.recordingStatus == "On")
        {
            recordTriggerInteraction(true, other);
        }
        if (ApplicationModel.mode == "Test")
        {
            triggerOnTestMode(true, other);
        }

    }

    public void exitDetected(Collider other)
    {
        if (ApplicationModel.recordingStatus == "On")
        {
            recordTriggerInteraction(false, other);
        }
        if (ApplicationModel.mode == "Test")
        {
            triggerOnTestMode(false, other);
        }
    }

    public void triggerOnTestMode(bool entering, Collider other)
    {
        bool cond1 = other.GetComponent<ArObject>() != null;
        bool cond3 = other.transform.IsChildOf(leftHandCollider.transform);
        bool cond4 = other.transform.IsChildOf(rightHandCollider.transform);
        bool cond5 = name == "BodyCollider";

        int objID = -1;

        if (cond1)
        {
            objID = other.GetComponent<ArObject>().getID();
            
        }
        else if (cond3)
        {
            objID = 1;
        }
        else if (cond4)
        {
            objID = 0;
        }
        else if (cond5)
        {
            objID = 2;
        }
        if (entering && enterEvents.ContainsKey(objID) && enterEvents[objID] != null)
        {
            enterEvents[objID].getEventIcon().GetComponent<EventIconController>().trigerDone();
        }
        if (!entering && exitEvents.ContainsKey(objID) && exitEvents[objID] != null)
        {
            exitEvents[objID].getEventIcon().GetComponent<EventIconController>().trigerDone();
        }

    }

    public void recordTriggerInteraction(bool entering, Collider other)
    {
        string name = other.gameObject.name;
        string target = ApplicationModel.recordedTarget;

        bool cond1 = (target == "ArObjects" && other.GetComponent<ArObject>() != null);
        bool cond2 = (target == "Puppet" && (name == "WoodBody" || name == "SphereRightHand" || name == "SphereLeftHand"));
        bool cond3 = (target == "LeftHand" && other.transform.IsChildOf(leftHandCollider.transform));
        bool cond4 = (target == "RightHand" && other.transform.IsChildOf(rightHandCollider.transform));
        bool cond5 = (target == "BothHands" && (name == "HandColliderRight(Clone)" || name == "HandColliderLeft(Clone)"));

        int dicIdx = -1;
        if (cond1)
        {
            dicIdx = other.GetComponent<ArObject>().getID();
        }
        else if (cond2)
        {
            dicIdx = 2;
        }
        else if (cond3)
        {
            dicIdx = 1;
        }
        else if (cond4)
        {
            dicIdx = 0;
        }

        if (cond1 || cond2 || cond3 || cond4 || cond5)
        {
            if (entering)
            {
                // create RecordedZoneEnter
                RecordedZoneEnter enterEv = ScriptableObject.CreateInstance<RecordedZoneEnter>();
                enterEv.setEvent(other.gameObject, this);
                if (!enterEvents.ContainsKey(dicIdx))
                {
                    enterEvents.Add(dicIdx, enterEv);
                    iconMaker.createEventIcon(enterEv);
                }
                else if (enterEvents.ContainsKey(dicIdx) && enterEvents[dicIdx] == null)
                {
                    enterEvents[dicIdx] = enterEv;
                    iconMaker.createEventIcon(enterEv);
                }
                
            }
            else
            {
                // create RecordedZoneExit
                RecordedZoneExit exitEv = ScriptableObject.CreateInstance<RecordedZoneExit>();
                exitEv.setEvent(other.gameObject, this);
                if (!exitEvents.ContainsKey(dicIdx))
                {
                    exitEvents.Add(dicIdx, exitEv);
                    iconMaker.createEventIcon(exitEv);
                }
                else if (exitEvents.ContainsKey(dicIdx) && exitEvents[dicIdx] == null)
                {
                    exitEvents[dicIdx] = exitEv;
                    iconMaker.createEventIcon(exitEv);
                }
                
            }
        }
    }

    public override Vector3 getOutsidePoint()
    {
        float maxX = Mathf.Max(vertex[1].x, vertex[2].x, vertex[5].x, vertex[6].x);
        float maxZ = Mathf.Max(vertex[1].z, vertex[2].z, vertex[5].z, vertex[6].z);
        float y = (vertex[0].y + vertex[2].y) / 2;
        Vector3 v = new Vector3(maxX+5, y, maxZ+5);
        return marble1.transform.TransformPoint(v);
    }

    public override Vector3 getInsidePoint()
    {
        Vector3 center = cubeCalculator.getCenter(vertex[2], vertex[6]);
        return marble1.transform.TransformPoint(center);
    }

    public override Vector3 getAttachPosition()
    {
        Vector3 center =  cubeCalculator.getCenter(vertex[2], vertex[6]);
        center = marble1.transform.TransformPoint(center);
        float y = Mathf.Min(marble1.transform.TransformPoint(vertex[0]).y, marble1.transform.TransformPoint(vertex[1]).y, marble1.transform.TransformPoint(vertex[2]).y, marble1.transform.TransformPoint(vertex[3]).y);
        Vector3 v = new Vector3(center.x, y + 0.05f, center.z);
        return v;
    }

    public override bool isInside(Vector3 position)
    {
        float maxX = Mathf.Max(vertex[1].x, vertex[2].x, vertex[5].x, vertex[6].x);
        float maxZ = Mathf.Max(vertex[1].z, vertex[2].z, vertex[5].z, vertex[6].z);
        float maxY = Mathf.Max(vertex[0].y, vertex[1].y, vertex[2].y, vertex[3].y);

        float minX = Mathf.Min(vertex[1].x, vertex[2].x, vertex[5].x, vertex[6].x);
        float minZ = Mathf.Min(vertex[1].z, vertex[2].z, vertex[5].z, vertex[6].z);
        float minY = Mathf.Min(vertex[0].y, vertex[1].y, vertex[2].y, vertex[3].y);

        Vector3 p = marble1.transform.InverseTransformPoint(position);
        if(p.x>minX && p.x<maxX && p.y>minY && p.y<maxY && p.z>minZ && p.z < maxZ)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override void dispatchEventIcons()
    {
        EventIconController[] eventIcons = GetComponentsInChildren<EventIconController>();
        Vector3 attachPos = getAttachPosition();
        float y = attachPos.y + 0.5f;
        float x = attachPos.x;
        float z = attachPos.z;
        foreach (EventIconController e in eventIcons)
        {
            e.gameObject.transform.position = new Vector3(x, y, z);
            y += 0.1f;
        }
    }

    public override EventDescription getDescription()
    {
        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        List<Vector3> descrPoints = new List<Vector3>();
        descrPoints.Add(marble1.transform.position);
        for(int i = 1; i<vertex.Length; i++)
        {
            descrPoints.Add(vertex[i]);
        }
        string trajectory = translater.turnTabToString(translater.turnVectorListToTab(descrPoints));
        EventDescription descr = new EventDescription("Zone3DCollider", id, -1, new int[] { }, traj: trajectory);
        return descr;
    }

    public override void addEnterEvent(RecordedZoneEnter enter)
    {
        if (enterEvents == null)
        {
            enterEvents = new Dictionary<int, RecordedZoneEnter>();
        }
        enterEvents.Add(enter.getObjectOfInterestID(), enter);
    }

    public override void addExitEvent(RecordedZoneExit exit)
    {
        if (exitEvents == null)
        {
            exitEvents = new Dictionary<int, RecordedZoneExit>();
        }
        exitEvents.Add(exit.getObjectOfInterestID(), exit);
    }
}

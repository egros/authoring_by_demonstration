﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.Extras;

public class ZoneColliderController : AreaColliderController
{
    public GameObject canvas;
    private GameObject vrCam;
    //private int id;

    // Laser interactions
    private SteamVR_LaserPointer laserPointer;
    private SteamVR_Action_Boolean grabInput;
    private SceneHandler sceneHandler;


    // Tube Renderer
    private List<Vector3> tubePoints;
    private TubeRenderer tubeRenderer;
    private Vector3 startPoint;

    private bool isClosed;
    //private bool playerIsInside;
    private int idx;
    private float yHeight;
    //private EventIconController eventIcon;

    // Objects to check for recording
    GameObject rightHand;
    GameObject leftHand;
    GameObject puppetBody;
    bool puppetIsInside;
    List<int> objectsInside;
    Dictionary<int, ArObject> dictObjects;
    //InteractionArObjectsRecorder objectsRecorder;
    //InteractionPuppetRecorder puppetRecorder;
    

    // Events
    
    List<ArObject> objectsToWatch;

    // Detection
    bool playerIsInside;
    Dictionary<int, bool> previousObjectsState;



    // Start is called before the first frame update
    void Start()
    {
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();

        laserPointer = GameObject.Find("RightHand").GetComponentInChildren<SteamVR_LaserPointer>();
        grabInput = SteamVR_Input.GetBooleanAction("GrabGrip");
        sceneHandler = GameObject.FindObjectOfType<SceneHandler>();

        gameObject.layer = 10;
        vrCam = GameObject.Find("VRCamera");
        canvas.transform.SetParent(vrCam.transform);
        canvas.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
        canvas.transform.localPosition = new Vector3(0f, -0.5f, 1.5f);
        tubeRenderer = GetComponentInChildren<TubeRenderer>();
        //eventIcon = GetComponentInChildren<EventIconController>();
        
        yHeight = 0.05f;

        if (tubePoints == null)
        {
            tubePoints = new List<Vector3>();
            idx = 0;
        }
        else
        {
            canvas.SetActive(false);
        }
        tubeRenderer.points = tubePoints.ToArray();

        // For recording
        rightHand = GameObject.Find("RightHand");
        leftHand = GameObject.Find("LeftHand");
        puppetBody = GameObject.Find("WoodBody");
        puppetIsInside = false;
        objectsInside = new List<int>();
        if (dictObjects == null)
        {
            dictObjects = new Dictionary<int, ArObject>();
        }        
        //objectsRecorder = GameObject.FindObjectOfType<InteractionArObjectsRecorder>();
        //puppetRecorder = GameObject.FindObjectOfType<InteractionPuppetRecorder>();
        if (enterEvents == null)
        {
            enterEvents = new Dictionary<int, RecordedZoneEnter>();
        }
        if (exitEvents == null)
        {
            exitEvents = new Dictionary<int, RecordedZoneExit>();
        }
        if (objectsToWatch == null)
        {
            objectsToWatch = new List<ArObject>();
        }
        // For detection
        playerIsInside = false;
        previousObjectsState = new Dictionary<int, bool>();
    }

    public void setAllPoints(List<Vector3> allPoints, Vector3 initPos, int idEv)
    {
        startPoint = initPos;
        transform.position = startPoint;
        isClosed = true;
        if (tubeRenderer == null)
        {
            tubeRenderer = GetComponentInChildren<TubeRenderer>();
        }
        tubePoints = allPoints;
        tubeRenderer.points = tubePoints.ToArray();
        id = idEv;
    }


    public void addPoint(Vector3 point)
    {
        if(idx == 0)
        {
            canvas.SetActive(false);
            startPoint = point;
            transform.position = point;
            tubePoints.Add(new Vector3(0f, yHeight, 0f));
            tubePoints.Add(new Vector3(0f, yHeight, 0f));
            idx = 1;
        }
        else
        {
            if (!isNearStartPoint(point))
            {
                float dx = point.x - startPoint.x;
                float dz = point.z - startPoint.z;
                Vector3 v = new Vector3(dx, 0f, dz);
                tubePoints[idx] = v;
                tubePoints.Add(v);
                idx++;
            }
            else
            {
                isClosed = true;
                tubePoints[idx] = tubePoints[0];
                id = ApplicationModel.iconIdx;
                ApplicationModel.iconIdx++;
            }
        }
        tubeRenderer.points = tubePoints.ToArray();
    }

    public void UpdateLastPoint(Vector3 point)
    {
        if(tubePoints.Count > 0)
        {
            if (!isNearStartPoint(point))
            {
                float dx = point.x - startPoint.x;
                float dz = point.z - startPoint.z;
                Vector3 v = new Vector3(dx, 0f, dz);
                tubePoints[idx] = v;
            }
            else
            {
                tubePoints[idx] = tubePoints[0];
            }
        }
        tubeRenderer.points = tubePoints.ToArray();
    }

    public bool isNearStartPoint(Vector3 point)
    {
        if (tubePoints.Count < 2)
        {
            return false;
        }

        Vector2 v2Start = new Vector2(startPoint.x, startPoint.z);
        Vector2 comparePoint = new Vector2(point.x, point.z);
        if (Vector2.Distance(v2Start, comparePoint) <= 0.2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool getIsClosed()
    {
        return isClosed;
    }


    // Update is called once per frame
    void Update()
    {
        // Get the floor point position to adda new point to the zone
        if (!isClosed)
        {
            if (sceneHandler.getCollidedObject() != null)
            {
                string collidedName = sceneHandler.getCollidedObject().name;
                if (collidedName == "MuseumFloor" || collidedName == "Floor" || collidedName == "WorldFloor")
                {
                    UpdateLastPoint(laserPointer.hitPoint);
                    if (grabInput.lastStateDown)
                    {
                        addPoint(laserPointer.hitPoint);
                    }
                }
            }
            
        }

        // Record collision with the zone
        if (ApplicationModel.recordingStatus == "On")
        {
            string target = ApplicationModel.recordedTarget;
            if (target == "ArObjects")
            {
                // Objects hold in hands or laser (objects hold by laser are right hand's children)
                ArObject[] rightHandObjects = rightHand.GetComponentsInChildren<ArObject>();
                ArObject[] leftHandObjects = leftHand.GetComponentsInChildren<ArObject>();

                foreach(ArObject aor in rightHandObjects)
                {
                    if(PointInPolygon(aor.transform.position.x, aor.transform.position.z))
                    {
                        if (!objectsInside.Contains(aor.getID()))
                        {
                            int id = aor.getID();
                            objectsInside.Add(id);
                            if (!dictObjects.ContainsKey(id))
                            {
                                dictObjects.Add(id, aor);
                            }

                            // Create enter event
                            if (!enterEvents.ContainsKey(id) || enterEvents[id]==null)
                            {
                                RecordedZoneEnter record = ScriptableObject.CreateInstance<RecordedZoneEnter>();
                                record.setEvent(aor.gameObject, this);
                                //objectsRecorder.addEnterZone(record);
                                iconMaker.createEventIcon(record);

                                objectsToWatch.Add(aor);
                                if (!enterEvents.ContainsKey(id))
                                {
                                    enterEvents.Add(id, record);
                                }
                                else //enterEvents[id]==null
                                {
                                    enterEvents[id] = record;
                                }
                            }
                        }
                    }
                }
                foreach(ArObject aor in leftHandObjects)
                {
                    if(PointInPolygon(aor.transform.position.x, aor.transform.position.z))
                    {
                        if (!objectsInside.Contains(aor.getID())){
                            int id = aor.getID();
                            objectsInside.Add(id);
                            if (!dictObjects.ContainsKey(id))
                            {
                                dictObjects.Add(id, aor);
                            }
                            Debug.Log(aor.name + "entered the zone");

                            // Create enter event
                            if (!enterEvents.ContainsKey(id) || enterEvents[id]==null)
                            {
                                RecordedZoneEnter record = ScriptableObject.CreateInstance<RecordedZoneEnter>();
                                record.setEvent(aor.gameObject, this);
                                //objectsRecorder.addEnterZone(record);
                                iconMaker.createEventIcon(record);

                                objectsToWatch.Add(aor);
                                enterEvents.Add(id, record);
                            }
                        }
                    }
                }

                List<int> toRemove = new List<int>();
                foreach(int i in objectsInside)
                {
                    Vector3 v = dictObjects[i].gameObject.transform.position;
                    if (!PointInPolygon(v.x, v.z))
                    {
                        toRemove.Add(i);

                        // Create enter event
                        if (!exitEvents.ContainsKey(i) || exitEvents[i]==null)
                        {
                            RecordedZoneExit record = ScriptableObject.CreateInstance<RecordedZoneExit>();
                            record.setEvent(dictObjects[i].gameObject, this);
                            //objectsRecorder.addExitZone(record);
                            iconMaker.createEventIcon(record);

                            objectsToWatch.Add(dictObjects[i]);
                            if (!exitEvents.ContainsKey(i))
                            {
                                exitEvents.Add(i, record);
                            }
                            else //exitEvents[i]==null
                            {
                                exitEvents[i] = record;
                            }
                        }
                        
                    }
                }
                foreach(int j in toRemove)
                {
                    objectsInside.Remove(j);
                }
            }
            else if(target == "Puppet")
            {
                if (puppetBody == null)
                {
                    puppetBody = GameObject.Find("WoodBody");
                }
                if (puppetBody != null)
                {
                    /*if (puppetRecorder == null)
                    {
                        puppetRecorder = GameObject.FindObjectOfType<InteractionPuppetRecorder>();
                    }*/
                    // Recording collisions with the puppet's body
                    if (PointInPolygon(puppetBody.transform.position.x, puppetBody.transform.position.z))
                    {
                        if (!puppetIsInside)
                        {
                            puppetIsInside = true;
                            if (!enterEvents.ContainsKey(2) || enterEvents[2]==null)
                            {
                                RecordedZoneEnter record = ScriptableObject.CreateInstance<RecordedZoneEnter>();
                                record.setEvent(puppetBody, this);
                                enterEvents.Add(2, record);
                                //puppetRecorder.addEnterZone(record);
                                iconMaker.createEventIcon(record);
                            }
                            // else: event exists already
                        }
                    }
                    else
                    {
                        if (puppetIsInside)
                        {
                            puppetIsInside = false;
                            if (!exitEvents.ContainsKey(2) || exitEvents[2]==null)
                            {
                                RecordedZoneExit record = ScriptableObject.CreateInstance<RecordedZoneExit>();
                                record.setEvent(puppetBody, this);
                                exitEvents.Add(2, record);
                                //puppetRecorder.addExitZone(record);
                                iconMaker.createEventIcon(record);
                            }
                            // else: event already exists
                        }
                    }
                }
                
            }
        }
        else
        {
            objectsInside = new List<int>();
        }

        // Detection
        if (ApplicationModel.mode == "Test")
        {
            if (isClosed)
            {
                // Monitor player
                if(enterEvents.ContainsKey(2) || exitEvents.ContainsKey(2))
                {
                    if (PointInPolygon(vrCam.transform.position.x, vrCam.transform.position.z))
                    {
                        if (!playerIsInside)
                        {
                            playerIsInside = true;
                            if (enterEvents.ContainsKey(2))
                            {
                                if (enterEvents[2] != null)
                                {
                                    enterEvents[2].getEventIcon().GetComponent<EventIconController>().trigerDone();
                                }
                                else
                                {
                                    enterEvents.Remove(2);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (playerIsInside)
                        {
                            playerIsInside = false;
                            if (exitEvents.ContainsKey(2))
                            {
                                if (exitEvents[2] != null)
                                {
                                    exitEvents[2].getEventIcon().GetComponent<EventIconController>().trigerDone();
                                }
                                else
                                {
                                    exitEvents.Remove(2);
                                }
                            }
                        }
                    }
                }
                // Monitor objects
                List<int> toRemove= new List<int>();
                for (int i = 0; i < objectsToWatch.Count; i++)
                {
                    ArObject aor = objectsToWatch[i];
                    if (aor != null)
                    {
                        int id = aor.getID();
                        bool isInside = PointInPolygon(aor.transform.position.x, aor.transform.position.z);
                        if (!previousObjectsState.ContainsKey(id))
                        {
                            previousObjectsState.Add(id, isInside);
                        }
                        else
                        {
                            if (previousObjectsState[id] != isInside) // enter or exit
                            {
                                if (isInside && enterEvents.ContainsKey(id)) // enter
                                {
                                    if (enterEvents[id] != null)
                                    {
                                        enterEvents[id].getEventIcon().GetComponent<EventIconController>().trigerDone();
                                    }
                                    else // event has been destroyed
                                    {
                                        enterEvents.Remove(id);
                                        if (!exitEvents.ContainsKey(id))
                                        {
                                            previousObjectsState.Remove(id);
                                            toRemove.Add(i);
                                        }
                                    }
                                }
                                else if(exitEvents.ContainsKey(id))//exit
                                {
                                    if (exitEvents[id] != null)
                                    {
                                        exitEvents[id].getEventIcon().GetComponent<EventIconController>().trigerDone();
                                    }
                                    else // event has been destroyed
                                    {
                                        exitEvents.Remove(id);
                                        if (!enterEvents.ContainsKey(id))
                                        {
                                            previousObjectsState.Remove(id);
                                            toRemove.Add(i);
                                        }
                                    }
                                }
                                previousObjectsState[id] = isInside;
                            }

                        }
                    }
                }
                foreach(int i in toRemove)
                {
                    objectsToWatch.RemoveAt(i);
                }
                
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    public float[] getMinMax()
    {
        float xMin = tubePoints[0].x;
        float xMax = tubePoints[0].x;
        float zMin = tubePoints[0].z;
        float zMax = tubePoints[0].z;

        foreach (Vector3 p in tubePoints)
        {
            float x = p.x;
            float z = p.z;
            if (x < xMin)
            {
                xMin = x;
            }
            else if (x > xMax)
            {
                xMax = x;
            }
            if (z < zMin)
            {
                zMin = z;
            }
            else if (z > zMax)
            {
                zMax = z;
            }
        }

        return new float[] { xMin, xMax, zMin, zMax };
    }

    public Vector3 GetCenterPoint()
    {
        float[] minMax = getMinMax();
        float xMin = minMax[0];
        float xMax = minMax[1];
        float zMin = minMax[2];
        float zMax = minMax[3];

        Vector3 center = new Vector3(startPoint.x + ((xMin + xMax) / 2), yHeight, startPoint.z + ((zMin + zMax) / 2));
        return center;
    }

    public override Vector3 getAttachPosition()
    {
        return GetCenterPoint();
    }

    public List<Vector3> getPoints()
    {
        return tubePoints;
    }

    public override void dispatchEventIcons()
    {
        EventIconController[] eventIcons = GetComponentsInChildren<EventIconController>();
        float y = yHeight + 0.5f;
        Vector3 center = GetCenterPoint();
        float x = center.x;
        float z = center.z;
        foreach(EventIconController e in eventIcons)
        {
            e.gameObject.transform.position = new Vector3(x, y, z);
            y += 0.1f;
        }
    }
    
    public override Vector3 getOutsidePoint()
    {
        Vector3 point = new Vector3(GetCenterPoint().x, yHeight + 0.7f, GetCenterPoint().z);
        
        float[] minMax = getMinMax();
        point.z = minMax[3] + 0.5f + startPoint.z;
        return point;
    }

    public override Vector3 getInsidePoint()
    {
        Vector3 point = new Vector3(GetCenterPoint().x, yHeight + 0.7f, GetCenterPoint().z);
        if (PointInPolygon(point.x, point.z))
        {
            return point;
        }

        else
        {
            float[] minMax = getMinMax();
            float xMin = minMax[0];
            float xMax = minMax[1];
            float zMin = minMax[2];
            float zMax = minMax[3];

            float z = zMin;
            float minZin = zMin;
            float maxZin = zMax;
            bool zInside = false;

            while (z<zMax)
            {
                if(PointInPolygon(point.x, z) && !zInside)
                {
                    zInside = true;
                    minZin = z;
                }
                if(!PointInPolygon(point.x, z) && zInside)
                {
                    zInside = false;
                    maxZin = z-0.01f;
                    break;
                }
                z += 0.01f;
            }

            point.z = (minZin + maxZin) / 2;
            point = new Vector3(point.x, point.y, point.z + startPoint.z);
            return point;
        }
    }

    // get description for saving
    public override EventDescription getDescription()
    {
        TurnTo translater = ScriptableObject.CreateInstance<TurnTo>();
        List<Vector3> descrPoints = new List<Vector3>();
        descrPoints.Add(startPoint);
        foreach(Vector3 p in tubePoints)
        {
            descrPoints.Add(p);
        }
        string trajectory = translater.turnTabToString(translater.turnVectorListToTab(descrPoints));
        EventDescription descr = new EventDescription("ZoneCollider", id, -1, new int[] { }, traj: trajectory);
        return descr;
    }


    // Following code from: http://csharphelper.com/blog/2014/07/determine-whether-a-point-is-inside-a-polygon-in-c/
    // Return True if the point is in the polygon.
    public bool PointInPolygon(float X, float Z)
    {
        X = X - startPoint.x;
        Z = Z - startPoint.z;
        Vector3[] pointsArray = tubePoints.ToArray();
        // Get the angle between the point and the
        // first and last vertices.
        int max_point = pointsArray.Length - 1;
        float total_angle = GetAngle(
            pointsArray[max_point].x, pointsArray[max_point].z,
            X, Z,
            pointsArray[0].x, pointsArray[0].z);

        // Add the angles from the point
        // to each other pair of vertices.
        for (int i = 0; i < max_point; i++)
        {
            total_angle += GetAngle(
                pointsArray[i].x, pointsArray[i].z,
                X, Z,
                pointsArray[i + 1].x, pointsArray[i + 1].z);
        }

        // The total angle should be 2 * PI or -2 * PI if
        // the point is in the polygon and close to zero
        // if the point is outside the polygon.
        // The following statement was changed. See the comments.
        //return (Math.Abs(total_angle) > 0.000001);
        return (Mathf.Abs(total_angle) > 1);
    }

    // Return the angle ABC.
    // Return a value between PI and -PI.
    // Note that the value is the opposite of what you might
    // expect because Y coordinates increase downward.
    public static float GetAngle(float Ax, float Ay,
        float Bx, float By, float Cx, float Cy)
    {
        // Get the dot product.
        float dot_product = DotProduct(Ax, Ay, Bx, By, Cx, Cy);

        // Get the cross product.
        float cross_product = CrossProductLength(Ax, Ay, Bx, By, Cx, Cy);

        // Calculate the angle.
        return (float)Mathf.Atan2(cross_product, dot_product);
    }

    // Return the dot product AB · BC.
    // Note that AB · BC = |AB| * |BC| * Cos(theta).
    private static float DotProduct(float Ax, float Ay,
        float Bx, float By, float Cx, float Cy)
    {
        // Get the vectors' coordinates.
        float BAx = Ax - Bx;
        float BAy = Ay - By;
        float BCx = Cx - Bx;
        float BCy = Cy - By;

        // Calculate the dot product.
        return (BAx * BCx + BAy * BCy);
    }

    // Return the cross product AB x BC.
    // The cross product is a vector perpendicular to AB
    // and BC having length |AB| * |BC| * Sin(theta) and
    // with direction given by the right-hand rule.
    // For two vectors in the X-Y plane, the result is a
    // vector with X and Y components 0 so the Z component
    // gives the vector's length and direction.
    public static float CrossProductLength(float Ax, float Ay,
        float Bx, float By, float Cx, float Cy)
    {
        // Get the vectors' coordinates.
        float BAx = Ax - Bx;
        float BAy = Ay - By;
        float BCx = Cx - Bx;
        float BCy = Cy - By;

        // Calculate the Z coordinate of the cross product.
        return (BAx * BCy - BAy * BCx);
    }

    public override bool isInside(Vector3 position)
    {
        return PointInPolygon(position.x, position.z);
    }

    public override void addEnterEvent(RecordedZoneEnter enter)
    {
        if (enterEvents == null)
        {
            enterEvents = new Dictionary<int, RecordedZoneEnter>();
        }
        enterEvents.Add(enter.getObjectOfInterestID(), enter);
        ArObject target = enter.getObjectOfInterest().GetComponent<ArObject>();
        if (target!=null)
        {
            if (objectsToWatch == null)
            {
                objectsToWatch = new List<ArObject>();
            }
            objectsToWatch.Add(target);
        }
    }

    public override void addExitEvent(RecordedZoneExit exit)
    {
        if (exitEvents == null)
        {
            exitEvents = new Dictionary<int, RecordedZoneExit>();
        }
        exitEvents.Add(exit.getObjectOfInterestID(), exit);
        ArObject target = exit.getObjectOfInterest().GetComponent<ArObject>();
        if (target != null)
        {
            if (objectsToWatch == null)
            {
                objectsToWatch = new List<ArObject>();
            }
            objectsToWatch.Add(target);
        }
    }
}

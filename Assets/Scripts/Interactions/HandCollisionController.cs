﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class HandCollisionController : MonoBehaviour
{
    private GameObject hand;
    private InteractionControllerRecorder handrecorder;
    private bool isMonitoring;
    private EventAnimationLinkController currentLink;
    private Hand dominantHand;
    private SteamVR_Action_Boolean input;
    private Dictionary<GameObject, EventIconController> collisionsToDetect;

    public void setHand(string handName)
    {
        hand = GameObject.Find(handName);
        handrecorder = hand.GetComponent<InteractionControllerRecorder>();
        collisionsToDetect = new Dictionary<GameObject, EventIconController>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (handrecorder == null)
        {
            handrecorder = hand.GetComponent<InteractionControllerRecorder>();
        }
        if (ApplicationModel.recordingStatus == "On" && handrecorder.getIsRecording() && other.gameObject.name!= "Open_Close_Sphere" && other.gameObject.tag == "ArObjects")
        {
            handrecorder.newCollision(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (isMonitoring)
        {
            EventIconController eventIcon = other.gameObject.GetComponent<EventIconController>();
            if (eventIcon != null)
            {
                if (currentLink.triggerIsNull())
                {
                    dominantHand.ShowGrabHint();
                    if (input.lastStateUp)
                    {
                        currentLink.addTrigger(eventIcon);
                        dominantHand.HideGrabHint();
                    }
                }
                else if (currentLink.animationIsNull() && eventIcon.isAnimation() && eventIcon!=currentLink.getTrigger())
                {
                    dominantHand.ShowGrabHint();
                    if (input.lastStateUp)
                    {
                        currentLink.addAnimation(eventIcon);
                        dominantHand.HideGrabHint();
                    }
                }
            }
        }
        if (ApplicationModel.mode == "Test")
        {
            if (collisionsToDetect.ContainsKey(other.gameObject))
            {
                EventIconController eventIcon = collisionsToDetect[other.gameObject];
                eventIcon.trigerDone();
                //eventIcon.launchAllAnimations();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (isMonitoring)
        {
            dominantHand.HideGrabHint();
        }
  
    }

    /*private void OnCollisionExit(Collision collision)
    {
        if (ApplicationModel.recordingStatus == "On")
        {
            handrecorder.endCollision(collision);
        }
    }*/

    public void setLink(EventAnimationLinkController link)
    {
        dominantHand = GameObject.Find(ApplicationModel.dominantHand).GetComponent<Hand>();
        input = SteamVR_Input.GetBooleanAction("GrabGrip");
        currentLink = link;
    }

    public void setMonitoring(bool monitor)
    {
        isMonitoring = monitor;
    }

    public void addCollisionToDetect(EventIconController eventIcon)
    {
        RecordedCollision collisionEvent = (RecordedCollision)eventIcon.getRecordedEvent();
        if (collisionEvent != null)
        {
            GameObject collidedObject = collisionEvent.getColllidedObject();
            if (collidedObject != null && eventIcon!=null)
            {
                if (collisionsToDetect == null)
                {
                    collisionsToDetect = new Dictionary<GameObject, EventIconController>();
                }
                try
                {
                    collisionsToDetect.Add(collidedObject, eventIcon);
                }
                catch
                {
                    //Debug.Log("Collision between "+collidedObject.name+" and hand already added");
                }
                
            }
        }
    }
}

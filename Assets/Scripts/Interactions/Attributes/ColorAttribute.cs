﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorAttribute : AttributeController
{
    public ColorPickerTriangle colorPicker;
    private Material objectMaterial;
    //private InteractionArObjectsRecorder recorder;
    private bool wasRecording;
    private InteractionAnalyzer iconMaker;

    protected override void Start()
    {
        base.Start();
        height = -0.05f;
        //recorder = GameObject.FindObjectOfType<InteractionArObjectsRecorder>();
        wasRecording = false;
        iconMaker = GameObject.FindObjectOfType<InteractionAnalyzer>();
        if(colorPicker != null)
        {
            colorPicker.addColorAttribute(this);
        }
    }

    public override void setArParent(ArObject parent)
    {
        base.setArParent(parent);
        MeshRenderer meshRender = arParent.gameObject.GetComponent<MeshRenderer>();
        
        if (meshRender != null)
        {
            objectMaterial = meshRender.material;
            colorPicker.SetNewColor(objectMaterial.color);
        }
    }

    public void createColorEvent()
    {
        if (objectMaterial != null)
        {
            if (ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "ArObjects")
            {
                if (!wasRecording)
                {
                    wasRecording = true;
                }
                Color c1 = colorPicker.TheColor;
                Color c2 = objectMaterial.color;
                bool cond1 = (Mathf.RoundToInt(c1.r * 1000) != Mathf.RoundToInt(c2.r * 1000));
                bool cond2 = (Mathf.RoundToInt(c1.g * 1000) != Mathf.RoundToInt(c2.g * 1000));
                bool cond3 = (Mathf.RoundToInt(c1.b * 1000) != Mathf.RoundToInt(c2.b * 1000));
                if (cond1 || cond2 || cond3)
                {
                    Color temporaryC = new Color(c1.r, c1.g, c1.b, c2.a);
                    objectMaterial.color = temporaryC;
                    RecordedColor colorEvent = ScriptableObject.CreateInstance<RecordedColor>();
                    colorEvent.setColorEvent(arParent.gameObject, colorPicker.TheColor);
                    iconMaker.createEventIcon(colorEvent);
                    //recorder.addColor(colorPicker.TheColor, arParent.gameObject);
                }

            }
        }
    }

    protected override void Update()
    {
        base.Update();
        if (objectMaterial != null)
        {
            if (ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "ArObjects")
            {
                if (!wasRecording)
                {
                    wasRecording = true;
                }
                /*Color c1 = colorPicker.TheColor;
                Color c2 = objectMaterial.color;
                bool cond1 = (Mathf.RoundToInt(c1.r * 1000) != Mathf.RoundToInt(c2.r * 1000));
                bool cond2 = (Mathf.RoundToInt(c1.g * 1000) != Mathf.RoundToInt(c2.g * 1000));
                bool cond3 = (Mathf.RoundToInt(c1.b * 1000) != Mathf.RoundToInt(c2.b * 1000));
                if (cond1 || cond2 || cond3)
                {
                    Color temporaryC = new Color(c1.r, c1.g, c1.b, c2.a);
                    objectMaterial.color = temporaryC;
                    RecordedColor colorEvent = ScriptableObject.CreateInstance<RecordedColor>();
                    colorEvent.setColorEvent(arParent.gameObject, colorPicker.TheColor);
                    iconMaker.createEventIcon(colorEvent);
                    //recorder.addColor(colorPicker.TheColor, arParent.gameObject);
                }*/
                
            }
            else
            {
                if (wasRecording)
                {
                    setToDefaultValue();
                    wasRecording = false;
                }
                if (colorPicker.TheColor != arParent.getColorAtStart())
                {
                    objectMaterial.color = colorPicker.TheColor;
                    arParent.setColorAtStart(colorPicker.TheColor);
                }
            }
                
        }
    }


    public override void setInformation()
    {
        information = "Set this attribute to pick a color for this object. \n Use the laser to pick a color.";
        panelText.text = information;
    }

    protected override bool getDefaultValue()
    {
        return false;
    }

    public override void setToDefaultValue()
    {
        arParent.defaultColor();
        Color c = new Color(objectMaterial.color.r, objectMaterial.color.g, objectMaterial.color.b, 1);
        colorPicker.SetNewColor(c);
    }


}

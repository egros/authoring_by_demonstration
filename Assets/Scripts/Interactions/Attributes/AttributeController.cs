﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;

public abstract class AttributeController : MonoBehaviour
{
    public GameObject canvas;
    protected InteractionArObjectsRecorder objectRecorder;

    protected ArObject arParent;
    protected Vector3 oldARPosition;
    protected Vector3 middlePoint;
    protected string information;
    protected Text panelText;
    protected float height;

    private GameObject vrCamera;
    private bool needToUpdate;

    protected virtual void Start()
    {
        objectRecorder = GameObject.FindObjectOfType<InteractionArObjectsRecorder>();
        vrCamera = GameObject.Find("VRCamera");
        panelText = canvas.GetComponentInChildren<Text>();
        setInformation();
        hideInfoPanel();
    }

    public virtual void setArParent(ArObject parent)
    {
        arParent = parent;
        oldARPosition = arParent.gameObject.transform.position;
        middlePoint = arParent.getMiddlePoint();
    }

    public void displayInfoPanel()
    {
        canvas.SetActive(true);
    }

    public void hideInfoPanel()
    {
        canvas.SetActive(false);
    }

    public abstract void setInformation();


    protected abstract bool getDefaultValue();

    public abstract void setToDefaultValue();

    protected virtual void Update()
    {
        if (needToUpdate)
        {
            if (arParent.transform.position.z != vrCamera.transform.position.z)
            {
                if (oldARPosition != arParent.transform.position)
                {
                    middlePoint = arParent.getMiddlePoint();
                }
                setPosition();
                // ROTATION
                transform.LookAt(vrCamera.transform);

            }
        }
        
    }

    private void OnEnable()
    {
        needToUpdate = true;
    }

    private void setPosition()
    {
        Vector3 relativPosition = vrCamera.transform.InverseTransformPoint(arParent.transform.position);
        GeometryAnalyzer geoAnalyzer = new GeometryAnalyzer();
        minNmax extremities = geoAnalyzer.getMinNmax(arParent.gameObject);
        float dX = Mathf.Abs((extremities.max.x - extremities.min.x) / 2)+0.12f;
        Vector3 newRelPos = new Vector3(relativPosition.x-dX, relativPosition.y, relativPosition.z);
        Vector3 finalPos = new Vector3(vrCamera.transform.TransformPoint(newRelPos).x, vrCamera.transform.TransformPoint(newRelPos).y + height, vrCamera.transform.TransformPoint(newRelPos).z);
        transform.position = finalPos;
        needToUpdate = false;
        /*
        // POSITION
        float R0 = arParent.getRadius() + 0.05f;//0.4f;
        float R1 = Vector2.Distance(new Vector2(arParent.transform.position.x, arParent.transform.position.z), new Vector2(vrCamera.transform.position.x, vrCamera.transform.position.z));
        float x0 = arParent.transform.position.x;
        float y0 = arParent.transform.position.z;
        float x1 = vrCamera.transform.position.x;
        float y1 = vrCamera.transform.position.z;

        // Solution obtenue sur http://math.15873.pagesperso-orange.fr/IntCercl.html
        float n = (R1 * R1 - R0 * R0 - x1 * x1 + x0 * x0 - y1 * y1 + y0 * y0) / (2 * (y0 - y1));
        float dif = (x0 - x1) / (y0 - y1);
        float a = Mathf.Pow(dif, 2) + 1;
        float b = 2 * y0 * dif - 2 * n * dif - 2 * x0;
        float c = x0 * x0 + y0 * y0 + n * n - R0 * R0 - 2 * y0 * n;

        float delta = Mathf.Sqrt(b * b - 4 * a * c);

        float xP = (-b + delta) / (2 * a); // +/- delta 
        float yP = n - xP * dif;

        if(!float.IsNaN(xP) && !float.IsNaN(yP))
        {
            transform.position = new Vector3(xP, middlePoint.y + height, yP);
            needToUpdate = false;
        }
        */
    }
}

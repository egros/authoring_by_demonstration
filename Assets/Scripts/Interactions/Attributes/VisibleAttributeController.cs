﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleAttributeController : BooleanAttributeController
{
    protected override void Start()
    {
        base.Start();
        height = 0.05f;
    }

    public override void setInformation()
    {
        information = "Set this attribute to decide whether the object is visible. \n Current status: Object is ";
        if (isEnabled)
        {
            information = information + "visible";
        }
        else
        {
            information = information + "not visible";
        }
        panelText.text = information;
    }

    protected override bool getDefaultValue()
    {
        return arParent.getIsVisibleAtStart();
    }

    public override void switchOnOff()
    {
        base.switchOnOff();
        if (ApplicationModel.recordingStatus == "On" && ApplicationModel.recordedTarget == "ArObjects")
        {
            // Create event
            if (!isEnabled)
            {
                // Hide event
                objectRecorder.addHide(arParent.gameObject);
                arParent.setTransparency(isEnabled);
            }
            else
            {
                // Show event
                objectRecorder.addShow(arParent.gameObject);
                arParent.setTransparency(isEnabled);
            }
        }
        else
        {
            arParent.setIsVisibleAtStart(isEnabled);
        }

    }

    public override void setToDefaultValue()
    {
        base.setToDefaultValue();
        arParent.setTransparency(getDefaultValue());
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BooleanAttributeController : AttributeController
{
    public Material disabledMat;
    public Material transparentMat;
    public MeshRenderer mesh;
    public bool isEnabled;

    protected override void Start()
    {
        base.Start();
        if (isEnabled)
        {
            mesh.material = transparentMat;
        }
        else
        {
            mesh.material = disabledMat;
        }
    }

    public virtual void switchOnOff()
    {
        isEnabled = !isEnabled;
        if (!isEnabled)
        {
            mesh.material = disabledMat;
        }
        else
        {
            mesh.material = transparentMat;
        }
        setInformation();
    }

    public override void setToDefaultValue()
    {
        if (getDefaultValue())
        {
            mesh.material = transparentMat;
            isEnabled = true;
        }
        else
        {
            mesh.material = disabledMat;
            isEnabled = false;
        }
    }

}

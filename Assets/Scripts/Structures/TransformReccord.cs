﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformReccord
{
    private Vector3 position;
    private Quaternion rotation;

    public TransformReccord(Vector3 pos, Quaternion rota)
    {
        position = pos;
        rotation = rota;
    }

    public Vector3 getPosition()
    {
        return position;
    }

    public Quaternion getRotation()
    {
        return rotation;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroSceneController : MonoBehaviour
{
    public string vrSceneName;
    // Start is called before the first frame update
    void Start()
    {
        SceneManager.LoadScene(vrSceneName);
    }

}

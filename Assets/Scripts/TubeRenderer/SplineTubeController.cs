﻿using BezierSolution;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineTubeController : MonoBehaviour
{
    public TubeRenderer tube;
    private GameObject anchor1;
    private GameObject anchor2;
    private SceneHandler laserAnchor;
    public Material tubeMaterial;
    private BezierSpline spline;
    private BezierPoint[] points;


    // Start is called before the first frame update
    void Start()
    {
        spline = GetComponentInChildren<BezierSpline>();
        points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
        if (tube == null)
        {
            makeTube();
        }
        
    }


    private void makeTube()
    {
        // Add TubeRendeder component.
        tube = gameObject.AddComponent<TubeRenderer>();

        // Optimise for realtime manipulation.
        tube.MarkDynamic();

        // Set a texture and a uv mapping.
        tube.GetComponent<Renderer>().material.mainTexture = tubeMaterial.mainTexture;
        //tube.GetComponent<Renderer>().material.mainTexture.wrapMode = TextureWrapMode.Repeat;
        tube.uvRect = new Rect(0, 0, 6, 1);
        tube.uvRectCap = new Rect(0, 0, 4 / 12f, 4 / 12f);
        tube.radius = 0.01f;

    }

    private void updateTube()
    {
        if (tube == null)
        {
            makeTube();
        }
        Vector3[] tab = getBezierPoints();
        tube.points = tab;
    }

    private Vector3[] getBezierPoints()
    {
        Vector3[] pointTab = new Vector3[points.Length];
        for(int i=0; i<points.Length; i++)
        {
            pointTab[i] = transform.parent.InverseTransformPoint(points[i].gameObject.transform.position);
        }
        return pointTab;
    }

    

    public void setPoints(GameObject point1, GameObject point2)
    {
        if (spline == null)
        {
            spline = GetComponentInChildren<BezierSpline>();
        }
        if (points == null)
        {
            points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
        }
        anchor1 = point1;
        anchor2 = point2;
        if (points.Length >= 2)
        {
            points[0].gameObject.transform.position = anchor1.transform.position;
            points[1].gameObject.transform.position = anchor2.transform.position;
        }
        updateTube();
    }

    public void setSceneHandler(SceneHandler sceneHandler)
    {
        laserAnchor = sceneHandler;
        if (spline == null)
        {
            spline = GetComponentInChildren<BezierSpline>();
        }
        if (points == null)
        {
            points = spline.gameObject.GetComponentsInChildren<BezierPoint>();
        }
        if (points.Length >= 2)
        {
            points[0].gameObject.transform.position = laserAnchor.getLaserHitPoint();
            points[1].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
        updateTube();
    }

    public void setAnchor(int anchorNum, GameObject anchor)
    {
        if (anchorNum == 1)
        {
            anchor1 = anchor;
            points[0].gameObject.transform.position = anchor1.transform.position;
            points[1].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
        else if (anchorNum == 2)
        {
            anchor2 = anchor;
            points[0].gameObject.transform.position = anchor1.transform.position;
            points[1].gameObject.transform.position = anchor2.transform.position;
        }
        updateTube();
    }

    private void Update()
    {
        if (anchor1 != null)
        {
            if (anchor1.transform.position != points[0].gameObject.transform.position)
            {
                points[0].gameObject.transform.position = anchor1.transform.position;
            }
        }
        else if (laserAnchor != null)
        {
            points[0].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
        if (anchor2 != null)
        {
            if (anchor2.transform.position != points[1].gameObject.transform.position)
            {
                points[1].gameObject.transform.position = anchor2.transform.position;
            }
        }
        else if (laserAnchor != null)
        {
            points[1].gameObject.transform.position = laserAnchor.getLaserHitPoint();
        }
        updateTube();

    }

}


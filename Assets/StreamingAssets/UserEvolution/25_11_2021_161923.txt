{"objects" : [
{"mainType":"3DProps","id":5,"subtype":"grape_highpoly","position":[-34.996585845947269,0.9237502813339233,-17.482080459594728],"size":[0.9999998211860657,1.000000238418579,1.0],"rotation":[-0.008450448513031006,0.9666103720664978,-0.09661004692316055,-0.23719114065170289],"visible":true,"color":[0.0,0.0,0.0,0.0]}, 
{"mainType":"Geometry","id":4,"subtype":"Cube","position":[-35.018280029296878,1.128488540649414,-16.425941467285158],"size":[0.09999997913837433,0.10000000894069672,0.10000000894069672],"rotation":[0.10549088567495346,0.33998021483421328,-0.07777267694473267,0.9312554001808167],"visible":true,"color":[1.0,1.0,1.0,1.0]}, 
{"mainType":"Geometry","id":3,"subtype":"Sphere","position":[-35.22224426269531,1.066328525543213,-16.873212814331056],"size":[0.10000000894069672,0.09999999403953552,0.09999999403953552],"rotation":[0.0,0.0,-8.731149137020111e-10,1.0],"visible":true,"color":[1.0,1.0,1.0,1.0]} 
], 
 "events": [{"mainType":"Collision","id":2,"target":0,"animations":[],"previousIcon":-1,"nextIcon":-1,"color":[],"holdingHand":-1,"trajectory":"","rotations":"","collided":5,"followDirection":false}, 
{"mainType":"Move","id":1,"target":4,"animations":[],"previousIcon":-1,"nextIcon":-1,"color":[],"holdingHand":-1,"trajectory":"[[-35.01828,1.128489,-16.42594], [-34.96782,1.246232,-16.45018], [-34.99032,1.341355,-16.52507], [-35.04244,1.431393,-16.65182], [-35.08228,1.495168,-16.82665], [-35.15671,1.460932,-17.02841], [-35.20632,1.369862,-17.179], [-35.26148,1.260474,-17.26939], [-35.27069,1.213302,-17.32263]]","rotations":"[[0.1054909,0.3399802,-0.07777268,0.9312554], [0.1135209,0.3315263,-0.1683362,0.9213394], [0.1049584,0.2767276,-0.2176083,0.930082], [0.06750792,0.2057106,-0.2438117,0.9453474], [-0.001082525,0.1311943,-0.2605807,0.9564961], [-0.05047594,0.00409928,-0.2131187,0.975713], [-0.09327488,-0.08046874,-0.157895,0.9797417], [-0.1056569,-0.1367737,-0.09085,0.9807527], [-0.1321591,-0.15374,-0.07519428,0.9763421], [-0.1321591,-0.15374,-0.07519428,0.9763421], [-0.1321591,-0.15374,-0.07519428,0.9763421], [-0.1321591,-0.15374,-0.07519428,0.9763421]]","collided":-1,"followDirection":false}, 
{"mainType":"Hide","id":0,"target":3,"animations":[],"previousIcon":-1,"nextIcon":-1,"color":[],"holdingHand":-1,"trajectory":"","rotations":"","collided":-1,"followDirection":false}]}